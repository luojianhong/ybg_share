import com.alibaba.fastjson.JSONObject;
import com.ybg.BlockApplication;
import com.ybg.block.core.dbapi.entity.BlockWallet;
import com.ybg.block.core.dbapi.service.BlockWalletService;
import com.ybg.block.core.eth.config.EthConfig;
import com.ybg.block.framework.api.YbgGeth;
import com.ybg.framework.vo.R;
import com.ybg.share.core.eth.EthAccountRemoteApi;
import com.ybg.share.core.eth.EthGethRemoteApi;
import com.ybg.share.core.eth.EthWalletRemoteApi;
import com.ybg.share.core.eth.vo.EthWalletSendResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jService;
import org.web3j.protocol.admin.Admin;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Transfer;
import org.web3j.utils.Convert;

import java.math.BigDecimal;
import java.util.List;

@SpringBootTest(classes = BlockApplication.class)
@RunWith(SpringRunner.class)
public class WalletTest {
    private static String address = "f57845cdd0879728eaa86a3d97bfe6db6ddb273c";

    @Autowired
    EthWalletRemoteApi ethRemoteApi;
    @Autowired
    EthConfig ethConfig;

    @Test
    public void testAddWallet() {
        YbgGeth ybgGeth = ethConfig.geth(null);

        String password = "123456";
        ethRemoteApi.addWallet(password);
    }

    @Test
    public void testLoadWallet() {
        String password = "123456";
        ethRemoteApi.loadWallet(password, "");
    }

    @Test
    public void testBanlance() {
        System.out.println(ethRemoteApi.getAddressBalance(address).getData());
    }

    @Autowired
    Admin admin;

    @Test
    public void testAccountList() {
        try {
            List<String> list = admin.personalListAccounts().send().getAccountIds();
            if (list == null || list.size() == 0) {
                System.out.println("没有账户");
                return;
            }
            list.stream().forEach(System.out::println);
//            for (String s : list) {
//                System.out.println(s);
//            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Autowired
    EthAccountRemoteApi ethAccountRemoteApi;

    @Autowired
    EthGethRemoteApi ethGethRemoteApi;

    @Test
    public void addAcount() {
        ethAccountRemoteApi.addAccount("123456");
        testAccountList();
      //   ethGethRemoteApi.setEtherbase();
    }

    @Test
    public void mineStart() {
        //https://www.cnblogs.com/tianlongtc/p/8871472.html
        String account = ethGethRemoteApi.getCoinbase().getData();
        ethAccountRemoteApi.unlockAccount(account,"123456");
        System.out.println("挖矿账户是" + account);
        ethGethRemoteApi.setEtherbase(account);
        R<Boolean> booleanR = ethGethRemoteApi.minerStart(10);
        if(booleanR.getCode()==R.SUCCESS){
            System.out.println(booleanR.getData());
        }else{
            System.out.println(booleanR.getMsg());
        }
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ethGethRemoteApi.mining();
        }

    }

    @Test
    public void mining(){
        ethGethRemoteApi.mining();
    }

    @Autowired
    private YbgGeth geth;
    @Autowired
    Web3jService web3jService;

    @Test
    public void mineStop() {

//        try {
//            new Request(
//                    "miner_stop", Collections.<String>emptyList(), web3jService, Void.class).send();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        try{
            String account = ethGethRemoteApi.getCoinbase().getData();
            ethAccountRemoteApi.unlockAccount(account,"123456");
            System.out.println("挖矿账户是" + account);
            ethGethRemoteApi.setEtherbase(account);
            geth.minerStop().send();
            ethGethRemoteApi.mining();
        }catch (Exception e){
            e.printStackTrace();
        }

//        boolean b = ethGethRemoteApi.minerStop().getData().booleanValue();
//        System.out.println("停止挖矿结果："+b);
    }


    @Autowired
    EthWalletRemoteApi ethWalletRemoteApi;
    @Test
    public void transTo(){
//        String address1="0xf8b0c7cb8c42cd402155d554b29525962b4d7f96";
//        String address2="0x8c1629b84855e4d4513d2b73748e9eaa93d8d239";
        String address1="1966decb8464d2952ae3b0682751b793f133d4c5";
        String address2="f57845cdd0879728eaa86a3d97bfe6db6ddb273c";

        R<EthWalletSendResult> ethWalletSendResultR = ethWalletRemoteApi.tradeTo(address1, "123456", address2, new BigDecimal("0.01"));
        System.out.println(JSONObject.toJSON(ethWalletSendResultR));
    }

    @Autowired
    Web3j web3j;
    @Autowired
    BlockWalletService blockWalletService;
    @Test
    public void transTo2(){
//开始发送0.01 =eth到指定地址
        String address_to = "1966decb8464d2952ae3b0682751b793f133d4c5";
        String passWord = "123456";
        BlockWallet blockWallet = blockWalletService.getByAddress(address);
        try{
            Credentials credentials = WalletUtils.loadJsonCredentials(passWord, blockWallet.getFileData());
            TransactionReceipt send = Transfer.sendFunds(web3j, credentials, address_to, BigDecimal.ONE, Convert.Unit.FINNEY).send();
            System.out.println(JSONObject.toJSON(send));
        }catch (Exception e){
            e.printStackTrace();
        }


    }

}
