import org.web3j.utils.Convert;

import java.math.BigDecimal;

public class TestValue {
    public static void main(String[] args) {
        BigDecimal weiValue = Convert.toWei(BigDecimal.ONE, Convert.Unit.FINNEY);
        System.out.println(weiValue);
    }
}
