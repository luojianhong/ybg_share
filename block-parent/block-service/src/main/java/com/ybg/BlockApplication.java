package com.ybg;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@Slf4j
@MapperScan("com.ybg.block.core.dbapi.dao")
@EnableScheduling
public class BlockApplication {
    public static void main(String[] args) {
        SpringApplication.run(BlockApplication.class, args);
        log.info("区块链系统启动完毕！");
    }
}
