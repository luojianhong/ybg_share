package com.ybg.block.core.dbapi.service.impl;

import com.ybg.block.core.dbapi.entity.FabricApp;
import com.ybg.block.core.dbapi.dao.FabricAppMapper;
import com.ybg.block.core.dbapi.service.FabricAppService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 应用 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
//@Service(version = "1.0.0", interfaceClass = FabricAppService.class)
@Service
public class FabricAppServiceImpl extends ServiceImpl<FabricAppMapper, FabricApp> implements FabricAppService {


}
