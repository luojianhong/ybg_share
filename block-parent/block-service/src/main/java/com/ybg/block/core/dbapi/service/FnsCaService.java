package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FnsCa;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.block.core.dbapi.vo.FnsCaVO;
import com.ybg.block.core.dbapi.vo.FnsPeerVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsCaService extends IService<FnsCa> {
    int add(FnsCaVO ca, MultipartFile skFile, MultipartFile certificateFile);

    int update(FnsCaVO ca, MultipartFile skFile, MultipartFile certificateFile);

    List<FnsCaVO> listAll();

    List<FnsCa> listById(int id);

    FnsCa get(int id);

    FnsCa getByFlag(String flag);

    int countById(int id);

    int count();

    int delete(int id);

    List<FnsPeerVO> getFullPeers();

    List<FnsPeerVO> getPeersByCA(FnsCa ca);

    List<FnsCaVO> listFullCA();
}
