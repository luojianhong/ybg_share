package com.ybg.block.core.fabric.datamapper;

import com.ybg.block.core.dbapi.entity.FabricChaincode;
import com.ybg.share.core.fabric.vo.FabricChaincodeVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface FabricChaincodeDataMapper {
    FabricChaincodeDataMapper INSTANCE = Mappers.getMapper(FabricChaincodeDataMapper.class);

    FabricChaincodeVO toVO(FabricChaincode bean);

    List<FabricChaincodeVO> toVOList(List<FabricChaincode> list);

}
