package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FabricCa;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 证书授权中心 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricCaMapper extends BaseMapper<FabricCa> {

}
