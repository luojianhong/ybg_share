package com.ybg.block.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 区块
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
@ApiModel(value = "区块")
public class FnsBlock extends Model<FnsBlock> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Integer id;
    /**
     * 通道ID
     */
    @ApiModelProperty(value = "通道ID")
    private Integer channelId;
    /**
     * 交易区块高度
     */
    @ApiModelProperty(value = "交易区块高度")
    private Integer height;
    /**
     * 交易hash
     */
    @ApiModelProperty(value = "交易hash")
    private String dataHash;
    /**
     * 当前区块Hash
     */
    @ApiModelProperty(value = "当前区块Hash")
    private String calculatedHash;
    /**
     * 当前区块
     */
    @ApiModelProperty(value = "当前区块")
    private String previousHash;
    /**
     * 信封计数
     */
    @ApiModelProperty(value = "信封计数")
    private Integer envelopeCount;
    /**
     * 交易数量
     */
    @ApiModelProperty(value = "交易数量")
    private Integer txCount;
    /**
     * 读写集计数
     */
    @ApiModelProperty(value = "读写集计数")
    private Integer rWSetCount;
    /**
     * 时间戳
     */
    @ApiModelProperty(value = "时间戳")
    private String timestamp;
    /**
     * 计算日期
     */
    @ApiModelProperty(value = "计算日期")
    private Integer calculateDate;
    /**
     * 生成日期
     */
    @ApiModelProperty(value = "生成日期")
    private String createDate;


    /**
     * 获取主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取通道ID
     */
    public Integer getChannelId() {
        return channelId;
    }

    /**
     * 设置通道ID
     */

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    /**
     * 获取交易区块高度
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * 设置交易区块高度
     */

    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * 获取交易hash
     */
    public String getDataHash() {
        return dataHash;
    }

    /**
     * 设置交易hash
     */

    public void setDataHash(String dataHash) {
        this.dataHash = dataHash;
    }

    /**
     * 获取当前区块Hash
     */
    public String getCalculatedHash() {
        return calculatedHash;
    }

    /**
     * 设置当前区块Hash
     */

    public void setCalculatedHash(String calculatedHash) {
        this.calculatedHash = calculatedHash;
    }

    /**
     * 获取当前区块
     */
    public String getPreviousHash() {
        return previousHash;
    }

    /**
     * 设置当前区块
     */

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    /**
     * 获取信封计数
     */
    public Integer getEnvelopeCount() {
        return envelopeCount;
    }

    /**
     * 设置信封计数
     */

    public void setEnvelopeCount(Integer envelopeCount) {
        this.envelopeCount = envelopeCount;
    }

    /**
     * 获取交易数量
     */
    public Integer getTxCount() {
        return txCount;
    }

    /**
     * 设置交易数量
     */

    public void setTxCount(Integer txCount) {
        this.txCount = txCount;
    }

    /**
     * 获取读写集计数
     */
    public Integer getrWSetCount() {
        return rWSetCount;
    }

    /**
     * 设置读写集计数
     */

    public void setrWSetCount(Integer rWSetCount) {
        this.rWSetCount = rWSetCount;
    }

    /**
     * 获取时间戳
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * 设置时间戳
     */

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * 获取计算日期
     */
    public Integer getCalculateDate() {
        return calculateDate;
    }

    /**
     * 设置计算日期
     */

    public void setCalculateDate(Integer calculateDate) {
        this.calculateDate = calculateDate;
    }

    /**
     * 获取生成日期
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * 设置生成日期
     */

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 通道ID列的数据库字段名称
     */
    public static final String CHANNEL_ID = "channel_id";

    /**
     * 交易区块高度列的数据库字段名称
     */
    public static final String HEIGHT = "height";

    /**
     * 交易hash列的数据库字段名称
     */
    public static final String DATA_HASH = "data_hash";

    /**
     * 当前区块Hash列的数据库字段名称
     */
    public static final String CALCULATED_HASH = "calculated_hash";

    /**
     * 当前区块列的数据库字段名称
     */
    public static final String PREVIOUS_HASH = "previous_hash";

    /**
     * 信封计数列的数据库字段名称
     */
    public static final String ENVELOPE_COUNT = "envelope_count";

    /**
     * 交易数量列的数据库字段名称
     */
    public static final String TX_COUNT = "tx_count";

    /**
     * 读写集计数列的数据库字段名称
     */
    public static final String R_W_SET_COUNT = "r_w_set_count";

    /**
     * 时间戳列的数据库字段名称
     */
    public static final String TIMESTAMP = "timestamp";

    /**
     * 计算日期列的数据库字段名称
     */
    public static final String CALCULATE_DATE = "calculate_date";

    /**
     * 生成日期列的数据库字段名称
     */
    public static final String CREATE_DATE = "create_date";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FnsBlock{" +
                "id=" + id +
                ", channelId=" + channelId +
                ", height=" + height +
                ", dataHash=" + dataHash +
                ", calculatedHash=" + calculatedHash +
                ", previousHash=" + previousHash +
                ", envelopeCount=" + envelopeCount +
                ", txCount=" + txCount +
                ", rWSetCount=" + rWSetCount +
                ", timestamp=" + timestamp +
                ", calculateDate=" + calculateDate +
                ", createDate=" + createDate +
                "}";
    }
}
