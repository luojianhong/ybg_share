package com.ybg.block.framework.api;

import com.ybg.block.framework.bean.*;
import com.ybg.block.framework.impl.YbgJsonRpc2_0Geth;
import org.web3j.protocol.Web3jService;
import org.web3j.protocol.admin.methods.response.BooleanResponse;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.Response;
import org.web3j.protocol.geth.Geth;

import java.math.BigDecimal;

/**
 * 节点信息
 * 由于不是所有接口都有实现 所以自己定义了一个写接口
 */
public interface YbgGeth extends Geth {
    /**
     * 指定挖矿账户
     *
     * @param accountId accountId
     * @return 其返回值为一个表示是否成功设置指定用户的布尔值。
     */
    Request<?, BooleanResponse> setEtherbase(String accountId);

    static YbgGeth build(Web3jService web3jService) {
        return new YbgJsonRpc2_0Geth(web3jService);
    }

    /**
     * addPeer   方法可以将新的远程节点加入到本地跟踪的远程节点列表，本地节点 将始终尝试保持与这些远程节点旳连接，并在必要时进行重连
     *
     * @param url url
     * @return 方法接受单一参数，即远程节点旳enode地址，当添加成功后会返回TRUE， 否则返回false。
     */
    Request<?, BooleanResponse> adminAddPeer(String url);


    /**
     * datadir属性可用来查询当前运行的Geth节点的区块链数据存储绝对路径。
     */
    Request<?, StringResponse> adminDatadir();

    /**
     * nodeInfo属性可用来查询当前运行的geth节点旳网络相关信息，包括ÐΞVp2p协议 信息以及运行中的应用协议信息，例如eth、les、shh、bzz等。
     *
     * @return req
     */
    Request<?, EthNodeInfoResponse> adminNodeInfo();

    /**
     * peers属性可用来查询当前geth节点已连接的远程节点的相关信息，包括ÐΞVp2p协议信息 以及运行中的应用协议信息，例如eth、les、shh、bzz等。
     *
     * @return req
     */
    Request<?, Response> adminPeers();

    /**
     * setSolc命令用来设置Solidity编译器的路径，以便当节点执行 eth_compileSolidity调用时可以找到编译器。Solidity编译器的 默认路径为/usr/bin/solc，因此只有当你希望使用一个其他路径时 才需要进行设置。
     *
     * @return setSolc命令的参数为指向Solidity编译器的绝对路径，并返回当前 solc编译器的版本字符串。
     */
    Request<?, StringResponse> adminSetSolc(String path);

    /**
     * startRPC方法启动一个基于HTTP的JSON RPC API服务器来处理客户端的 调用请求。所有的参数都是可选的：
     *
     * @param host 要监听的网络地址，默认值：localhost
     * @param port 要监听的网络端口，默认值：8545
     * @param cors 要使用的跨源资源共享头，默认值：""
     * @param apis 要透过该服务接口提供服务的API模块，默认值："eth,net,web3"
     * @return startRPC方法返回一个布尔值来表示HTTP RPC监听是否正确启动。需要指出的 是，任何时候都只能激活一个HTTP端结点。
     */
    Request<?, BooleanResponse> adminStartRPC(String host, Integer port, String cors, String apis);

    /**
     * startWS方法启动一个基于WebSocket的JSON RPC API服务来处理客户端的 调用请求。所有的参数都是可选的：
     *
     * @param host 要启动监听的网络地址，默认值：localhost
     * @param port 要启动监听的网络端口，默认值：8545
     * @param cors 要启用的跨源资源共享头：默认值：""
     * @param apis 要启用的API服务模块，默认值："eth,net,web3"
     * @return startWS方法返回一个布尔值来表征webSocket上的RPC监听是否启动成功。 注意在任何时刻都只能启用一个Websocket端结点。
     */
    Request<?, BooleanResponse> adminStartWS(String host, Integer port, String cors, String apis);

    /**
     * stopRPC方法用来关闭当前启动的HTTP RPC端结点。由于一个Geth节点 只能同时启动一个HTTP端结点，因此
     *
     * @return stopRPC方法不需要参数，其返回 值为一个布尔值，表示端结点是否成功关闭。
     */
    Request<?, BooleanResponse> adminStopRPC();

    /**
     * stopWS命令用来关闭当前启动的WebSocket RPC端结点。由于一个 Geth节点同时只能启用一个Websocket RPC端结点，因此
     *
     * @return stopWS命令 不需要参数，其返回值为一个表示是否成功关闭端结点的布尔值。
     */
    Request<?, BooleanResponse> adminStopWS();

    /**
     * miner的setExtra方法用来设置要包含在挖出区块中的额外的数据，最多32个字节。
     *
     * @param extra 额外参数
     * @return 其返回 值为一个布尔值
     */
    Request<?, BooleanResponse> minerSetExtra(String extra);

    /**
     * miner的setGasPrice方法用来设置挖矿时交易的最小可接受 gas价格，任何低于此设置值的交易将被排除在挖矿过程之外。
     *
     * @param hexNumber gas价格
     * @return 其返回 值为一个布尔值
     */
    Request<?, BooleanResponse> minerSetGasPrice(BigDecimal hexNumber);

    /**
     * miner的getHashrate方法返回当前挖矿的哈希生成速率
     *
     * @return 速率
     */
    Request<?, EthBigIntegerRessponse> minerGetHashrate();

    /**
     * txpool的content属性可以用来当前在池中的待定和排队交易清单，
     * 值为一个 包含两个字段的对象：pending和queued，每个字段都是一个关联数组。
     * 请注意，可能会有多个交易与同一个账户和nonce关联，当用户使用不同的 gas设置多次广播交易时可能会发生此现象。
     */
    Request txPoolContent();

    /**
     * txpool的inspect属性可以列出交易池中当前等待打包进下一个区块的交易的概要信息。
     * 该方法特别适合开发人员快速查看池中的交易以便发现潜在的问题。
     * inspect属性的值是一个包含两个字段的对象：pending和queued，每个字段都是一个 关联数组。
     * 请注意，可能会有多个交易与同一个账号和nonce相关联。当一个用户使用不同 的gas设置多次广播交易时，可能发生上述情况。
     */
    Request txPoolInspect();

    /**
     * txpool的status属性可以用来查询交易池中当前等待打包入下一个区块的 交易数量等信息。
     * status属性的值是一个包含两个字段的对象：pending和queued，每个字段的 值都是一个关联数组。
     */
    Request<?, EthTxpoolStatusResponse> txPoolStatus();
}
