package com.ybg.block.core.eth.config;

import com.ybg.block.framework.api.YbgGeth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.admin.Admin;
import org.web3j.protocol.http.HttpService;

@Slf4j
@Configuration
@ConditionalOnClass(EthHttpServiceProperties.class)
@EnableConfigurationProperties(EthHttpServiceProperties.class)
public class EthConfig {

    @Autowired
    private EthHttpServiceProperties ethHttpServiceProperties;

    @Bean
    @ConditionalOnProperty(name = "url", prefix = "eth.httpservice")
    public HttpService httpService() {
        // defaults to http://localhost:8545/
        log.info("加载eth配置，eth的客户端地址是：" + ethHttpServiceProperties.getUrl());
        return new HttpService(ethHttpServiceProperties.getUrl());
    }

    @Bean
    @ConditionalOnProperty(name = "url", prefix = "eth.httpservice")
    public Web3j web3j(HttpService httpService) {
        log.info("加载 web3j");
        return Web3j.build(httpService);
    }

    @Bean
    @ConditionalOnProperty(name = "url", prefix = "eth.httpservice")
    public Admin admin(HttpService httpService) {
        log.info("加载 超管");
        return Admin.build(httpService);
    }

    //查看所有核心依赖 https://blog.csdn.net/weixin_34357267/article/details/92411510
    @Bean
    @ConditionalOnProperty(name = "url", prefix = "eth.httpservice")
    public YbgGeth geth(HttpService httpService) {
        log.info("加载 Ybggeth");
        return YbgGeth.build(httpService);
    }


}
