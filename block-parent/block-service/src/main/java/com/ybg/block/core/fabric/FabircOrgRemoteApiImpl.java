package com.ybg.block.core.fabric;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.block.core.dbapi.entity.FabricLeague;
import com.ybg.block.core.dbapi.entity.FabricOrderer;
import com.ybg.block.core.dbapi.entity.FabricOrg;
import com.ybg.block.core.dbapi.service.FabricAppService;
import com.ybg.block.core.dbapi.service.FabricBlockService;
import com.ybg.block.core.dbapi.service.FabricCaService;
import com.ybg.block.core.dbapi.service.FabricChaincodeService;
import com.ybg.block.core.dbapi.service.FabricChannelService;
import com.ybg.block.core.dbapi.service.FabricLeagueService;
import com.ybg.block.core.dbapi.service.FabricOrdererService;
import com.ybg.block.core.dbapi.service.FabricOrgService;
import com.ybg.block.core.dbapi.service.FabricPeerService;
import com.ybg.share.core.fabric.FabircOrgRemoteApi;
import com.ybg.block.core.fabric.datamapper.FabricOrgDataMapper;
import com.ybg.share.core.fabric.dto.FabircOrgEditDTO;
import com.ybg.share.core.fabric.dto.FabircOrgQueryDTO;
import com.ybg.share.core.fabric.vo.FabricOrgVO;
import com.ybg.framework.vo.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 组织接口Api
 *
 * @author yanyu
 */
@Slf4j
@Service(version = "${dubbo.provider.version}", timeout = 120000, cache = "false")
public class FabircOrgRemoteApiImpl implements FabircOrgRemoteApi {
    @Autowired
    private FabricOrgService fabricOrgService;
    @Autowired
    private FabricLeagueService fabricLeagueService;

    @Autowired
    private FabricOrdererService fabricOrdererService;
    @Autowired
    private FabricPeerService fabricPeerService;
    @Autowired
    private FabricCaService fabricCaService;
    @Autowired
    private FabricChannelService fabricChannelService;
    @Autowired
    private FabricBlockService fabricBlockService;
    @Autowired
    private FabricChaincodeService fabricChaincodeService;
    @Autowired
    private FabricAppService fabricAppService;
    @Override
    public R<?> editOrg(FabircOrgEditDTO dto) {
        if (StrUtil.isBlank(dto.getMspId())) {
            return R.fail("mspId 不能为空 参见configtx文件中 -> Organizations-&Org1-Name");
        }
        if (dto.getTls() == null) {
            return R.fail("请选择是否启用tls");
        }
        if (dto.getId() == null && dto.getLeagueId() == null) {
            return R.fail("请选择联盟");
        }
        if (dto.getId() != null) {
            dto.setLeagueId(null);
        }
        if (dto.getLeagueId() != null) {
            FabricLeague byId = fabricLeagueService.getById(dto.getLeagueId());
            if (byId == null) {
                return R.fail("联盟不存在");
            }
        }
        FabricOrg fabricOrg = FabricOrgDataMapper.INSTANCE.toBean(dto);
        fabricOrgService.saveOrUpdate(fabricOrg);
        return R.success();
    }

    @Override
    public R<FabricOrgVO> getById(Long id) {
        FabricOrg fabricOrg = fabricOrgService.getById(id);
        return R.success(FabricOrgDataMapper.INSTANCE.toVO(fabricOrg));
    }

    @Override
    public R<?> deleteById(Long id) {
        fabricOrgService.removeById(id);
        //删除其他绑定的关系
        Map<String ,Object> columnMap= new LinkedHashMap<>();
        columnMap.put(FabricOrderer.ORG_ID,id);
        fabricOrdererService.removeByMap(columnMap);
        fabricPeerService.removeByMap(columnMap);
        fabricCaService.removeByMap(columnMap);
        fabricChannelService.removeByMap(columnMap);
        fabricBlockService.removeByMap(columnMap);
        fabricChaincodeService.removeByMap(columnMap);
        fabricAppService.removeByMap(columnMap);
        return R.success();
    }

    @Override
    public R<Page<FabricOrgVO>> page(FabircOrgQueryDTO dto) {
        // todo  这里有四个表join,违反阿里的java 规范 以后再改
        Page<FabricOrgVO> page= fabricOrgService.pageQuery(dto);
        return R.success(page);
    }

}
