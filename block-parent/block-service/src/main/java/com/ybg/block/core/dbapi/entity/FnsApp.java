package com.ybg.block.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 应用
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
@ApiModel(value = "应用")
public class FnsApp extends Model<FnsApp> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Integer id;
    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;
    /**
     * Key
     */
    @ApiModelProperty(value = "Key")
    private String appKey;
    /**
     * 链码ID
     */
    @ApiModelProperty(value = "链码ID")
    private Integer chaincodeId;
    /**
     * 创建日期
     */
    @ApiModelProperty(value = "创建日期")
    private String createDate;
    /**
     * 修改日期
     */
    @ApiModelProperty(value = "修改日期")
    private String modifyDate;
    /**
     * 是否启用 0禁用，1启用
     */
    @ApiModelProperty(value = "是否启用 0禁用，1启用")
    private Integer active;


    /**
     * 获取主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取Key
     */
    public String getAppKey() {
        return appKey;
    }

    /**
     * 设置Key
     */

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    /**
     * 获取链码ID
     */
    public Integer getChaincodeId() {
        return chaincodeId;
    }

    /**
     * 设置链码ID
     */

    public void setChaincodeId(Integer chaincodeId) {
        this.chaincodeId = chaincodeId;
    }

    /**
     * 获取创建日期
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建日期
     */

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取修改日期
     */
    public String getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置修改日期
     */

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * 获取是否启用 0禁用，1启用
     */
    public Integer getActive() {
        return active;
    }

    /**
     * 设置是否启用 0禁用，1启用
     */

    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 名称列的数据库字段名称
     */
    public static final String NAME = "name";

    /**
     * Key列的数据库字段名称
     */
    public static final String APP_KEY = "app_key";

    /**
     * 链码ID列的数据库字段名称
     */
    public static final String CHAINCODE_ID = "chaincode_id";

    /**
     * 创建日期列的数据库字段名称
     */
    public static final String CREATE_DATE = "create_date";

    /**
     * 修改日期列的数据库字段名称
     */
    public static final String MODIFY_DATE = "modify_date";

    /**
     * 是否启用 0禁用，1启用列的数据库字段名称
     */
    public static final String ACTIVE = "active";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FnsApp{" +
                "id=" + id +
                ", name=" + name +
                ", appKey=" + appKey +
                ", chaincodeId=" + chaincodeId +
                ", createDate=" + createDate +
                ", modifyDate=" + modifyDate +
                ", active=" + active +
                "}";
    }
}
