package com.ybg.block.core.fabric.datamapper;

import com.ybg.block.core.dbapi.entity.FabricOrg;
import com.ybg.share.core.fabric.dto.FabircOrgEditDTO;
import com.ybg.share.core.fabric.vo.FabricOrgVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author yanyu
 */
@Mapper
public interface FabricOrgDataMapper {
    FabricOrgDataMapper INSTANCE = Mappers.getMapper(FabricOrgDataMapper.class);

    FabricOrgVO toVO(FabricOrg bean);

    List<FabricOrgVO> toVOList(List<FabricOrgVO> list);

    FabricOrg toBean(FabircOrgEditDTO dto);
}
