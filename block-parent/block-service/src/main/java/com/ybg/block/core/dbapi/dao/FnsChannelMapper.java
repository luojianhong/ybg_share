package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FnsChannel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ybg.block.core.dbapi.vo.FnsChannelVO;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsChannelMapper extends BaseMapper<FnsChannel> {
    @Insert("insert into fns_channel (name,block_listener,callback_location,peer_id,date) values " +
            "(#{c.name},#{c.blockListener},#{c.callbackLocation},#{c.peerId},#{c.date})")
    int add(@Param("c") FnsChannel channel);

    @Update("update fns_channel set name=#{c.name}, block_listener=#{c.blockListener}, " +
            "callback_location=#{c.callbackLocation} where id=#{c.id}")
    int update(@Param("c") FnsChannel channel);

    @Update("update fns_channel set height=#{height} where id=#{id}")
    int updateHeight(@Param("id") int id, @Param("height") int height);

    @Select("select count(name) from fns_channel where peer_id=#{id}")
    int count(@Param("id") int id);

    @Select("select count(name) from fns_channel")
    int countAll();

    @Delete("delete from fns_channel where id=#{id}")
    int delete(@Param("id") int id);

    @Delete("delete from fns_channel where peer_id=#{peerId}")
    int deleteAll(@Param("peerId") int peerId);

    @Select("select id,name,block_listener,callback_location,peer_id,date from fns_channel where name=#{c.name} and peer_id=#{c.peerId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "blockListener", column = "block_listener"),
            @Result(property = "callbackLocation", column = "callback_location"),
            @Result(property = "peerId", column = "peer_id"),
            @Result(property = "date", column = "date")
    })
    FnsChannel check(@Param("c") FnsChannel channel);

    @Select("select id,name,block_listener,callback_location,peer_id,date from fns_channel where id=#{id}")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "name", column = "name"),
//            @Result(property = "blockListener", column = "block_listener"),
//            @Result(property = "callbackLocation", column = "callback_location"),
//            @Result(property = "peerId", column = "peer_id"),
//            @Result(property = "date", column = "date")
//    })
    FnsChannelVO get(@Param("id") int id);

    @Select("select id,name,block_listener,callback_location,peer_id,date from fns_channel where peer_id=#{id}")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "name", column = "name"),
//            @Result(property = "blockListener", column = "block_listener"),
//            @Result(property = "callbackLocation", column = "callback_location"),
//            @Result(property = "peerId", column = "peer_id"),
//            @Result(property = "date", column = "date")
//    })
    List<FnsChannelVO> list(@Param("id") int id);

    @Select("select id,name,block_listener,callback_location,height,peer_id,date from fns_channel")
//    @Results({
//            @Result(property = "blockListener", column = "block_listener"),
//            @Result(property = "callbackLocation", column = "callback_location"),
//            @Result(property = "peerId", column = "peer_id")
//    })
    List<FnsChannelVO> listAll();
}
