package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FabricCa;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 证书授权中心 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricCaService extends IService<FabricCa> {


}
