package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FnsChaincode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ybg.block.core.dbapi.vo.FnsChaincodeVO;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsChaincodeMapper extends BaseMapper<FnsChaincode> {
    @Insert("insert into fns_chaincode (name,path,version,proposal_wait_time,channel_id,cc,date,source,policy," +
            "chaincode_event_listener,callback_location,events) values " +
            "(#{c.name},#{c.path},#{c.version},#{c.proposalWaitTime},#{c.channelId},#{c.cc},#{c.date},#{c.source},#{c.policy}," +
            "#{c.chaincodeEventListener},#{c.callbackLocation},#{c.events})")
    int add(@Param("c") FnsChaincodeVO chaincode);

    @Update("update fns_chaincode set name=#{c.name}, path=#{c.path}, version=#{c.version}, " +
            "proposal_wait_time=#{c.proposalWaitTime}, chaincode_event_listener=#{c.chaincodeEventListener}, " +
            "callback_location=#{c.callbackLocation}, events=#{c.events} where id=#{c.id}")
    int update(@Param("c") FnsChaincodeVO chaincode);

    @Update("update fns_chaincode set name=#{c.name}, path=#{c.path}, version=#{c.version}, " +
            "proposal_wait_time=#{c.proposalWaitTime}, source=#{c.source}, policy=#{c.policy}, " +
            "chaincode_event_listener=#{c.chaincodeEventListener}, callback_location=#{c.callbackLocation}, " +
            "events=#{c.events} where id=#{c.id}")
    int updateForUpgrade(@Param("c") FnsChaincodeVO chaincode);

    @Select("select count(name) from fns_chaincode where channel_id=#{id}")
    int count(@Param("id") int id);

    @Select("select count(name) from fns_chaincode")
    int countAll();

    @Delete("delete from fns_chaincode where id=#{id}")
    int delete(@Param("id") int id);

    @Delete("delete from fns_chaincode where channel_id=#{channelId}")
    int deleteAll(@Param("channelId") int channelId);

    @Select("select id,name,path,version,proposal_wait_time,channel_id,cc,date,source,policy," +
            "chaincode_event_listener,callback_location,events from fns_chaincode " +
            "where name=#{c.name} and path=#{c.path} and version=#{c.version} and channel_id=#{c.channelId}")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "name", column = "name"),
//            @Result(property = "path", column = "path"),
//            @Result(property = "version", column = "version"),
//            @Result(property = "proposalWaitTime", column = "proposal_wait_time"),
//            @Result(property = "channelId", column = "channel_id"),
//            @Result(property = "cc", column = "cc"),
//            @Result(property = "chaincodeEventListener", column = "chaincode_event_listener"),
//            @Result(property = "callbackLocation", column = "callback_location"),
//            @Result(property = "events", column = "events"),
//            @Result(property = "date", column = "date"),
//            @Result(property = "source", column = "source"),
//            @Result(property = "policy", column = "policy")
//    })
    FnsChaincodeVO check(@Param("c") FnsChaincodeVO chaincode);

    @Select("select id,name,path,version,proposal_wait_time,channel_id,cc,date,source,policy," +
            "chaincode_event_listener,callback_location,events from fns_chaincode where id=#{id}")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "name", column = "name"),
//            @Result(property = "path", column = "path"),
//            @Result(property = "version", column = "version"),
//            @Result(property = "proposalWaitTime", column = "proposal_wait_time"),
//            @Result(property = "channelId", column = "channel_id"),
//            @Result(property = "cc", column = "cc"),
//            @Result(property = "chaincodeEventListener", column = "chaincode_event_listener"),
//            @Result(property = "callbackLocation", column = "callback_location"),
//            @Result(property = "events", column = "events"),
//            @Result(property = "date", column = "date"),
//            @Result(property = "source", column = "source"),
//            @Result(property = "policy", column = "policy")
//    })
    FnsChaincodeVO get(@Param("id") int id);

    @Select("select id,name,path,version,proposal_wait_time,channel_id,cc,date,source,policy," +
            "chaincode_event_listener,callback_location,events from fns_chaincode where cc=#{cc}")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "name", column = "name"),
//            @Result(property = "path", column = "path"),
//            @Result(property = "version", column = "version"),
//            @Result(property = "proposalWaitTime", column = "proposal_wait_time"),
//            @Result(property = "channelId", column = "channel_id"),
//            @Result(property = "cc", column = "cc"),
//            @Result(property = "chaincodeEventListener", column = "chaincode_event_listener"),
//            @Result(property = "callbackLocation", column = "callback_location"),
//            @Result(property = "events", column = "events"),
//            @Result(property = "date", column = "date"),
//            @Result(property = "source", column = "source"),
//            @Result(property = "policy", column = "policy")
//    })
    FnsChaincode getByCC(@Param("cc") String cc);

    @Select("select id,name,path,version,proposal_wait_time,channel_id,cc,date,source,policy," +
            "chaincode_event_listener,callback_location,events from fns_chaincode where channel_id=#{id}")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "name", column = "name"),
//            @Result(property = "path", column = "path"),
//            @Result(property = "version", column = "version"),
//            @Result(property = "proposalWaitTime", column = "proposal_wait_time"),
//            @Result(property = "channelId", column = "channel_id"),
//            @Result(property = "cc", column = "cc"),
//            @Result(property = "chaincodeEventListener", column = "chaincode_event_listener"),
//            @Result(property = "callbackLocation", column = "callback_location"),
//            @Result(property = "events", column = "events"),
//            @Result(property = "date", column = "date"),
//            @Result(property = "source", column = "source"),
//            @Result(property = "policy", column = "policy")
//    })
    List<FnsChaincode> list(@Param("id") int id);

    @Select("select id,name,path,version,proposal_wait_time,channel_id,cc,date,source,policy," +
            "chaincode_event_listener,callback_location,events from fns_chaincode")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "name", column = "name"),
//            @Result(property = "path", column = "path"),
//            @Result(property = "version", column = "version"),
//            @Result(property = "proposalWaitTime", column = "proposal_wait_time"),
//            @Result(property = "channelId", column = "channel_id"),
//            @Result(property = "cc", column = "cc"),
//            @Result(property = "chaincodeEventListener", column = "chaincode_event_listener"),
//            @Result(property = "callbackLocation", column = "callback_location"),
//            @Result(property = "events", column = "events"),
//            @Result(property = "date", column = "date"),
//            @Result(property = "source", column = "source"),
//            @Result(property = "policy", column = "policy")
//    })
    List<FnsChaincodeVO> listAll();
}
