package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FnsLeague;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.block.core.dbapi.vo.FnsLeagueVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsLeagueService extends IService<FnsLeague> {
    int add(FnsLeague league);

    int update(FnsLeague league);

    List<FnsLeagueVO> listAll();

    FnsLeague get(int id);

    int delete(int id);
}
