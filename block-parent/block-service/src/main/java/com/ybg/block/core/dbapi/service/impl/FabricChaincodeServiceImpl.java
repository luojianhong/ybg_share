package com.ybg.block.core.dbapi.service.impl;

import com.ybg.block.core.dbapi.entity.FabricChaincode;
import com.ybg.block.core.dbapi.dao.FabricChaincodeMapper;
import com.ybg.block.core.dbapi.service.FabricChaincodeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 链码表 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
//@Service(version = "1.0.0", interfaceClass = FabricChaincodeService.class)
@Service
public class FabricChaincodeServiceImpl extends ServiceImpl<FabricChaincodeMapper, FabricChaincode> implements FabricChaincodeService {


}
