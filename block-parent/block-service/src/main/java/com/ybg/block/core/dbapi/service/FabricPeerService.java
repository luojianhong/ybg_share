package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FabricPeer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 节点 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricPeerService extends IService<FabricPeer> {

}
