package com.ybg.block.core.dbapi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 钱包账户
 * </p>
 *
 * @author yanyu
 * @since 2019-11-16
 */
@ApiModel(value = "钱包账户")
@TableName("block_wallet_account")
public class BlockWalletAccount extends Model<BlockWalletAccount> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Integer id;
    /**
     * 账户
     */
    @ApiModelProperty(value = "账户")
    private String account;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 钱包类型
     */
    @ApiModelProperty(value = "钱包类型")
    private String walletType;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;


    /**
     * 获取主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取账户
     */
    public String getAccount() {
        return account;
    }

    /**
     * 设置账户
     */

    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * 获取创建时间
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     */

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取钱包类型
     */
    public String getWalletType() {
        return walletType;
    }

    /**
     * 设置钱包类型
     */

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    /**
     * 获取密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     */

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 账户列的数据库字段名称
     */
    public static final String ACCOUNT = "account";

    /**
     * 创建时间列的数据库字段名称
     */
    public static final String CREATE_TIME = "create_time";

    /**
     * 钱包类型列的数据库字段名称
     */
    public static final String WALLET_TYPE = "wallet_type";

    /**
     * 密码列的数据库字段名称
     */
    public static final String PASSWORD = "password";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BlockWalletAccount{" +
                "id=" + id +
                ", account=" + account +
                ", createTime=" + createTime +
                ", walletType=" + walletType +
                ", password=" + password +
                "}";
    }
}
