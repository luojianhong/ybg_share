package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FnsApp;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsAppService extends IService<FnsApp> {
    /**
     *  简单的增加
     */

    int add(FnsApp app);

    /**
     * 简单的新增
     * @param app
     * @return
     */
    int update(FnsApp app);

    /**
     * 简单的更新
     * @param id
     * @return
     */
    int updateKey(int id);

    List<FnsApp> list(int id);

    FnsApp get(int id);

    int delete(int id);

    int deleteAll(int id);

    int count();

    int countById(int id);
}
