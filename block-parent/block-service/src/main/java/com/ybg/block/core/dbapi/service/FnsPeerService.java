package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FnsPeer;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.block.core.dbapi.vo.FnsOrgVO;
import com.ybg.block.core.dbapi.vo.FnsPeerVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsPeerService extends IService<FnsPeer> {
    int add(FnsPeerVO peer, MultipartFile serverCrtFile, MultipartFile clientCertFile, MultipartFile clientKeyFile);

    int update(FnsPeerVO peer, MultipartFile serverCrtFile, MultipartFile clientCertFile, MultipartFile clientKeyFile);

    List<FnsPeerVO> listAll();

    List<FnsPeerVO> listById(int id);

    FnsPeerVO get(int id);

    int countById(int id);

    int count();

    int delete(int id);

    List<FnsOrgVO> listOrgByOrgId(int orgId);

    List<FnsOrgVO> getForPeerAndOrderer();

    FnsPeerVO resetPeer(FnsPeerVO peer);
}
