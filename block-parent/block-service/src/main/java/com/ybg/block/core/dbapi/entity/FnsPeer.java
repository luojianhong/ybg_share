package com.ybg.block.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 节点
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
@ApiModel(value = "节点")
public class FnsPeer extends Model<FnsPeer> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Integer id;
    @ApiModelProperty(value = "")
    private String name;
    /**
     * 节点访问地址
     */
    @ApiModelProperty(value = "节点访问地址")
    private String location;
    /**
     * 节点事件监听访问地址
     */
    @ApiModelProperty(value = "节点事件监听访问地址")
    private String eventHubLocation;
    /**
     * 组织ID
     */
    @ApiModelProperty(value = "组织ID")
    private Integer orgId;
    /**
     * Peer TLS证书
     */
    @ApiModelProperty(value = "Peer TLS证书")
    private String serverCrtPath;
    /**
     * Peer User Client Cert
     */
    @ApiModelProperty(value = "Peer User Client Cert")
    private String clientCertPath;
    /**
     * Peer User Client Key
     */
    @ApiModelProperty(value = "Peer User Client Key")
    private String clientKeyPath;
    /**
     * 创建日期
     */
    @ApiModelProperty(value = "创建日期")
    private String date;


    /**
     * 获取主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取节点访问地址
     */
    public String getLocation() {
        return location;
    }

    /**
     * 设置节点访问地址
     */

    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * 获取节点事件监听访问地址
     */
    public String getEventHubLocation() {
        return eventHubLocation;
    }

    /**
     * 设置节点事件监听访问地址
     */

    public void setEventHubLocation(String eventHubLocation) {
        this.eventHubLocation = eventHubLocation;
    }

    /**
     * 获取组织ID
     */
    public Integer getOrgId() {
        return orgId;
    }

    /**
     * 设置组织ID
     */

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    /**
     * 获取Peer TLS证书
     */
    public String getServerCrtPath() {
        return serverCrtPath;
    }

    /**
     * 设置Peer TLS证书
     */

    public void setServerCrtPath(String serverCrtPath) {
        this.serverCrtPath = serverCrtPath;
    }

    /**
     * 获取Peer User Client Cert
     */
    public String getClientCertPath() {
        return clientCertPath;
    }

    /**
     * 设置Peer User Client Cert
     */

    public void setClientCertPath(String clientCertPath) {
        this.clientCertPath = clientCertPath;
    }

    /**
     * 获取Peer User Client Key
     */
    public String getClientKeyPath() {
        return clientKeyPath;
    }

    /**
     * 设置Peer User Client Key
     */

    public void setClientKeyPath(String clientKeyPath) {
        this.clientKeyPath = clientKeyPath;
    }

    /**
     * 获取创建日期
     */
    public String getDate() {
        return date;
    }

    /**
     * 设置创建日期
     */

    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    public static final String NAME = "name";

    /**
     * 节点访问地址列的数据库字段名称
     */
    public static final String LOCATION = "location";

    /**
     * 节点事件监听访问地址列的数据库字段名称
     */
    public static final String EVENT_HUB_LOCATION = "event_hub_location";

    /**
     * 组织ID列的数据库字段名称
     */
    public static final String ORG_ID = "org_id";

    /**
     * Peer TLS证书列的数据库字段名称
     */
    public static final String SERVER_CRT_PATH = "server_crt_path";

    /**
     * Peer User Client Cert列的数据库字段名称
     */
    public static final String CLIENT_CERT_PATH = "client_cert_path";

    /**
     * Peer User Client Key列的数据库字段名称
     */
    public static final String CLIENT_KEY_PATH = "client_key_path";

    /**
     * 创建日期列的数据库字段名称
     */
    public static final String DATE = "date";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FnsPeer{" +
                "id=" + id +
                ", name=" + name +
                ", location=" + location +
                ", eventHubLocation=" + eventHubLocation +
                ", orgId=" + orgId +
                ", serverCrtPath=" + serverCrtPath +
                ", clientCertPath=" + clientCertPath +
                ", clientKeyPath=" + clientKeyPath +
                ", date=" + date +
                "}";
    }
}
