package com.ybg.block.core.dbapi.service.impl;

import com.ybg.block.core.dbapi.entity.FnsLeague;
import com.ybg.block.core.dbapi.service.FnsLeagueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.block.core.dbapi.vo.FnsLeagueVO;
import com.ybg.block.core.fabricbak.utils.CacheUtil;
import com.ybg.block.core.fabricbak.utils.DateUtil;
import com.ybg.block.core.fabricbak.utils.DeleteUtil;
import com.ybg.block.core.dbapi.dao.FnsAppMapper;
import com.ybg.block.core.dbapi.dao.FnsBlockMapper;
import com.ybg.block.core.dbapi.dao.FnsCaMapper;
import com.ybg.block.core.dbapi.dao.FnsChaincodeMapper;
import com.ybg.block.core.dbapi.dao.FnsChannelMapper;
import com.ybg.block.core.dbapi.dao.FnsLeagueMapper;
import com.ybg.block.core.dbapi.dao.FnsOrdererMapper;
import com.ybg.block.core.dbapi.dao.FnsOrgMapper;
import com.ybg.block.core.dbapi.dao.FnsPeerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
//@Service(version = "1.0.0", interfaceClass = FnsLeagueService.class)
@Service
public class FnsLeagueServiceImpl extends ServiceImpl<FnsLeagueMapper, FnsLeague> implements FnsLeagueService {
    @Autowired
    private FnsLeagueMapper leagueMapper;
    @Autowired
    private FnsOrgMapper orgMapper;
    @Autowired
    private FnsPeerMapper peerMapper;
    @Autowired
    private FnsCaMapper caMapper;
    @Autowired
    private FnsOrdererMapper ordererMapper;
    @Autowired
    private FnsChannelMapper channelMapper;
    @Autowired
    private FnsChaincodeMapper chaincodeMapper;
    @Autowired
    private FnsAppMapper appMapper;
    @Autowired
    private FnsBlockMapper blockMapper;

    @Override
    public int add(FnsLeague leagueInfo) {
        if (StringUtils.isEmpty(leagueInfo.getName())) {
            return 0;
        }
        leagueInfo.setDate(DateUtil.getCurrent("yyyy-MM-dd"));
        CacheUtil.removeHome();
        return leagueMapper.add(leagueInfo);
    }

    @Override
    public int update(FnsLeague leagueInfo) {
        CacheUtil.removeFlagCA(leagueInfo.getId(), peerMapper, caMapper);
        CacheUtil.removeHome();
        return leagueMapper.update(leagueInfo);
    }

    @Override
    public List<FnsLeagueVO> listAll() {
        return leagueMapper.listAll();
    }

    @Override
    public FnsLeague get(int id) {
        return leagueMapper.get(id);
    }

    @Override
    public int delete(int id) {
        return DeleteUtil.obtain().deleteLeague(id, leagueMapper, orgMapper, ordererMapper,
                peerMapper, caMapper, channelMapper, chaincodeMapper, appMapper, blockMapper);
    }
}
