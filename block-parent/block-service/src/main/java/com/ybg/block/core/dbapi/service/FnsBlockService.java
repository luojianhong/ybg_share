package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FnsBlock;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.block.core.dbapi.vo.FnsBlockVO;
import com.ybg.block.core.dbapi.vo.FnsChannelVO;
import com.ybg.block.core.fabricbak.bean.ChannelBlockList;
import com.ybg.block.core.fabricbak.bean.ChannelPercent;
import com.ybg.block.core.fabricbak.bean.Curve;
import com.ybg.block.core.fabricbak.bean.DayStatistics;
import com.ybg.block.core.fabricbak.bean.Platform;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsBlockService extends IService<FnsBlock> {
    int add(FnsBlock block);

    int addBlockList(List<FnsBlockVO> blocks);

    List<ChannelPercent> getChannelPercents(List<FnsChannelVO> channels);

    List<ChannelBlockList> getChannelBlockLists(List<FnsChannelVO> channels);

    DayStatistics getDayStatistics();

    Platform getPlatform();

    FnsBlockVO getByChannelId(int channelId);

    List<FnsBlockVO> getLimit(int limit);

    Curve get20CountList();

    Curve get20TxCountList();

    Curve get20RWCountList();
}
