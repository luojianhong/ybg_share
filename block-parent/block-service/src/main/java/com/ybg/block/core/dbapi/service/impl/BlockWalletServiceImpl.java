package com.ybg.block.core.dbapi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.block.core.dbapi.entity.BlockWallet;
import com.ybg.block.core.dbapi.dao.BlockWalletMapper;
import com.ybg.block.core.dbapi.service.BlockWalletService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.share.core.enums.WalletType;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;

/**
 * <p>
 * 钱包表 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-15
 */
//@Service(version = "1.0.0", interfaceClass = BlockWalletService.class)
@Service
public class BlockWalletServiceImpl extends ServiceImpl<BlockWalletMapper, BlockWallet> implements BlockWalletService {

    @Override
    public BlockWallet getByAddress(String address) {
        QueryWrapper<BlockWallet> wrapper = new QueryWrapper<>();
        wrapper.eq(BlockWallet.ADDRESS, address);
        wrapper.eq(BlockWallet.WALLET_TYPE, WalletType.ETH.getValue());
        return getOne(wrapper);
    }
}
