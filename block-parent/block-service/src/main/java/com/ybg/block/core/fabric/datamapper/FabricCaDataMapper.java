package com.ybg.block.core.fabric.datamapper;

import com.ybg.block.core.dbapi.entity.FabricCa;
import com.ybg.share.core.fabric.vo.FabricCaVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface FabricCaDataMapper {

    FabricCaDataMapper INSTANCE = Mappers.getMapper(FabricCaDataMapper.class);

    FabricCaVO toVO(FabricCa bean);

    List<FabricCaVO> toVOList(List<FabricCa> list);
}
