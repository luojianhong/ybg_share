/*
 * Copyright (c) 2018. Aberic - All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ybg.block.core.fabricbak.scheduled;


import com.ybg.block.core.dbapi.service.FnsAppService;
import com.ybg.block.core.dbapi.service.FnsBlockService;
import com.ybg.block.core.dbapi.service.FnsCaService;
import com.ybg.block.core.dbapi.service.FnsChaincodeService;
import com.ybg.block.core.dbapi.service.FnsChannelService;
import com.ybg.block.core.dbapi.service.FnsLeagueService;
import com.ybg.block.core.dbapi.service.FnsOrdererService;
import com.ybg.block.core.dbapi.service.FnsOrgService;
import com.ybg.block.core.dbapi.service.FnsPeerService;
import com.ybg.block.core.fabricbak.service.TraceService;
import com.ybg.block.core.fabricbak.utils.BlockUtil;
import com.ybg.block.core.fabricbak.utils.DataUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


/**
 * 描述：
 *
 * @author : Aberic 【2018-08-10 11:13】
 */
@Component
@Slf4j
public class ScheduledTasks {

    @Autowired
    private FnsLeagueService leagueService;
    @Autowired
    private FnsOrgService orgService;
    @Autowired
    private FnsOrdererService ordererService;
    @Autowired
    private FnsPeerService peerService;
    @Autowired
    private FnsCaService caService;
    @Autowired
    private FnsChannelService channelService;
    @Autowired
    private FnsChaincodeService chaincodeService;
    @Autowired
    private FnsAppService appService;
    @Autowired
    private TraceService traceService;
    @Autowired
    private FnsBlockService blockService;

    //fixedDelay = x 表示当前方法执行完毕x ms后，Spring scheduling会再次调用该方法
    @Scheduled(fixedDelay = 15000)
    public void homeUpgrade() {
        log.info("===============>>>>>>>>>> home upgrade start <<<<<<<<<<===============");
        DataUtil.obtain().home(leagueService, orgService, ordererService,
                peerService, caService, channelService, chaincodeService,
                appService, blockService);
        log.info("===============>>>>>>>>>> home upgrade end   <<<<<<<<<<===============");
    }

    @Scheduled(fixedDelay = 60000)
    public void checkChannel() {
        log.info("===============>>>>>>>>>> check channel start <<<<<<<<<<===============");
        BlockUtil.obtain().checkChannel(channelService, caService, blockService, traceService, channelService.listAll());
        log.info("===============>>>>>>>>>> check channel end   <<<<<<<<<<===============");
    }

}
