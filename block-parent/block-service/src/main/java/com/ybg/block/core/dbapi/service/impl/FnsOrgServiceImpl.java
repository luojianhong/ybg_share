package com.ybg.block.core.dbapi.service.impl;

import com.ybg.block.core.dbapi.entity.FnsOrg;
import com.ybg.block.core.dbapi.service.FnsOrgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.block.core.dbapi.vo.FnsLeagueVO;
import com.ybg.block.core.dbapi.vo.FnsOrgVO;
import com.ybg.block.core.fabricbak.utils.CacheUtil;
import com.ybg.block.core.fabricbak.utils.DateUtil;
import com.ybg.block.core.fabricbak.utils.DeleteUtil;
import com.ybg.block.core.fabricbak.utils.FabricHelper;
import com.ybg.block.core.dbapi.dao.FnsAppMapper;
import com.ybg.block.core.dbapi.dao.FnsBlockMapper;
import com.ybg.block.core.dbapi.dao.FnsCaMapper;
import com.ybg.block.core.dbapi.dao.FnsChaincodeMapper;
import com.ybg.block.core.dbapi.dao.FnsChannelMapper;
import com.ybg.block.core.dbapi.dao.FnsLeagueMapper;
import com.ybg.block.core.dbapi.dao.FnsOrdererMapper;
import com.ybg.block.core.dbapi.dao.FnsOrgMapper;
import com.ybg.block.core.dbapi.dao.FnsPeerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
//@Service(version = "1.0.0", interfaceClass = FnsOrgService.class)
@Service
public class FnsOrgServiceImpl extends ServiceImpl<FnsOrgMapper, FnsOrg> implements FnsOrgService {
    @Autowired
    private FnsLeagueMapper leagueMapper;
    @Autowired
    private FnsOrgMapper orgMapper;
    @Autowired
    private FnsPeerMapper peerMapper;
    @Autowired
    private FnsCaMapper caMapper;
    @Autowired
    private FnsOrdererMapper ordererMapper;
    @Autowired
    private FnsChannelMapper channelMapper;
    @Autowired
    private FnsChaincodeMapper chaincodeMapper;
    @Autowired
    private FnsAppMapper appMapper;
    @Autowired
    private FnsBlockMapper blockMapper;


    @Override
    public int add(FnsOrg org) {
        if (StringUtils.isEmpty(org.getMspId())) {
            return 0;
        }
        org.setDate(DateUtil.getCurrent("yyyy-MM-dd"));
        CacheUtil.removeHome();
        return orgMapper.add(org);
    }

    @Override
    public int update(FnsOrg org) {
        FabricHelper.obtain().removeChaincodeManager(peerMapper.list(org.getId()), channelMapper, chaincodeMapper);
        CacheUtil.removeHome();
        return orgMapper.update(org);
    }

    @Override
    public List<FnsOrgVO> listAll() {
        List<FnsOrgVO> orgs = orgMapper.listAll();
        for (FnsOrgVO org : orgs) {
            org.setOrdererCount(ordererMapper.count(org.getId()));
            org.setPeerCount(peerMapper.count(org.getId()));
            org.setLeagueName(leagueMapper.get(org.getLeagueId()).getName());
        }
        return orgs;
    }

    @Override
    public List<FnsOrgVO> listById(int id) {
        return orgMapper.list(id);
    }

    @Override
    public FnsOrg get(int id) {
        return orgMapper.get(id);
    }

    @Override
    public int countById(int id) {
        return orgMapper.count(id);
    }

    @Override
    public int count() {
        return orgMapper.countAll();
    }

    @Override
    public int delete(int id) {
        return DeleteUtil.obtain().deleteOrg(id, orgMapper, ordererMapper, peerMapper, caMapper, channelMapper, chaincodeMapper, appMapper, blockMapper);
    }

    @Override
    public List<FnsLeagueVO> listAllLeague() {
        return leagueMapper.listAll();
    }
}
