package com.ybg.block.core.dbapi.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 节点
 * </p>
 *
 * @author yanyu
 * @since 2019-11-29
 */
@ApiModel(value="节点") 
public class FabricPeer extends Model<FabricPeer> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
@ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 节点名称
     */
@ApiModelProperty(value = "节点名称")
    private String name;
    /**
     * 节点访问地址
     */
@ApiModelProperty(value = "节点访问地址")
    private String location;
    /**
     * 节点服务事件路径 根据自身设置实际情况修改，host:port的格式
     */
@ApiModelProperty(value = "节点服务事件路径 根据自身设置实际情况修改，host:port的格式")
    private String eventHubLocation;
    /**
     * 组织ID
     */
@ApiModelProperty(value = "组织ID")
    private Long orgId;
    /**
     * Peer TLS证书
     */
@ApiModelProperty(value = "Peer TLS证书")
    private String serverCrtPath;
    /**
     * Peer User Client Cert
     */
@ApiModelProperty(value = "Peer User Client Cert")
    private String clientCertPath;
    /**
     * Peer User Client Key
     */
@ApiModelProperty(value = "Peer User Client Key")
    private String clientKeyPath;
    /**
     * 创建日期
     */
@ApiModelProperty(value = "创建日期")
    private LocalDateTime createTime;
    /**
     * 联盟ID(冗余)
     */
@ApiModelProperty(value = "联盟ID(冗余)")
    private Long leagueId;
    /**
     * 组织名称(冗余)
     */
@ApiModelProperty(value = "组织名称(冗余)")
    private String orgName;
    /**
     * 联盟名称(冗余)
     */
@ApiModelProperty(value = "联盟名称(冗余)")
    private String leagueName;
    /**
     * 通道数量(冗余)
     */
@ApiModelProperty(value = "通道数量(冗余)")
    private Integer channelCount;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取节点名称
     */
    public String getName() {
        return name;
    }
    /**
     * 设置节点名称
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取节点访问地址
     */
    public String getLocation() {
        return location;
    }
    /**
     * 设置节点访问地址
     */

    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * 获取节点服务事件路径 根据自身设置实际情况修改，host:port的格式
     */
    public String getEventHubLocation() {
        return eventHubLocation;
    }
    /**
     * 设置节点服务事件路径 根据自身设置实际情况修改，host:port的格式
     */

    public void setEventHubLocation(String eventHubLocation) {
        this.eventHubLocation = eventHubLocation;
    }

    /**
     * 获取组织ID
     */
    public Long getOrgId() {
        return orgId;
    }
    /**
     * 设置组织ID
     */

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 获取Peer TLS证书
     */
    public String getServerCrtPath() {
        return serverCrtPath;
    }
    /**
     * 设置Peer TLS证书
     */

    public void setServerCrtPath(String serverCrtPath) {
        this.serverCrtPath = serverCrtPath;
    }

    /**
     * 获取Peer User Client Cert
     */
    public String getClientCertPath() {
        return clientCertPath;
    }
    /**
     * 设置Peer User Client Cert
     */

    public void setClientCertPath(String clientCertPath) {
        this.clientCertPath = clientCertPath;
    }

    /**
     * 获取Peer User Client Key
     */
    public String getClientKeyPath() {
        return clientKeyPath;
    }
    /**
     * 设置Peer User Client Key
     */

    public void setClientKeyPath(String clientKeyPath) {
        this.clientKeyPath = clientKeyPath;
    }

    /**
     * 获取创建日期
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }
    /**
     * 设置创建日期
     */

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取联盟ID(冗余)
     */
    public Long getLeagueId() {
        return leagueId;
    }
    /**
     * 设置联盟ID(冗余)
     */

    public void setLeagueId(Long leagueId) {
        this.leagueId = leagueId;
    }

    /**
     * 获取组织名称(冗余)
     */
    public String getOrgName() {
        return orgName;
    }
    /**
     * 设置组织名称(冗余)
     */

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * 获取联盟名称(冗余)
     */
    public String getLeagueName() {
        return leagueName;
    }
    /**
     * 设置联盟名称(冗余)
     */

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    /**
     * 获取通道数量(冗余)
     */
    public Integer getChannelCount() {
        return channelCount;
    }
    /**
     * 设置通道数量(冗余)
     */

    public void setChannelCount(Integer channelCount) {
        this.channelCount = channelCount;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 节点名称列的数据库字段名称
     */
    public static final String NAME = "name";

    /**
     * 节点访问地址列的数据库字段名称
     */
    public static final String LOCATION = "location";

    /**
     * 节点服务事件路径 根据自身设置实际情况修改，host:port的格式列的数据库字段名称
     */
    public static final String EVENT_HUB_LOCATION = "event_hub_location";

    /**
     * 组织ID列的数据库字段名称
     */
    public static final String ORG_ID = "org_id";

    /**
     * Peer TLS证书列的数据库字段名称
     */
    public static final String SERVER_CRT_PATH = "server_crt_path";

    /**
     * Peer User Client Cert列的数据库字段名称
     */
    public static final String CLIENT_CERT_PATH = "client_cert_path";

    /**
     * Peer User Client Key列的数据库字段名称
     */
    public static final String CLIENT_KEY_PATH = "client_key_path";

    /**
     * 创建日期列的数据库字段名称
     */
    public static final String CREATE_TIME = "create_time";

    /**
     * 联盟ID(冗余)列的数据库字段名称
     */
    public static final String LEAGUE_ID = "league_id";

    /**
     * 组织名称(冗余)列的数据库字段名称
     */
    public static final String ORG_NAME = "org_name";

    /**
     * 联盟名称(冗余)列的数据库字段名称
     */
    public static final String LEAGUE_NAME = "league_name";

    /**
     * 通道数量(冗余)列的数据库字段名称
     */
    public static final String CHANNEL_COUNT = "channel_count";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FabricPeer{" +
        "id=" + id +
        ", name=" + name +
        ", location=" + location +
        ", eventHubLocation=" + eventHubLocation +
        ", orgId=" + orgId +
        ", serverCrtPath=" + serverCrtPath +
        ", clientCertPath=" + clientCertPath +
        ", clientKeyPath=" + clientKeyPath +
        ", createTime=" + createTime +
        ", leagueId=" + leagueId +
        ", orgName=" + orgName +
        ", leagueName=" + leagueName +
        ", channelCount=" + channelCount +
        "}";
    }
}
