package com.ybg.block.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 通道
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
@ApiModel(value = "通道")
public class FnsChannel extends Model<FnsChannel> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Integer id;
    /**
     * 通道名称
     */
    @ApiModelProperty(value = "通道名称")
    private String name;
    /**
     * 同步区块监听
     */
    @ApiModelProperty(value = "同步区块监听")
    private Integer blockListener;
    /**
     * 同步区块监听回调地址
     */
    @ApiModelProperty(value = "同步区块监听回调地址")
    private String callbackLocation;
    /**
     * 节点ID
     */
    @ApiModelProperty(value = "节点ID")
    private Integer peerId;
    /**
     * 区块高度
     */
    @ApiModelProperty(value = "区块高度")
    private Integer height;
    /**
     * 日期
     */
    @ApiModelProperty(value = "日期")
    private String date;


    /**
     * 获取主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取通道名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置通道名称
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取同步区块监听
     */
    public Integer getBlockListener() {
        return blockListener;
    }

    /**
     * 设置同步区块监听
     */

    public void setBlockListener(Integer blockListener) {
        this.blockListener = blockListener;
    }

    /**
     * 获取同步区块监听回调地址
     */
    public String getCallbackLocation() {
        return callbackLocation;
    }

    /**
     * 设置同步区块监听回调地址
     */

    public void setCallbackLocation(String callbackLocation) {
        this.callbackLocation = callbackLocation;
    }

    /**
     * 获取节点ID
     */
    public Integer getPeerId() {
        return peerId;
    }

    /**
     * 设置节点ID
     */

    public void setPeerId(Integer peerId) {
        this.peerId = peerId;
    }

    /**
     * 获取区块高度
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * 设置区块高度
     */

    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * 获取日期
     */
    public String getDate() {
        return date;
    }

    /**
     * 设置日期
     */

    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 通道名称列的数据库字段名称
     */
    public static final String NAME = "name";

    /**
     * 同步区块监听列的数据库字段名称
     */
    public static final String BLOCK_LISTENER = "block_listener";

    /**
     * 同步区块监听回调地址列的数据库字段名称
     */
    public static final String CALLBACK_LOCATION = "callback_location";

    /**
     * 节点ID列的数据库字段名称
     */
    public static final String PEER_ID = "peer_id";

    /**
     * 区块高度列的数据库字段名称
     */
    public static final String HEIGHT = "height";

    /**
     * 日期列的数据库字段名称
     */
    public static final String DATE = "date";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FnsChannel{" +
                "id=" + id +
                ", name=" + name +
                ", blockListener=" + blockListener +
                ", callbackLocation=" + callbackLocation +
                ", peerId=" + peerId +
                ", height=" + height +
                ", date=" + date +
                "}";
    }
}
