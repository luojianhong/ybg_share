package com.ybg.block.core.fabric.datamapper;

import com.ybg.block.core.dbapi.entity.FabricPeer;
import com.ybg.share.core.fabric.vo.FabricPeerVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface FabricPeerDataMapper {
    FabricPeerDataMapper INSTANCE = Mappers.getMapper(FabricPeerDataMapper.class);

    FabricPeerVO toVO(FabricPeer bean);

    List<FabricPeerVO> toVOList(List<FabricPeer> bean);

}
