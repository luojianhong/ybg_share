package com.ybg.block.core.dbapi.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.block.core.dbapi.entity.FabricOrg;
import com.ybg.block.core.dbapi.dao.FabricOrgMapper;
import com.ybg.block.core.dbapi.service.FabricOrgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.share.core.fabric.dto.FabircOrgQueryDTO;
import com.ybg.share.core.fabric.vo.FabricOrgVO;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;

/**
 * <p>
 * 组织 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
//@Service(version = "1.0.0", interfaceClass = FabricOrgService.class)
@Service
public class FabricOrgServiceImpl extends ServiceImpl<FabricOrgMapper, FabricOrg> implements FabricOrgService {




    @Override
    public Page<FabricOrgVO> pageQuery(FabircOrgQueryDTO dto) {
        Page<FabricOrgVO> page = new Page<>(dto.getCurrentPage(), dto.getSize());
        page.setTotal(baseMapper.countPageQuery(dto));
        page.setRecords(baseMapper.listPageQuery(dto));
        return page;
    }
}
