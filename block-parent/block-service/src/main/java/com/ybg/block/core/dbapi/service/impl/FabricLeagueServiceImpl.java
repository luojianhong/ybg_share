package com.ybg.block.core.dbapi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.block.core.dbapi.entity.FabricLeague;
import com.ybg.block.core.dbapi.dao.FabricLeagueMapper;
import com.ybg.block.core.dbapi.service.FabricLeagueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 联盟 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
//@Service(version = "1.0.0", interfaceClass = FabricLeagueService.class)
@Service
public class FabricLeagueServiceImpl extends ServiceImpl<FabricLeagueMapper, FabricLeague> implements FabricLeagueService {

    @Override
    public FabricLeague getByLeagueName(String name) {
        QueryWrapper<FabricLeague> wrapper= new QueryWrapper<>();
        return baseMapper.selectOne(wrapper);
    }
}
