package com.ybg.block.core.fabric;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.block.core.dbapi.entity.FabricCa;
import com.ybg.block.core.dbapi.entity.FabricOrg;
import com.ybg.block.core.dbapi.entity.FabricPeer;
import com.ybg.block.core.dbapi.service.FabricCaService;
import com.ybg.block.core.dbapi.service.FabricOrgService;
import com.ybg.block.core.dbapi.service.FabricPeerService;
import com.ybg.share.core.fabric.FabricCaRemoteApi;
import com.ybg.block.core.fabric.datamapper.FabricCaDataMapper;
import com.ybg.share.core.fabric.dto.FabricCaEditDTO;
import com.ybg.share.core.fabric.dto.FabricCaQueryDTO;
import com.ybg.share.core.fabric.vo.FabricCaVO;
import com.ybg.block.core.fabricbak.utils.MD5Util;
import com.ybg.framework.vo.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service(version = "${dubbo.provider.version}", timeout = 120000, cache = "false")
public class FabricCaRemoteApiImpl implements FabricCaRemoteApi {

    @Autowired
    private FabricCaService fabricCaService;
    @Autowired
    private FabricPeerService fabricPeerService;
    @Autowired
    private FabricOrgService fabricOrgService;

    @Override
    public R<?> editFabricCa(FabricCaEditDTO dto) {
        FabricPeer peer = null;
        FabricCa updateCa = null;
        if (dto.getId() == null && dto.getPeerId() == null) {
            return R.fail("节点ID不能为空");
        }
        if (StrUtil.isBlank(dto.getName())) {
            return R.fail("名称不能为空");
        }
        if (dto.getId() == null && StrUtil.isBlank(dto.getSk())) {
            return R.fail("SK文件不能为空");
        }
        if (dto.getId() == null && StrUtil.isBlank(dto.getCertificate())) {
            return R.fail("CA文件不能为空");
        }
        if (dto.getId() == null && dto.getPeerId() != null) {
            peer = fabricPeerService.getById(dto.getPeerId());
            if (peer == null) {
                return R.fail("节点不存在");
            }
        }
        if (dto.getId() != null) {
            updateCa= fabricCaService.getById(dto.getId());
            if(updateCa==null){
                return R.fail("数据不存在");
            }
            peer = fabricPeerService.getById(updateCa.getPeerId());
        }
        if(updateCa==null){
            updateCa= new FabricCa();
        }
        FabricOrg org = fabricOrgService.getById(peer.getOrgId());
        updateCa.setName(dto.getName());
        updateCa.setSk(dto.getSk());
        updateCa.setCertificate(dto.getCertificate());
        updateCa.setFlag(MD5Util.md516(peer.getLeagueName()+ org.getMspId() + peer.getName() + dto.getName()));
        updateCa.setPeerId(dto.getPeerId());
        updateCa.setCreateTime(LocalDateTime.now());
        updateCa.setLeagueId(peer.getLeagueId());
        updateCa.setOrgId(peer.getOrgId());
        updateCa.setPeerName(peer.getName());
        updateCa.setOrgName(peer.getOrgName());
        updateCa.setLeagueName(peer.getLeagueName());
        fabricCaService.saveOrUpdate(updateCa);
        return R.success();
    }

    @Override
    public R<?> deleteFabricCa(Long id) {
        FabricCa fabricCa = fabricCaService.getById(id);
        if (fabricCa == null) {
            return R.fail("数据不存在");
        }
        fabricCaService.removeById(id);
        return R.success();
    }

    @Override
    public R<FabricCaVO> getById(Long id) {
        FabricCa fabricCa = fabricCaService.getById(id);
        return R.success(FabricCaDataMapper.INSTANCE.toVO(fabricCa));
    }

    @Override
    public R<List<FabricCaVO>> query(FabricCaQueryDTO dto) {
        QueryWrapper<FabricCa> wrapper = new QueryWrapper<>();
        wrapper.eq(dto.getId() != null, FabricCa.ID, dto.getId());
        wrapper.eq(dto.getLeagueId() != null, FabricCa.LEAGUE_ID, dto.getLeagueId());
        wrapper.eq(dto.getOrgId() != null, FabricCa.ORG_ID, dto.getOrgId());
        wrapper.eq(dto.getPeerId() != null, FabricCa.PEER_ID, dto.getPeerId());
        List<FabricCa> list = fabricCaService.list(wrapper);
        return R.success(FabricCaDataMapper.INSTANCE.toVOList(list));
    }
}
