package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FnsApp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsAppMapper extends BaseMapper<FnsApp> {
    @Insert("insert into fns_app (name, app_key, chaincode_id, create_date, modify_date, active)" +
            " values (#{a.name},#{a.appKey},#{a.chaincodeId},#{a.createDate},#{a.modifyDate},#{a.active})")
    int add(@Param("a") FnsApp app);

    @Update("update fns_app set name=#{a.name}, app_key=#{a.appKey}, modify_date=#{a.modifyDate}, active=#{a.active} where id=#{a.id}")
    int update(@Param("a") FnsApp app);

    @Update("update fns_app set app_key=#{a.appKey} where id=#{a.id}")
    int updateKey(@Param("a") FnsApp app);

    @Select("select count(name) from fns_app where chaincode_id=#{id}")
    int countById(@Param("id") int id);

    @Select("select count(name) from fns_app")
    int count();

    @Select("select app_key from fns_app where name=#{a.name} and chaincode_id=#{a.chaincodeId}")
    @Results({
            @Result(property = "name", column = "name"),
            @Result(property = "appKey", column = "appKey"),
            @Result(property = "chaincode_id", column = "chaincodeId")
    })
    FnsApp check(@Param("a") FnsApp app);

    @Select("select id, name, app_key, chaincode_id, create_date, modify_date, active from fns_app where chaincode_id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "appKey", column = "app_key"),
            @Result(property = "chaincodeId", column = "chaincode_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "modifyDate", column = "modify_date"),
            @Result(property = "active", column = "active")
    })
    List<FnsApp> list(@Param("id") int id);

    @Select("select id, name, app_key, chaincode_id, create_date, modify_date, active from fns_app where id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "appKey", column = "app_key"),
            @Result(property = "chaincodeId", column = "chaincode_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "modifyDate", column = "modify_date"),
            @Result(property = "active", column = "active")
    })
    FnsApp get(@Param("id") int id);

    @Select("select id, name, app_key, chaincode_id, create_date, modify_date, active from fns_app where app_key=#{appKey}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "appKey", column = "app_key"),
            @Result(property = "chaincodeId", column = "chaincode_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "modifyDate", column = "modify_date"),
            @Result(property = "active", column = "active")
    })
    FnsApp getByKey(@Param("appKey") String appKey);

    @Delete("delete from fns_app where id=#{id}")
    int delete(@Param("id") int id);

    @Delete("delete from fns_app where chaincode_id=#{id}")
    int deleteAll(@Param("id") int id);
}
