package com.ybg.block.core.fabric.datamapper;

import com.ybg.block.core.dbapi.entity.FabricOrderer;
import com.ybg.share.core.fabric.vo.FabricOrdererVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface FabricOrdererDataMapper {
    FabricOrdererDataMapper INSTANCE = Mappers.getMapper(FabricOrdererDataMapper.class);

    FabricOrdererVO toVO(FabricOrderer bean);

    List<FabricOrdererVO> toVOList(List<FabricOrderer> bean);

}
