package com.ybg.block.core.dbapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.block.core.dbapi.entity.FnsOrderer;
import com.ybg.block.core.dbapi.vo.FnsOrdererVO;
import com.ybg.block.core.dbapi.vo.FnsOrgVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsOrdererService extends IService<FnsOrderer> {
    int add(FnsOrdererVO orderer, MultipartFile serverCrtFile, MultipartFile clientCertFile, MultipartFile clientKeyFile);

    int update(FnsOrdererVO orderer, MultipartFile serverCrtFile, MultipartFile clientCertFile, MultipartFile clientKeyFile);

    List<FnsOrdererVO> listAll();

    List<FnsOrderer> listById(int id);

    FnsOrderer get(int id);

    int countById(int id);

    int count();

    int delete(int id);

    List<FnsOrgVO> listOrgById(int ordererId);

    List<FnsOrgVO> listAllOrg();

    FnsOrdererVO resetOrderer(FnsOrdererVO orderer);
}
