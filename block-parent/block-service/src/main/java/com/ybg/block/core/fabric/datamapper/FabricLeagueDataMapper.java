package com.ybg.block.core.fabric.datamapper;

import com.ybg.block.core.dbapi.entity.FabricLeague;
import com.ybg.share.core.fabric.dto.FabricLeagueEditDTO;
import com.ybg.share.core.fabric.vo.FabricLeagueVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author yanyu
 */
@Mapper
public interface FabricLeagueDataMapper {
    FabricLeagueDataMapper INSTANCE = Mappers.getMapper(FabricLeagueDataMapper.class);

    FabricLeagueVO toVO(FabricLeague bean);

    List<FabricLeagueVO> toVOList(List<FabricLeague> bean);

    FabricLeague toBean(FabricLeagueEditDTO dto);
}
