package com.ybg.block.core.dbapi.service.impl;

import com.ybg.block.core.dbapi.entity.FabricPeer;
import com.ybg.block.core.dbapi.dao.FabricPeerMapper;
import com.ybg.block.core.dbapi.service.FabricPeerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 节点 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
//@Service(version = "1.0.0", interfaceClass = FabricPeerService.class)
@Service
public class FabricPeerServiceImpl extends ServiceImpl<FabricPeerMapper, FabricPeer> implements FabricPeerService {


}
