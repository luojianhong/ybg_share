package com.ybg.block.core.dbapi.service.impl;
import com.ybg.block.core.dbapi.vo.FnsBlockVO;
import com.ybg.block.core.dbapi.vo.FnsChannelVO;
import com.ybg.block.core.fabricbak.bean.ChannelBlock;
import com.ybg.block.core.fabricbak.bean.ChannelBlockList;
import com.ybg.block.core.fabricbak.bean.ChannelPercent;
import com.ybg.block.core.fabricbak.bean.Curve;
import com.ybg.block.core.fabricbak.bean.DayStatistics;
import com.ybg.block.core.fabricbak.bean.Platform;
import com.ybg.block.core.dbapi.dao.FnsChannelMapper;
import com.ybg.block.core.dbapi.dao.FnsPeerMapper;
import com.ybg.block.core.dbapi.entity.FnsBlock;
import com.ybg.block.core.dbapi.dao.FnsBlockMapper;
import com.ybg.block.core.dbapi.service.FnsBlockService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.block.core.fabricbak.utils.DateUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
//@Service(version = "1.0.0", interfaceClass = FnsBlockService.class)
@Service
public class FnsBlockServiceImpl extends ServiceImpl<FnsBlockMapper, FnsBlock> implements FnsBlockService {
    @Resource
    private FnsBlockMapper blockMapper;
    @Resource
    private FnsPeerMapper peerMapper;
    @Resource
    private FnsChannelMapper channelMapper;

    @Override
    public int add(FnsBlock block) {
        return blockMapper.add(block);
    }

    @Override
    public int addBlockList(List<FnsBlockVO> blocks) {
        if (blocks.size() > 0) {
            return blockMapper.addList(blocks);
        }
        return 0;
    }

    @Override
    public List<ChannelPercent> getChannelPercents(List<FnsChannelVO> channels) {
        List<ChannelPercent> channelPercents = new LinkedList<>();
        for (FnsChannelVO channel : channels) {
            int txCount = 0;
            try {
                txCount = blockMapper.countTxByChannelId(channel.getId());
            } catch (Exception ignored) {

            }
            ChannelPercent channelPercent = new ChannelPercent();
            channelPercent.setName(String.format("%s-%s", channel.getPeerName(), channel.getName()));
            channelPercent.setBlockPercent(blockMapper.countByChannelId(channel.getId()));
            channelPercent.setTxPercent(txCount);
            channelPercents.add(channelPercent);
        }
        return channelPercents;
    }

    @Override
    public List<ChannelBlockList> getChannelBlockLists(List<FnsChannelVO> channels) {
        List<ChannelBlockList> channelBlockLists = new LinkedList<>();
        for (FnsChannelVO channel : channels) {
            List<ChannelBlock> channelBlocks = new LinkedList<>();
            int zeroCount = 0;
            for (int i = 14; i >= 0; i--) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, 0 - i);
                int date = Integer.valueOf(DateUtil.date2Str(calendar.getTime(), "yyyyMMdd"));
                int blockCount = blockMapper.countByChannelIdAndDate(channel.getId(), date);
                if (blockCount == 0) {
                    zeroCount++;
                }
                ChannelBlock channelBlock = new ChannelBlock();
                channelBlock.setBlocks(blockCount);
                channelBlock.setDate(String.valueOf(date));
                channelBlocks.add(channelBlock);
            }
            ChannelBlockList channelBlockList = new ChannelBlockList();
            channelBlockList.setName(String.format("%s-%s", channel.getPeerName(), channel.getName()));
            channelBlockList.setChannelBlocks(channelBlocks);
            channelBlockList.setZeroCount(zeroCount);
            channelBlockLists.add(channelBlockList);
        }
        channelBlockLists.sort((t1, t2) -> Math.toIntExact(t2.getZeroCount() - t1.getZeroCount()));
        return channelBlockLists.size() >= 3 ? channelBlockLists.subList(0, 3) : channelBlockLists;
    }

    @Override
    public DayStatistics getDayStatistics() {
        int today = Integer.valueOf(DateUtil.getCurrent("yyyyMMdd"));
        int todayBlocks = 0;
        int todayTxs = 0;
        int allTxs = 0;
        try {
            todayBlocks = blockMapper.countByDate(today);
        } catch (Exception ignored) {

        }
        try {
            todayTxs = blockMapper.countTxByDate(today);
        } catch (Exception ignored) {

        }
        try {
            allTxs = blockMapper.countTx();
        } catch (Exception ignored) {

        }
        int allBlocks = blockMapper.count();
        DayStatistics dayStatistics = new DayStatistics();
        dayStatistics.setBlockCount(todayBlocks);
        dayStatistics.setTxCount(todayTxs);
        dayStatistics.setBlockPercent(todayBlocks == 0 ? 0 : (1 - todayBlocks / allBlocks) * 100);
        dayStatistics.setTxPercent(todayTxs == 0 ? 0 : (1 - todayTxs / allTxs) * 100);
        return dayStatistics;
    }

    @Override
    public Platform getPlatform() {
        int txCount = 0;
        int rwSetCount = 0;
        try {
            txCount = blockMapper.countTx();
        } catch (Exception ignored) {

        }
        try {
            rwSetCount = blockMapper.countRWSet();
        } catch (Exception ignored) {

        }
        Platform platform = new Platform();
        platform.setBlockCount(blockMapper.count());
        platform.setTxCount(txCount);
        platform.setRwSetCount(rwSetCount);
        return platform;
    }

    @Override
    public FnsBlockVO getByChannelId(int channelId) {
        return blockMapper.getByChannelId(channelId);
    }

    @Override
    public List<FnsBlockVO> getLimit(int limit) {
        List<FnsBlockVO> blocks = new LinkedList<>();
        List<FnsChannelVO> channels = channelMapper.listAll();
        for (FnsChannelVO channel : channels) {
            List<FnsBlockVO> blockTmps = blockMapper.getLimit(channel.getId(), limit);
            for (FnsBlockVO block : blockTmps) {
                block.setPeerChannelName(String.format("%s-%s", peerMapper.get(channel.getPeerId()).getName(), channel.getName()));
                block.setHeight(block.getHeight() + 1);
            }
            blocks.addAll(blockTmps);
        }
        blocks.sort((o1, o2) -> {
            try {
                return DateUtil.str2Date(o2.getTimestamp(), "yyyy/MM/dd HH:mm:ss").compareTo(DateUtil.str2Date(o1.getTimestamp(), "yyyy/MM/dd HH:mm:ss"));
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        });
        return blocks.size() != 0 && blocks.size() >= 6 ? blocks.subList(0, 6) : blocks;
    }

    @Override
    public Curve get20CountList() {
        List<Integer> integers = new LinkedList<>();
        Curve curve = new Curve();
        curve.setName("Block Count");
        for (int i = 19; i >= 0; i--) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 0 - i);
            int date = Integer.valueOf(DateUtil.date2Str(calendar.getTime(), "yyyyMMdd"));
            int blockCount = 0;
            try {
                blockCount = blockMapper.countByDate(date);
            } catch (Exception ignored) {

            }
            integers.add(blockCount);
        }
        int ud = integers.get(19) - integers.get(18);
        curve.setUpDown(ud >= 0 ? "+" + ud : String.valueOf(ud));
        curve.setIntegers(integers);
        return curve;
    }

    @Override
    public Curve get20TxCountList() {
        List<Integer> integers = new LinkedList<>();
        Curve curve = new Curve();
        curve.setName("TX Count");
        for (int i = 19; i >= 0; i--) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 0 - i);
            int date = Integer.valueOf(DateUtil.date2Str(calendar.getTime(), "yyyyMMdd"));
            int txCount = 0;
            try {
                txCount = blockMapper.countTxByDate(date);
            } catch (Exception ignored) {

            }
            integers.add(txCount);
        }
        int ud = integers.get(19) - integers.get(18);
        curve.setUpDown(ud >= 0 ? "+" + ud : String.valueOf(ud));
        curve.setIntegers(integers);
        return curve;
    }

    @Override
    public Curve get20RWCountList() {
        List<Integer> integers = new LinkedList<>();
        Curve curve = new Curve();
        curve.setName("RWSet Count");
        for (int i = 19; i >= 0; i--) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 0 - i);
            int date = Integer.valueOf(DateUtil.date2Str(calendar.getTime(), "yyyyMMdd"));
            int rwCount = 0;
            try {
                rwCount = blockMapper.countRWSetByDate(date);
            } catch (Exception ignored) {

            }
            integers.add(rwCount);
        }
        int ud = integers.get(19) - integers.get(18);
        curve.setUpDown(ud >= 0 ? "+" + ud : String.valueOf(ud));
        curve.setIntegers(integers);
        return curve;
    }
}
