package com.ybg.block.framework.impl;

import com.ybg.block.framework.api.YbgGeth;
import com.ybg.block.framework.bean.*;
import org.web3j.protocol.Web3jService;
import org.web3j.protocol.admin.methods.response.BooleanResponse;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.Response;
import org.web3j.protocol.geth.JsonRpc2_0Geth;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

public class YbgJsonRpc2_0Geth extends JsonRpc2_0Geth implements YbgGeth {
    public YbgJsonRpc2_0Geth(Web3jService web3jService) {
        super(web3jService);
    }

    @Override
    public Request<?, BooleanResponse> setEtherbase(String accountId) {
        return new Request<>(
                "eth_setEtherbase", Collections.<String>emptyList(), web3jService, BooleanResponse.class);

    }

    @Override
    public Request<?, BooleanResponse> adminAddPeer(String url) {

        return new Request<>(
                "admin_addPeer", Arrays.asList(url), web3jService, BooleanResponse.class);
    }

    @Override
    public Request<?, StringResponse> adminDatadir() {
        return new Request<>(
                "admin_datadir", Collections.<String>emptyList(), web3jService, StringResponse.class);
    }

    @Override
    public Request<?, EthNodeInfoResponse> adminNodeInfo() {
        return new Request<>(
                "admin_nodeInfo", Collections.<String>emptyList(), web3jService, EthNodeInfoResponse.class);
    }

    @Override
    public Request<?, Response> adminPeers() {
        //这个解析有问题
        return new Request(
                "admin_nodeInfo", Collections.<String>emptyList(), web3jService, Response.class);
    }

    @Override
    public Request<?, StringResponse> adminSetSolc(String path) {
        return new Request<>(
                "admin_setSolc", Arrays.asList(path), web3jService, StringResponse.class);
    }

    @Override
    public Request<?, BooleanResponse> adminStartRPC(String host, Integer port, String cors, String apis) {
        return new Request<>(
                "admin_startRPC", Arrays.asList(host,port,cors,apis), web3jService, BooleanResponse.class);
    }

    @Override
    public Request<?, BooleanResponse> adminStartWS(String host, Integer port, String cors, String apis) {
        return new Request<>(
                "admin_startWS", Arrays.asList(host,port,cors,apis), web3jService, BooleanResponse.class);
    }

    @Override
    public Request<?, BooleanResponse> adminStopRPC() {
        return new Request<>(
                "admin_stopRPC", Collections.<String>emptyList(), web3jService, BooleanResponse.class);
    }

    @Override
    public Request<?, BooleanResponse> adminStopWS() {
        return new Request<>(
                "admin_stopWS", Collections.<String>emptyList(), web3jService, BooleanResponse.class);
    }

    @Override
    public Request<?, BooleanResponse> minerSetExtra(String extra) {
        return new Request<>(
                "miner_setExtra", Collections.<String>emptyList(), web3jService, BooleanResponse.class);
    }

    @Override
    public Request<?, BooleanResponse> minerSetGasPrice(BigDecimal hexNumber) {
        return new Request<>(
                "miner_blockProfile", Arrays.asList(hexNumber), web3jService, BooleanResponse.class);
    }

    @Override
    public Request<?, EthBigIntegerRessponse> minerGetHashrate() {
        return new Request<>(
                "miner_getHashrate", Collections.<String>emptyList(), web3jService, EthBigIntegerRessponse.class);
    }

    @Override
    public Request txPoolContent() {
        // todo 没有验证过的 http://cw.hubwiz.com/card/c/geth-rpc-api/1/5/1/
        return new Request<>(
                "txpool_content", Collections.<String>emptyList(), web3jService, StringResponse.class);
    }

    @Override
    public Request txPoolInspect() {
        //todo 没有验证过的 http://cw.hubwiz.com/card/c/geth-rpc-api/1/5/1/
        return new Request<>(
                "txpool_inspect", Collections.<String>emptyList(), web3jService, StringResponse.class);
    }

    @Override
    public Request<?,EthTxpoolStatusResponse> txPoolStatus() {
         return new Request<>(
                "miner_getHashrate", Collections.<String>emptyList(), web3jService, EthTxpoolStatusResponse.class);
    }
}
