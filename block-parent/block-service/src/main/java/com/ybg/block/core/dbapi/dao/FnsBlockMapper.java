package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.dao.provider.BlockDAOProvider;
import com.ybg.block.core.dbapi.entity.FnsBlock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ybg.block.core.dbapi.vo.FnsBlockVO;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsBlockMapper extends BaseMapper<FnsBlock> {
    @Insert("insert into fns_block (channel_id,height,data_hash,calculated_hash,previous_hash," +
            "envelope_count,tx_count,r_w_set_count,timestamp,calculate_date,create_date) " +
            "values " +
            "(#{b.channelId},#{b.height},#{b.dataHash},#{b.calculatedHash},#{b.previousHash},#{b.envelopeCount}," +
            "#{b.txCount},#{b.rwSetCount},#{b.timestamp},#{b.calculateDate},#{b.createDate})")
    int add(@Param("b") FnsBlock block);

    @InsertProvider(type = BlockDAOProvider.class, method = "insertAll")
    int addList(@Param("list") List<FnsBlockVO> blocks);

    @Select("select count(id) from fns_block")
    int count();

    @Select("select count(id) from fns_block where channel_id=#{channelId}")
    int countByChannelId(@Param("channelId") int channelId);

    @Select("select count(id) from fns_block where calculate_date=#{calculateDate}")
    int countByDate(@Param("calculateDate") int calculateDate);

    @Select("select count(id) from fns_block where channel_id=#{channelId} and calculate_date=#{calculateDate}")
    int countByChannelIdAndDate(@Param("channelId") int channelId, @Param("calculateDate") int calculateDate);

    @Select("select sum(tx_count) from fns_block")
    int countTx();

    @Select("select sum(tx_count) from fns_block where calculate_date=#{calculateDate}")
    int countTxByDate(@Param("calculateDate") int calculateDate);

    @Select("select sum(tx_count) from fns_block where channel_id=#{channelId}")
    int countTxByChannelId(@Param("channelId") int channelId);

    @Select("select sum(r_w_set_count) from fns_block")
    int countRWSet();

    @Select("select sum(r_w_set_count) from fns_block where calculate_date=#{calculateDate}")
    int countRWSetByDate(@Param("calculateDate") int calculateDate);

    @Select("select channel_id,height,data_hash,calculated_hash,previous_hash,envelope_count,tx_count,r_w_set_count," +
            "timestamp,calculate_date,create_date from fns_block where channel_id=#{channelId} order by id desc limit 1")
//    @Results({
//            @Result(property = "channelId", column = "channel_id"),
//            @Result(property = "dataHash", column = "data_hash"),
//            @Result(property = "calculatedHash", column = "calculated_hash"),
//            @Result(property = "previousHash", column = "previous_hash"),
//            @Result(property = "envelopeCount", column = "envelope_count"),
//            @Result(property = "txCount", column = "tx_count"),
//            @Result(property = "rwSetCount", column = "r_w_set_count"),
//            @Result(property = "calculateDate", column = "calculate_date"),
//            @Result(property = "createDate", column = "create_date")
//    })
    FnsBlockVO getByChannelId(@Param("channelId") int channelId);

    @Select("select channel_id,height,data_hash,calculated_hash,previous_hash,envelope_count,tx_count,r_w_set_count," +
            "timestamp,calculate_date,create_date from fns_block where channel_id=#{channelId} order by id desc limit #{limit}")
//    @Results({
//            @Result(property = "channelId", column = "channel_id"),
//            @Result(property = "dataHash", column = "data_hash"),
//            @Result(property = "calculatedHash", column = "calculated_hash"),
//            @Result(property = "previousHash", column = "previous_hash"),
//            @Result(property = "envelopeCount", column = "envelope_count"),
//            @Result(property = "txCount", column = "tx_count"),
//            @Result(property = "rwSetCount", column = "r_w_set_count"),
//            @Result(property = "calculateDate", column = "calculate_date"),
//            @Result(property = "createDate", column = "create_date")
//    })
    List<FnsBlockVO> getLimit(@Param("channelId") int channelId, @Param("limit") int limit);

    @Delete("delete from fns_block where channel_id=#{channelId}")
    int deleteByChannelId(@Param("channelId") int channelId);
}
