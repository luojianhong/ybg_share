package com.ybg.block.core.dbapi.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 联盟
 * </p>
 *
 * @author yanyu
 * @since 2019-11-29
 */
@ApiModel(value="联盟") 
public class FabricLeague extends Model<FabricLeague> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
@ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 联盟名称
     */
@ApiModelProperty(value = "联盟名称")
    private String name;
    /**
     * 日期
     */
@ApiModelProperty(value = "日期")
    private LocalDateTime createTime;
    /**
     * 组织数量（冗余）
     */
@ApiModelProperty(value = "组织数量（冗余）")
    private Integer orgCount;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取联盟名称
     */
    public String getName() {
        return name;
    }
    /**
     * 设置联盟名称
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取日期
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }
    /**
     * 设置日期
     */

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取组织数量（冗余）
     */
    public Integer getOrgCount() {
        return orgCount;
    }
    /**
     * 设置组织数量（冗余）
     */

    public void setOrgCount(Integer orgCount) {
        this.orgCount = orgCount;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 联盟名称列的数据库字段名称
     */
    public static final String NAME = "name";

    /**
     * 日期列的数据库字段名称
     */
    public static final String CREATE_TIME = "create_time";

    /**
     * 组织数量（冗余）列的数据库字段名称
     */
    public static final String ORG_COUNT = "org_count";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FabricLeague{" +
        "id=" + id +
        ", name=" + name +
        ", createTime=" + createTime +
        ", orgCount=" + orgCount +
        "}";
    }
}
