package com.ybg.block.core.fabric.datamapper;

import com.ybg.block.core.dbapi.entity.FabricChannel;
import com.ybg.share.core.fabric.vo.FabricChannelVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface FabricChannelDataMapper {

    FabricChannelDataMapper INSTANCE = Mappers.getMapper(FabricChannelDataMapper.class);

    FabricChannelVO toVO(FabricChannel bean);

    List<FabricChannelVO> toVOList(List<FabricChannel> list);

}
