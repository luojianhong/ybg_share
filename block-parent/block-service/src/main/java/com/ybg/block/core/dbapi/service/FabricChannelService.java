package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FabricChannel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 通道 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricChannelService extends IService<FabricChannel> {

}
