package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FnsOrg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ybg.block.core.dbapi.vo.FnsOrgVO;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsOrgMapper extends BaseMapper<FnsOrg> {
    @Insert("insert into fns_org (msp_id,tls,league_id,date)" +
            "values (#{o.mspId},#{o.tls},#{o.leagueId},#{o.date})")
    int add(@Param("o") FnsOrg org);

    @Update("update fns_org set tls=#{o.tls}, msp_id=#{o.mspId}, league_id=#{o.leagueId}" +
            " where id=#{o.id}")
    int update(@Param("o") FnsOrg org);

    @Select("select count(msp_id) from fns_org where league_id=#{id}")
    int count(@Param("id") int id);

    @Select("select count(msp_id) from fns_org")
    int countAll();

    @Delete("delete from fns_org where id=#{id}")
    int delete(@Param("id") int id);

    @Delete("delete from fns_org where league_id=#{leagueId}")
    int deleteAll(@Param("leagueId") int leagueId);

    @Select("select id,tls,msp_id,league_id,date from fns_org where id=#{id}")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "tls", column = "tls"),
//            @Result(property = "mspId", column = "msp_id"),
//            @Result(property = "leagueId", column = "league_id"),
//            @Result(property = "date", column = "date")
//    })
    FnsOrg get(@Param("id") int id);

    @Select("select id,tls,msp_id,league_id,date from fns_org where league_id=#{id}")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "tls", column = "tls"),
//            @Result(property = "mspId", column = "msp_id"),
//            @Result(property = "leagueId", column = "league_id"),
//            @Result(property = "date", column = "date")
//    })
    List<FnsOrgVO> list(@Param("id") int id);

    @Select("select id,tls,msp_id,league_id,date from fns_org")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "tls", column = "tls"),
//            @Result(property = "mspId", column = "msp_id"),
//            @Result(property = "leagueId", column = "league_id"),
//            @Result(property = "date", column = "date")
//    })
    List<FnsOrgVO> listAll();
}
