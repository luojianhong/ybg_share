package com.ybg.block.core.dbapi.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 通道
 * </p>
 *
 * @author yanyu
 * @since 2019-12-02
 */
@ApiModel(value="通道") 
public class FabricChannel extends Model<FabricChannel> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
@ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 通道名称
     */
@ApiModelProperty(value = "通道名称")
    private String name;
    /**
     * 同步区块监听
     */
@ApiModelProperty(value = "同步区块监听")
    private Boolean blockListener;
    /**
     * 同步区块监听回调地址
     */
@ApiModelProperty(value = "同步区块监听回调地址")
    private String callbackLocation;
    /**
     * 节点ID
     */
@ApiModelProperty(value = "节点ID")
    private Long peerId;
    /**
     * 区块高度
     */
@ApiModelProperty(value = "区块高度")
    private Integer height;
    /**
     * 日期
     */
@ApiModelProperty(value = "日期")
    private LocalDateTime createTime;
    /**
     * 联盟ID(冗余)
     */
@ApiModelProperty(value = "联盟ID(冗余)")
    private Long leagueId;
    /**
     * 组织ID(冗余)
     */
@ApiModelProperty(value = "组织ID(冗余)")
    private Long orgId;
    /**
     * 节点名称冗余
     */
@ApiModelProperty(value = "节点名称冗余")
    private String peerName;
    /**
     * 组织名称（冗余）
     */
@ApiModelProperty(value = "组织名称（冗余）")
    private String orgName;
    /**
     * 联盟名称（冗余）
     */
@ApiModelProperty(value = "联盟名称（冗余）")
    private String leagueName;
    /**
     * 链码数量(冗余）
     */
@ApiModelProperty(value = "链码数量(冗余）")
    private Integer chaincodeCount;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取通道名称
     */
    public String getName() {
        return name;
    }
    /**
     * 设置通道名称
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取同步区块监听
     */
    public Boolean getBlockListener() {
        return blockListener;
    }
    /**
     * 设置同步区块监听
     */

    public void setBlockListener(Boolean blockListener) {
        this.blockListener = blockListener;
    }

    /**
     * 获取同步区块监听回调地址
     */
    public String getCallbackLocation() {
        return callbackLocation;
    }
    /**
     * 设置同步区块监听回调地址
     */

    public void setCallbackLocation(String callbackLocation) {
        this.callbackLocation = callbackLocation;
    }

    /**
     * 获取节点ID
     */
    public Long getPeerId() {
        return peerId;
    }
    /**
     * 设置节点ID
     */

    public void setPeerId(Long peerId) {
        this.peerId = peerId;
    }

    /**
     * 获取区块高度
     */
    public Integer getHeight() {
        return height;
    }
    /**
     * 设置区块高度
     */

    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * 获取日期
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }
    /**
     * 设置日期
     */

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取联盟ID(冗余)
     */
    public Long getLeagueId() {
        return leagueId;
    }
    /**
     * 设置联盟ID(冗余)
     */

    public void setLeagueId(Long leagueId) {
        this.leagueId = leagueId;
    }

    /**
     * 获取组织ID(冗余)
     */
    public Long getOrgId() {
        return orgId;
    }
    /**
     * 设置组织ID(冗余)
     */

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 获取节点名称冗余
     */
    public String getPeerName() {
        return peerName;
    }
    /**
     * 设置节点名称冗余
     */

    public void setPeerName(String peerName) {
        this.peerName = peerName;
    }

    /**
     * 获取组织名称（冗余）
     */
    public String getOrgName() {
        return orgName;
    }
    /**
     * 设置组织名称（冗余）
     */

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * 获取联盟名称（冗余）
     */
    public String getLeagueName() {
        return leagueName;
    }
    /**
     * 设置联盟名称（冗余）
     */

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    /**
     * 获取链码数量(冗余）
     */
    public Integer getChaincodeCount() {
        return chaincodeCount;
    }
    /**
     * 设置链码数量(冗余）
     */

    public void setChaincodeCount(Integer chaincodeCount) {
        this.chaincodeCount = chaincodeCount;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 通道名称列的数据库字段名称
     */
    public static final String NAME = "name";

    /**
     * 同步区块监听列的数据库字段名称
     */
    public static final String BLOCK_LISTENER = "block_listener";

    /**
     * 同步区块监听回调地址列的数据库字段名称
     */
    public static final String CALLBACK_LOCATION = "callback_location";

    /**
     * 节点ID列的数据库字段名称
     */
    public static final String PEER_ID = "peer_id";

    /**
     * 区块高度列的数据库字段名称
     */
    public static final String HEIGHT = "height";

    /**
     * 日期列的数据库字段名称
     */
    public static final String CREATE_TIME = "create_time";

    /**
     * 联盟ID(冗余)列的数据库字段名称
     */
    public static final String LEAGUE_ID = "league_id";

    /**
     * 组织ID(冗余)列的数据库字段名称
     */
    public static final String ORG_ID = "org_id";

    /**
     * 节点名称冗余列的数据库字段名称
     */
    public static final String PEER_NAME = "peer_name";

    /**
     * 组织名称（冗余）列的数据库字段名称
     */
    public static final String ORG_NAME = "org_name";

    /**
     * 联盟名称（冗余）列的数据库字段名称
     */
    public static final String LEAGUE_NAME = "league_name";

    /**
     * 链码数量(冗余）列的数据库字段名称
     */
    public static final String CHAINCODE_COUNT = "chaincode_count";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FabricChannel{" +
        "id=" + id +
        ", name=" + name +
        ", blockListener=" + blockListener +
        ", callbackLocation=" + callbackLocation +
        ", peerId=" + peerId +
        ", height=" + height +
        ", createTime=" + createTime +
        ", leagueId=" + leagueId +
        ", orgId=" + orgId +
        ", peerName=" + peerName +
        ", orgName=" + orgName +
        ", leagueName=" + leagueName +
        ", chaincodeCount=" + chaincodeCount +
        "}";
    }
}
