package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FabricBlock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 区块 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricBlockMapper extends BaseMapper<FabricBlock> {

}
