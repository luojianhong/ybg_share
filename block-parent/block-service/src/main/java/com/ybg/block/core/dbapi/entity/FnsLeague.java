package com.ybg.block.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 联盟
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
@ApiModel(value = "联盟")
public class FnsLeague extends Model<FnsLeague> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Integer id;
    /**
     * 联盟名称
     */
    @ApiModelProperty(value = "联盟名称")
    private String name;
    /**
     * 日期
     */
    @ApiModelProperty(value = "日期")
    private String date;


    /**
     * 获取主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取联盟名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置联盟名称
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取日期
     */
    public String getDate() {
        return date;
    }

    /**
     * 设置日期
     */

    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 联盟名称列的数据库字段名称
     */
    public static final String NAME = "name";

    /**
     * 日期列的数据库字段名称
     */
    public static final String DATE = "date";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FnsLeague{" +
                "id=" + id +
                ", name=" + name +
                ", date=" + date +
                "}";
    }
}
