package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.BlockWallet;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 钱包表 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-15
 */
public interface BlockWalletService extends IService<BlockWallet> {


    BlockWallet getByAddress(String address);

}
