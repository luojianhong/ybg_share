package com.ybg.block.core.dbapi.service.impl;

import com.ybg.block.core.dbapi.entity.FabricBlock;
import com.ybg.block.core.dbapi.dao.FabricBlockMapper;
import com.ybg.block.core.dbapi.service.FabricBlockService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 区块 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
//@Service(version = "1.0.0", interfaceClass = FabricBlockService.class)
@Service
public class FabricBlockServiceImpl extends ServiceImpl<FabricBlockMapper, FabricBlock> implements FabricBlockService {


}
