package com.ybg.block.core.dbapi.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 应用
 * </p>
 *
 * @author yanyu
 * @since 2019-11-29
 */
@ApiModel(value="应用") 
public class FabricApp extends Model<FabricApp> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
@ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 应用编码
     */
@ApiModelProperty(value = "应用编码")
    private String appCode;
    /**
     * app名称
     */
@ApiModelProperty(value = "app名称")
    private String appName;
    /**
     * 创建时间
     */
@ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 链码ID
     */
@ApiModelProperty(value = "链码ID")
    private Long chaincodeId;
    /**
     * 是否启用 1启用，0禁用
     */
@ApiModelProperty(value = "是否启用 1启用，0禁用")
    private Boolean status;
    /**
     * 更新时间
     */
@ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /**
     * 联盟ID(冗余)
     */
@ApiModelProperty(value = "联盟ID(冗余)")
    private Long leagueId;
    /**
     * 组织ID(冗余)
     */
@ApiModelProperty(value = "组织ID(冗余)")
    private Long orgId;
    /**
     * 节点ID(冗余)
     */
@ApiModelProperty(value = "节点ID(冗余)")
    private Long peerId;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取应用编码
     */
    public String getAppCode() {
        return appCode;
    }
    /**
     * 设置应用编码
     */

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    /**
     * 获取app名称
     */
    public String getAppName() {
        return appName;
    }
    /**
     * 设置app名称
     */

    public void setAppName(String appName) {
        this.appName = appName;
    }

    /**
     * 获取创建时间
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }
    /**
     * 设置创建时间
     */

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取链码ID
     */
    public Long getChaincodeId() {
        return chaincodeId;
    }
    /**
     * 设置链码ID
     */

    public void setChaincodeId(Long chaincodeId) {
        this.chaincodeId = chaincodeId;
    }

    /**
     * 获取是否启用 1启用，0禁用
     */
    public Boolean getStatus() {
        return status;
    }
    /**
     * 设置是否启用 1启用，0禁用
     */

    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 获取更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }
    /**
     * 设置更新时间
     */

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取联盟ID(冗余)
     */
    public Long getLeagueId() {
        return leagueId;
    }
    /**
     * 设置联盟ID(冗余)
     */

    public void setLeagueId(Long leagueId) {
        this.leagueId = leagueId;
    }

    /**
     * 获取组织ID(冗余)
     */
    public Long getOrgId() {
        return orgId;
    }
    /**
     * 设置组织ID(冗余)
     */

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 获取节点ID(冗余)
     */
    public Long getPeerId() {
        return peerId;
    }
    /**
     * 设置节点ID(冗余)
     */

    public void setPeerId(Long peerId) {
        this.peerId = peerId;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 应用编码列的数据库字段名称
     */
    public static final String APP_CODE = "app_code";

    /**
     * app名称列的数据库字段名称
     */
    public static final String APP_NAME = "app_name";

    /**
     * 创建时间列的数据库字段名称
     */
    public static final String CREATE_TIME = "create_time";

    /**
     * 链码ID列的数据库字段名称
     */
    public static final String CHAINCODE_ID = "chaincode_id";

    /**
     * 是否启用 1启用，0禁用列的数据库字段名称
     */
    public static final String STATUS = "status";

    /**
     * 更新时间列的数据库字段名称
     */
    public static final String UPDATE_TIME = "update_time";

    /**
     * 联盟ID(冗余)列的数据库字段名称
     */
    public static final String LEAGUE_ID = "league_id";

    /**
     * 组织ID(冗余)列的数据库字段名称
     */
    public static final String ORG_ID = "org_id";

    /**
     * 节点ID(冗余)列的数据库字段名称
     */
    public static final String PEER_ID = "peer_id";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FabricApp{" +
        "id=" + id +
        ", appCode=" + appCode +
        ", appName=" + appName +
        ", createTime=" + createTime +
        ", chaincodeId=" + chaincodeId +
        ", status=" + status +
        ", updateTime=" + updateTime +
        ", leagueId=" + leagueId +
        ", orgId=" + orgId +
        ", peerId=" + peerId +
        "}";
    }
}
