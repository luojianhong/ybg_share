/*
 * Copyright (c) 2018. Aberic - aberic@qq.com - All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ybg.block.core.fabricbak.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.ybg.block.core.dbapi.dao.FnsAppMapper;
import com.ybg.block.core.dbapi.dao.FnsCaMapper;
import com.ybg.block.core.dbapi.dao.FnsChaincodeMapper;
import com.ybg.block.core.dbapi.dao.FnsChannelMapper;
import com.ybg.block.core.dbapi.dao.FnsLeagueMapper;
import com.ybg.block.core.dbapi.dao.FnsOrdererMapper;
import com.ybg.block.core.dbapi.dao.FnsOrgMapper;
import com.ybg.block.core.dbapi.dao.FnsPeerMapper;
import com.ybg.block.core.dbapi.entity.FnsCa;
import com.ybg.block.core.fabricbak.base.BaseService;
import com.ybg.block.core.fabricbak.sdk.FabricManager;
import com.ybg.block.core.fabricbak.bean.State;
import com.ybg.block.core.fabricbak.service.StateService;
import com.ybg.block.core.fabricbak.utils.CacheUtil;
import com.ybg.block.core.fabricbak.utils.FabricHelper;
import com.ybg.block.core.fabricbak.utils.VerifyUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 *
 * @author : Aberic 【2018/6/4 15:03】
 */
@Service("stateService")
public class StateServiceImpl implements StateService, BaseService {

    @Autowired
    private FnsLeagueMapper leagueMapper;
    @Autowired
    private FnsAppMapper appMapper;
    @Autowired
    private FnsOrgMapper orgMapper;
    @Autowired
    private FnsOrdererMapper ordererMapper;
    @Autowired
    private FnsPeerMapper peerMapper;
    @Autowired
    private FnsCaMapper caMapper;
    @Autowired
    private FnsChannelMapper channelMapper;
    @Autowired
    private FnsChaincodeMapper chaincodeMapper;

    @Override
    public String invoke(State state) {
        return chaincode(state, ChainCodeIntent.INVOKE, CacheUtil.getFlagCA(state.getFlag(), caMapper));
    }

    @Override
    public String query(State state) {
        return chaincode(state, ChainCodeIntent.QUERY, CacheUtil.getFlagCA(state.getFlag(), caMapper));
    }


    enum ChainCodeIntent {
        INVOKE, QUERY
    }

    private String chaincode(State state, ChainCodeIntent intent, FnsCa ca) {
        String cc = VerifyUtil.getCc(state, chaincodeMapper, appMapper);
        if (StringUtils.isEmpty(cc)) {
            return responseFail("Request failed：app key is invalid");
        }
        List<String> array = state.getStrArray();
        int length = array.size();
        String fcn = null;
        String[] argArray = new String[length - 1];
        for (int i = 0; i < length; i++) {
            if (i == 0) {
                fcn = array.get(i);
            } else {
                argArray[i - 1] = array.get(i);
            }
        }
        return chaincodeExec(intent, ca, cc, fcn, argArray);
    }

    private String chaincodeExec(ChainCodeIntent intent, FnsCa ca, String cc, String fcn, String[] argArray) {
        JSONObject jsonObject = null;
        try {
            FabricManager manager = FabricHelper.obtain().get(leagueMapper, orgMapper, channelMapper, chaincodeMapper, ordererMapper, peerMapper,
                    ca, cc);
            switch (intent) {
                case INVOKE:
                    jsonObject = manager.invoke(fcn, argArray);
                    break;
                case QUERY:
                    jsonObject = manager.query(fcn, argArray);
                    break;
            }
            return jsonObject.toJSONString();
        } catch (Exception e) {
            e.printStackTrace();
            return responseFail(String.format("Request failed： %s", e.getMessage()));
        }
    }

}
