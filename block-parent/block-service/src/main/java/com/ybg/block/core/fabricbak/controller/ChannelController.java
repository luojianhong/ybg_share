/*
 * Copyright (c) 2018. Aberic - aberic@qq.com - All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ybg.block.core.fabricbak.controller;


import com.ybg.block.core.dbapi.entity.FnsLeague;
import com.ybg.block.core.dbapi.service.FnsChannelService;
import com.ybg.block.core.dbapi.service.FnsLeagueService;
import com.ybg.block.core.dbapi.service.FnsOrgService;
import com.ybg.block.core.dbapi.service.FnsPeerService;
import com.ybg.block.core.dbapi.entity.FnsChannel;
import com.ybg.block.core.dbapi.entity.FnsOrg;
import com.ybg.block.core.dbapi.vo.FnsChannelVO;
import com.ybg.block.core.dbapi.vo.FnsPeerVO;
import com.ybg.block.core.fabricbak.utils.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

/**
 * 描述：
 *
 * @author : Aberic 【2018/6/4 15:01】
 */
@CrossOrigin
@RestController
@RequestMapping("channel")
public class ChannelController {

    @Autowired
    private FnsChannelService channelService;
    @Autowired
    private FnsPeerService peerService;
    @Autowired
    private FnsOrgService orgService;
    @Autowired
    private FnsLeagueService leagueService;

    @PostMapping(value = "submit")
    public ModelAndView submit(@ModelAttribute FnsChannel channel,
                               @RequestParam("intent") String intent,
                               @RequestParam("id") int id) {
        switch (intent) {
            case "add":
                channelService.add(channel);
                break;
            case "edit":
                channel.setId(id);
                // TODO 这里掉了SDK
                channelService.update(channel);
                break;
        }
        return new ModelAndView(new RedirectView("list"));
    }

    @GetMapping(value = "add")
    public ModelAndView add() {
        ModelAndView modelAndView = new ModelAndView("channelSubmit");
        modelAndView.addObject("intentLittle", SpringUtil.get("enter"));
        modelAndView.addObject("submit", SpringUtil.get("submit"));
        modelAndView.addObject("intent", "add");
        modelAndView.addObject("channel", new FnsChannel());
        modelAndView.addObject("peers", peerService.listAll());
        return modelAndView;
    }

    @GetMapping(value = "edit")
    public ModelAndView edit(@RequestParam("id") int id) {
        ModelAndView modelAndView = new ModelAndView("channelSubmit");
        modelAndView.addObject("intentLittle", SpringUtil.get("edit"));
        modelAndView.addObject("submit", SpringUtil.get("modify"));
        modelAndView.addObject("intent", "edit");
        FnsChannelVO channel = channelService.get(id);
        FnsOrg org = orgService.get(peerService.get(channel.getPeerId()).getOrgId());
        channel.setOrgName(org.getMspId());
        List<FnsPeerVO> peers = peerService.listById(org.getId());
        FnsLeague league = leagueService.get(orgService.get(org.getId()).getLeagueId());
        channel.setLeagueName(league.getName());
        for (FnsPeerVO peer : peers) {
            peer.setLeagueName(league.getName());
            peer.setOrgName(org.getMspId());
        }
        modelAndView.addObject("channel", channel);
        modelAndView.addObject("peers", peers);
        return modelAndView;
    }

    @GetMapping(value = "delete")
    public ModelAndView delete(@RequestParam("id") int id) {
        channelService.delete(id);
        return new ModelAndView(new RedirectView("list"));
    }

    @GetMapping(value = "list")
    public ModelAndView list() {
        ModelAndView modelAndView = new ModelAndView("channels");
        modelAndView.addObject("channels", channelService.listAll());
        return modelAndView;
    }

}
