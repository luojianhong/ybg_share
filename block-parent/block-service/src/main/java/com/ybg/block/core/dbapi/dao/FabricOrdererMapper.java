package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FabricOrderer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 排序服务 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricOrdererMapper extends BaseMapper<FabricOrderer> {

}
