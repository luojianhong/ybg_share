package com.ybg.block.core.dbapi.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 证书授权中心
 * </p>
 *
 * @author yanyu
 * @since 2019-11-29
 */
@ApiModel(value="证书授权中心") 
public class FabricCa extends Model<FabricCa> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
@ApiModelProperty(value = "主键")
    private Long id;
    /**
     * CA集合
     */
@ApiModelProperty(value = "CA集合")
    private String name;
    /**
     * CA sk文件
     */
@ApiModelProperty(value = "CA sk文件")
    private String sk;
    /**
     * CA 证书
     */
@ApiModelProperty(value = "CA 证书")
    private String certificate;
    /**
     * ca签名
     */
@ApiModelProperty(value = "ca签名")
    private String flag;
    /**
     * 节点ID
     */
@ApiModelProperty(value = "节点ID")
    private Long peerId;
    /**
     * 日期
     */
@ApiModelProperty(value = "日期")
    private LocalDateTime createTime;
    /**
     * 联盟ID(冗余)
     */
@ApiModelProperty(value = "联盟ID(冗余)")
    private Long leagueId;
    /**
     * 组织ID(冗余)
     */
@ApiModelProperty(value = "组织ID(冗余)")
    private Long orgId;
    /**
     * 节点名称（冗余）
     */
@ApiModelProperty(value = "节点名称（冗余）")
    private String peerName;
    /**
     * 组织名称（冗余）
     */
@ApiModelProperty(value = "组织名称（冗余）")
    private String orgName;
    /**
     * 联盟名称（冗余）
     */
@ApiModelProperty(value = "联盟名称（冗余）")
    private String leagueName;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取CA集合
     */
    public String getName() {
        return name;
    }
    /**
     * 设置CA集合
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取CA sk文件
     */
    public String getSk() {
        return sk;
    }
    /**
     * 设置CA sk文件
     */

    public void setSk(String sk) {
        this.sk = sk;
    }

    /**
     * 获取CA 证书
     */
    public String getCertificate() {
        return certificate;
    }
    /**
     * 设置CA 证书
     */

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    /**
     * 获取ca签名
     */
    public String getFlag() {
        return flag;
    }
    /**
     * 设置ca签名
     */

    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * 获取节点ID
     */
    public Long getPeerId() {
        return peerId;
    }
    /**
     * 设置节点ID
     */

    public void setPeerId(Long peerId) {
        this.peerId = peerId;
    }

    /**
     * 获取日期
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }
    /**
     * 设置日期
     */

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取联盟ID(冗余)
     */
    public Long getLeagueId() {
        return leagueId;
    }
    /**
     * 设置联盟ID(冗余)
     */

    public void setLeagueId(Long leagueId) {
        this.leagueId = leagueId;
    }

    /**
     * 获取组织ID(冗余)
     */
    public Long getOrgId() {
        return orgId;
    }
    /**
     * 设置组织ID(冗余)
     */

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 获取节点名称（冗余）
     */
    public String getPeerName() {
        return peerName;
    }
    /**
     * 设置节点名称（冗余）
     */

    public void setPeerName(String peerName) {
        this.peerName = peerName;
    }

    /**
     * 获取组织名称（冗余）
     */
    public String getOrgName() {
        return orgName;
    }
    /**
     * 设置组织名称（冗余）
     */

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * 获取联盟名称（冗余）
     */
    public String getLeagueName() {
        return leagueName;
    }
    /**
     * 设置联盟名称（冗余）
     */

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * CA集合列的数据库字段名称
     */
    public static final String NAME = "name";

    /**
     * CA sk文件列的数据库字段名称
     */
    public static final String SK = "sk";

    /**
     * CA 证书列的数据库字段名称
     */
    public static final String CERTIFICATE = "certificate";

    /**
     * ca签名列的数据库字段名称
     */
    public static final String FLAG = "flag";

    /**
     * 节点ID列的数据库字段名称
     */
    public static final String PEER_ID = "peer_id";

    /**
     * 日期列的数据库字段名称
     */
    public static final String CREATE_TIME = "create_time";

    /**
     * 联盟ID(冗余)列的数据库字段名称
     */
    public static final String LEAGUE_ID = "league_id";

    /**
     * 组织ID(冗余)列的数据库字段名称
     */
    public static final String ORG_ID = "org_id";

    /**
     * 节点名称（冗余）列的数据库字段名称
     */
    public static final String PEER_NAME = "peer_name";

    /**
     * 组织名称（冗余）列的数据库字段名称
     */
    public static final String ORG_NAME = "org_name";

    /**
     * 联盟名称（冗余）列的数据库字段名称
     */
    public static final String LEAGUE_NAME = "league_name";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FabricCa{" +
        "id=" + id +
        ", name=" + name +
        ", sk=" + sk +
        ", certificate=" + certificate +
        ", flag=" + flag +
        ", peerId=" + peerId +
        ", createTime=" + createTime +
        ", leagueId=" + leagueId +
        ", orgId=" + orgId +
        ", peerName=" + peerName +
        ", orgName=" + orgName +
        ", leagueName=" + leagueName +
        "}";
    }
}
