package com.ybg.block.core.fabricbak.bean;

public enum  ChainCodeIntent {
    INSTALL, INSTANTIATE, UPGRADE;
}
