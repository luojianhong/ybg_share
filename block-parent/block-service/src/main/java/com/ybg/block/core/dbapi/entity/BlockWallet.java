package com.ybg.block.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 钱包表
 * </p>
 *
 * @author yanyu
 * @since 2019-11-16
 */
@ApiModel(value = "钱包表")
public class BlockWallet extends Model<BlockWallet> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 钱包类型
     */
    @ApiModelProperty(value = "钱包类型")
    private String walletType;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;
    /**
     * 文件内容（备份）
     */
    @ApiModelProperty(value = "文件内容（备份）")
    private String fileData;
    /**
     * 钱包地址
     */
    @ApiModelProperty(value = "钱包地址")
    private String address;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取钱包类型
     */
    public String getWalletType() {
        return walletType;
    }

    /**
     * 设置钱包类型
     */

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    /**
     * 获取创建时间
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     */

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     */

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取文件内容（备份）
     */
    public String getFileData() {
        return fileData;
    }

    /**
     * 设置文件内容（备份）
     */

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }

    /**
     * 获取钱包地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置钱包地址
     */

    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 钱包类型列的数据库字段名称
     */
    public static final String WALLET_TYPE = "wallet_type";

    /**
     * 创建时间列的数据库字段名称
     */
    public static final String CREATE_TIME = "create_time";

    /**
     * 密码列的数据库字段名称
     */
    public static final String PASSWORD = "password";

    /**
     * 文件内容（备份）列的数据库字段名称
     */
    public static final String FILE_DATA = "file_data";

    /**
     * 钱包地址列的数据库字段名称
     */
    public static final String ADDRESS = "address";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BlockWallet{" +
                "id=" + id +
                ", walletType=" + walletType +
                ", createTime=" + createTime +
                ", password=" + password +
                ", fileData=" + fileData +
                ", address=" + address +
                "}";
    }
}
