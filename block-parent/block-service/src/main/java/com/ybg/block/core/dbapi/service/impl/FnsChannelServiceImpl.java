package com.ybg.block.core.dbapi.service.impl;

import com.ybg.block.core.dbapi.entity.FnsChannel;
import com.ybg.block.core.dbapi.entity.FnsOrg;
import com.ybg.block.core.dbapi.service.FnsChannelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.block.core.dbapi.vo.FnsChannelVO;
import com.ybg.block.core.dbapi.vo.FnsPeerVO;
import com.ybg.block.core.fabricbak.utils.CacheUtil;
import com.ybg.block.core.fabricbak.utils.DateUtil;
import com.ybg.block.core.fabricbak.utils.DeleteUtil;
import com.ybg.block.core.fabricbak.utils.FabricHelper;
import com.ybg.block.core.dbapi.dao.FnsAppMapper;
import com.ybg.block.core.dbapi.dao.FnsBlockMapper;
import com.ybg.block.core.dbapi.dao.FnsChaincodeMapper;
import com.ybg.block.core.dbapi.dao.FnsChannelMapper;
import com.ybg.block.core.dbapi.dao.FnsLeagueMapper;
import com.ybg.block.core.dbapi.dao.FnsOrgMapper;
import com.ybg.block.core.dbapi.dao.FnsPeerMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
//@Service(version = "1.0.0", interfaceClass = FnsChannelService.class)
@Service
@Slf4j
public class FnsChannelServiceImpl extends ServiceImpl<FnsChannelMapper, FnsChannel> implements FnsChannelService {
    @Resource
    private FnsLeagueMapper leagueMapper;
    @Resource
    private FnsOrgMapper orgMapper;
    @Resource
    private FnsPeerMapper peerMapper;
    @Resource
    private FnsChannelMapper channelMapper;
    @Resource
    private FnsChaincodeMapper chaincodeMapper;
    @Resource
    private FnsAppMapper appMapper;
    @Resource
    private FnsBlockMapper blockMapper;

    @Override
    public int add(FnsChannel channel) {
        if (StringUtils.isEmpty(channel.getName())) {
            log.debug("channel name is empty");
            return 0;
        }
        if (null != channelMapper.check(channel)) {
            log.debug("had the same channel in this peer");
            return 0;
        }
        if (channel.getBlockListener()!=1) {
            channel.setCallbackLocation("");
        }
        channel.setDate(DateUtil.getCurrent("yyyy-MM-dd"));
        CacheUtil.removeHome();
        return channelMapper.add(channel);
    }

    @Override
    public int update(FnsChannel channel) {
        FabricHelper.obtain().removeChaincodeManager(chaincodeMapper.list(channel.getId()));
        CacheUtil.removeHome();
        blockMapper.deleteByChannelId(channel.getId());
        if (channel.getBlockListener()!=1) {
            channel.setCallbackLocation("");
        }
        return channelMapper.update(channel);
    }

    @Override
    public int updateHeight(int id, int height) {
        return channelMapper.updateHeight(id, height);
    }

    @Override
    public List<FnsChannelVO> listAll() {
        List<FnsChannelVO> channels = channelMapper.listAll();
        for (FnsChannelVO channel : channels) {
            FnsPeerVO peer = peerMapper.get(channel.getPeerId());
            FnsOrg org = orgMapper.get(peer.getOrgId());
            channel.setLeagueName(leagueMapper.get(org.getLeagueId()).getName());
            channel.setOrgName(org.getMspId());
            channel.setPeerName(peer.getName());
            channel.setChaincodeCount(chaincodeMapper.count(channel.getId()));
        }
        return channels;
    }

    @Override
    public List<FnsChannelVO> listById(int id) {
        return channelMapper.list(id);
    }

    @Override
    public FnsChannelVO get(int id) {
        return channelMapper.get(id);
    }

    @Override
    public int countById(int id) {
        return channelMapper.count(id);
    }

    @Override
    public int count() {
        return channelMapper.countAll();
    }

    @Override
    public int delete(int id) {
        return DeleteUtil.obtain().deleteChannel(id, channelMapper, chaincodeMapper, appMapper, blockMapper);
    }
}
