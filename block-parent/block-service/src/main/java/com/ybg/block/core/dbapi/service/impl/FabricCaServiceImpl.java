package com.ybg.block.core.dbapi.service.impl;

import com.ybg.block.core.dbapi.entity.FabricCa;
import com.ybg.block.core.dbapi.dao.FabricCaMapper;
import com.ybg.block.core.dbapi.service.FabricCaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 证书授权中心 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
//@Service(version = "1.0.0", interfaceClass = FabricCaService.class)
@Service
public class FabricCaServiceImpl extends ServiceImpl<FabricCaMapper, FabricCa> implements FabricCaService {


}
