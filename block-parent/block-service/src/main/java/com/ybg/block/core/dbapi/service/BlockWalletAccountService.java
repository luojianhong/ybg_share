package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.BlockWalletAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 钱包账户 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-16
 */
public interface BlockWalletAccountService extends IService<BlockWalletAccount> {

}
