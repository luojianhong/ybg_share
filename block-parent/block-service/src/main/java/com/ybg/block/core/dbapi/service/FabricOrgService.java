package com.ybg.block.core.dbapi.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.block.core.dbapi.entity.FabricOrg;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.share.core.fabric.dto.FabircOrgQueryDTO;
import com.ybg.share.core.fabric.vo.FabricOrgVO;

/**
 * <p>
 * 组织 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricOrgService extends IService<FabricOrg> {



    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    Page<FabricOrgVO> pageQuery(FabircOrgQueryDTO dto);
}
