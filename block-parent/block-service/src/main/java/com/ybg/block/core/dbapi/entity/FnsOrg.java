package com.ybg.block.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 组织
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
@ApiModel(value = "组织")
public class FnsOrg extends Model<FnsOrg> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Integer id;
    /**
     * 节点是否开启TLS	根据自身创建网络情况选择true或false
     */
    @ApiModelProperty(value = "节点是否开启TLS	根据自身创建网络情况选择true或false")
    private Integer tls;
    /**
     * 节点所属组织ID
     */
    @ApiModelProperty(value = "节点所属组织ID")
    private String mspId;
    /**
     * 联盟ID
     */
    @ApiModelProperty(value = "联盟ID")
    private Integer leagueId;
    /**
     * 创建日期
     */
    @ApiModelProperty(value = "创建日期")
    private String date;


    /**
     * 获取主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取节点是否开启TLS	根据自身创建网络情况选择true或false
     */
    public Integer getTls() {
        return tls;
    }

    /**
     * 设置节点是否开启TLS	根据自身创建网络情况选择true或false
     */

    public void setTls(Integer tls) {
        this.tls = tls;
    }

    /**
     * 获取节点所属组织ID
     */
    public String getMspId() {
        return mspId;
    }

    /**
     * 设置节点所属组织ID
     */

    public void setMspId(String mspId) {
        this.mspId = mspId;
    }

    /**
     * 获取联盟ID
     */
    public Integer getLeagueId() {
        return leagueId;
    }

    /**
     * 设置联盟ID
     */

    public void setLeagueId(Integer leagueId) {
        this.leagueId = leagueId;
    }

    /**
     * 获取创建日期
     */
    public String getDate() {
        return date;
    }

    /**
     * 设置创建日期
     */

    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 节点是否开启TLS	根据自身创建网络情况选择true或false列的数据库字段名称
     */
    public static final String TLS = "tls";

    /**
     * 节点所属组织ID列的数据库字段名称
     */
    public static final String MSP_ID = "msp_id";

    /**
     * 联盟ID列的数据库字段名称
     */
    public static final String LEAGUE_ID = "league_id";

    /**
     * 创建日期列的数据库字段名称
     */
    public static final String DATE = "date";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FnsOrg{" +
                "id=" + id +
                ", tls=" + tls +
                ", mspId=" + mspId +
                ", leagueId=" + leagueId +
                ", date=" + date +
                "}";
    }
}
