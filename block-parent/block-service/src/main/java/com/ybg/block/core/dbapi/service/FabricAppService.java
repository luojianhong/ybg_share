package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FabricApp;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 应用 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricAppService extends IService<FabricApp> {

}
