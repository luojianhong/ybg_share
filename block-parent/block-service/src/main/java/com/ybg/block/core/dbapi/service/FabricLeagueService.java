package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FabricLeague;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 联盟 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricLeagueService extends IService<FabricLeague> {
    /**
     * 根据名称查询
     * @param name
     * @return
     */
    FabricLeague getByLeagueName(String name);

}
