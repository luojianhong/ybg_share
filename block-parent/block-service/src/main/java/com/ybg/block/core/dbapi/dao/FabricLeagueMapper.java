package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FabricLeague;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 联盟 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricLeagueMapper extends BaseMapper<FabricLeague> {

}
