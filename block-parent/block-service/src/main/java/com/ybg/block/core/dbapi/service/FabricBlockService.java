package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FabricBlock;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 区块 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricBlockService extends IService<FabricBlock> {

}
