package com.ybg.block.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 链码表
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
@ApiModel(value = "链码表")
public class FnsChaincode extends Model<FnsChaincode> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Integer id;
    /**
     * 链码名称
     */
    @ApiModelProperty(value = "链码名称")
    private String name;
    @ApiModelProperty(value = "")
    private String source;
    /**
     * 智能合约路径 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02
     */
    @ApiModelProperty(value = "智能合约路径 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02")
    private String path;
    @ApiModelProperty(value = "")
    private String policy;
    /**
     * 智能合约版本 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的1.0
     */
    @ApiModelProperty(value = "智能合约版本 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的1.0")
    private String version;
    /**
     * 提案请求超时时间以毫秒为单位 默认90000
     */
    @ApiModelProperty(value = "提案请求超时时间以毫秒为单位 默认90000")
    private Integer proposalWaitTime;
    /**
     * 通道ID
     */
    @ApiModelProperty(value = "通道ID")
    private Integer channelId;
    /**
     * 智能合约名称 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令所创建的testcc
     */
    @ApiModelProperty(value = "智能合约名称 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令所创建的testcc")
    private String cc;
    /**
     * 链码事件监听
     */
    @ApiModelProperty(value = "链码事件监听")
    private Integer chaincodeEventListener;
    /**
     * 链码事件监听回调地址
     */
    @ApiModelProperty(value = "链码事件监听回调地址")
    private String callbackLocation;
    /**
     * 链码监听事件名集合
     */
    @ApiModelProperty(value = "链码监听事件名集合")
    private String events;
    /**
     * 日期
     */
    @ApiModelProperty(value = "日期")
    private String date;


    /**
     * 获取主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取链码名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置链码名称
     */

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    /**
     * 获取智能合约路径 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02
     */
    public String getPath() {
        return path;
    }

    /**
     * 设置智能合约路径 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02
     */

    public void setPath(String path) {
        this.path = path;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    /**
     * 获取智能合约版本 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的1.0
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置智能合约版本 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的1.0
     */

    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * 获取提案请求超时时间以毫秒为单位 默认90000
     */
    public Integer getProposalWaitTime() {
        return proposalWaitTime;
    }

    /**
     * 设置提案请求超时时间以毫秒为单位 默认90000
     */

    public void setProposalWaitTime(Integer proposalWaitTime) {
        this.proposalWaitTime = proposalWaitTime;
    }

    /**
     * 获取通道ID
     */
    public Integer getChannelId() {
        return channelId;
    }

    /**
     * 设置通道ID
     */

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    /**
     * 获取智能合约名称 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令所创建的testcc
     */
    public String getCc() {
        return cc;
    }

    /**
     * 设置智能合约名称 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令所创建的testcc
     */

    public void setCc(String cc) {
        this.cc = cc;
    }

    /**
     * 获取链码事件监听
     */
    public Integer getChaincodeEventListener() {
        return chaincodeEventListener;
    }

    /**
     * 设置链码事件监听
     */

    public void setChaincodeEventListener(Integer chaincodeEventListener) {
        this.chaincodeEventListener = chaincodeEventListener;
    }

    /**
     * 获取链码事件监听回调地址
     */
    public String getCallbackLocation() {
        return callbackLocation;
    }

    /**
     * 设置链码事件监听回调地址
     */

    public void setCallbackLocation(String callbackLocation) {
        this.callbackLocation = callbackLocation;
    }

    /**
     * 获取链码监听事件名集合
     */
    public String getEvents() {
        return events;
    }

    /**
     * 设置链码监听事件名集合
     */

    public void setEvents(String events) {
        this.events = events;
    }

    /**
     * 获取日期
     */
    public String getDate() {
        return date;
    }

    /**
     * 设置日期
     */

    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 链码名称列的数据库字段名称
     */
    public static final String NAME = "name";

    public static final String SOURCE = "source";

    /**
     * 智能合约路径 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02列的数据库字段名称
     */
    public static final String PATH = "path";

    public static final String POLICY = "policy";

    /**
     * 智能合约版本 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的1.0列的数据库字段名称
     */
    public static final String VERSION = "version";

    /**
     * 提案请求超时时间以毫秒为单位 默认90000列的数据库字段名称
     */
    public static final String PROPOSAL_WAIT_TIME = "proposal_wait_time";

    /**
     * 通道ID列的数据库字段名称
     */
    public static final String CHANNEL_ID = "channel_id";

    /**
     * 智能合约名称 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令所创建的testcc列的数据库字段名称
     */
    public static final String CC = "cc";

    /**
     * 链码事件监听列的数据库字段名称
     */
    public static final String CHAINCODE_EVENT_LISTENER = "chaincode_event_listener";

    /**
     * 链码事件监听回调地址列的数据库字段名称
     */
    public static final String CALLBACK_LOCATION = "callback_location";

    /**
     * 链码监听事件名集合列的数据库字段名称
     */
    public static final String EVENTS = "events";

    /**
     * 日期列的数据库字段名称
     */
    public static final String DATE = "date";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FnsChaincode{" +
                "id=" + id +
                ", name=" + name +
                ", source=" + source +
                ", path=" + path +
                ", policy=" + policy +
                ", version=" + version +
                ", proposalWaitTime=" + proposalWaitTime +
                ", channelId=" + channelId +
                ", cc=" + cc +
                ", chaincodeEventListener=" + chaincodeEventListener +
                ", callbackLocation=" + callbackLocation +
                ", events=" + events +
                ", date=" + date +
                "}";
    }
}
