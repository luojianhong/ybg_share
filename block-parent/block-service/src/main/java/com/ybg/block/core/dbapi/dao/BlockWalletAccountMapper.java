package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.BlockWalletAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 钱包账户 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-16
 */
public interface BlockWalletAccountMapper extends BaseMapper<BlockWalletAccount> {

}
