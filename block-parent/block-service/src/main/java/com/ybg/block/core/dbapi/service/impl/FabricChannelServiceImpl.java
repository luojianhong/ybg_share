package com.ybg.block.core.dbapi.service.impl;

import com.ybg.block.core.dbapi.entity.FabricChannel;
import com.ybg.block.core.dbapi.dao.FabricChannelMapper;
import com.ybg.block.core.dbapi.service.FabricChannelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 通道 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
//@Service(version = "1.0.0", interfaceClass = FabricChannelService.class)
@Service
public class FabricChannelServiceImpl extends ServiceImpl<FabricChannelMapper, FabricChannel> implements FabricChannelService {


}
