package com.ybg.block.core.fabric;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.block.core.dbapi.entity.FabricLeague;
import com.ybg.block.core.dbapi.entity.FabricOrg;
import com.ybg.block.core.dbapi.service.FabricAppService;
import com.ybg.block.core.dbapi.service.FabricBlockService;
import com.ybg.block.core.dbapi.service.FabricCaService;
import com.ybg.block.core.dbapi.service.FabricChaincodeService;
import com.ybg.block.core.dbapi.service.FabricChannelService;
import com.ybg.block.core.dbapi.service.FabricLeagueService;
import com.ybg.block.core.dbapi.service.FabricOrdererService;
import com.ybg.block.core.dbapi.service.FabricOrgService;
import com.ybg.block.core.dbapi.service.FabricPeerService;
import com.ybg.share.core.fabric.FabricLeagueRemoteApi;
import com.ybg.block.core.fabric.datamapper.FabricLeagueDataMapper;
import com.ybg.share.core.fabric.dto.FabricLeagueEditDTO;
import com.ybg.share.core.fabric.dto.FabricLeagueQueryDTO;
import com.ybg.share.core.fabric.vo.FabricLeagueVO;
import com.ybg.framework.vo.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yanyu
 */
@Slf4j
@Service(version = "${dubbo.provider.version}", timeout = 120000, cache = "false")
public class FabricLeagueRemoteApiImpl implements FabricLeagueRemoteApi {

    @Autowired
    private FabricLeagueService fabricLeagueService;
    @Autowired
    private FabricOrgService fabricOrgService;
    @Autowired
    private FabricOrdererService fabricOrdererService;
    @Autowired
    private FabricPeerService fabricPeerService;
    @Autowired
    private FabricCaService fabricCaService;
    @Autowired
    private FabricChannelService fabricChannelService;
    @Autowired
    private FabricBlockService fabricBlockService;
    @Autowired
    private FabricChaincodeService fabricChaincodeService;
    @Autowired
    private FabricAppService fabricAppService;

    @Override
    public R<?> editLeague(FabricLeagueEditDTO dto) {
        if (StrUtil.isBlank(dto.getName())) {
            return R.fail("名称不能为空");
        }

        FabricLeague fabricLeague = fabricLeagueService.getByLeagueName(dto.getName());
        if (fabricLeague != null) {
            return R.fail("该名称已存在");
        }
        if (dto.getId() != null) {
            FabricLeague fabricLeagueById = fabricLeagueService.getById(dto.getId());
            if (fabricLeagueById == null) {
                return R.fail("该联盟不存在");
            }
            FabricLeague bean = new FabricLeague();
            bean.setName(bean.getName());
            fabricLeagueService.updateById(bean);
        } else {
            FabricLeague bean = new FabricLeague();
            bean.setName(dto.getName());
            bean.setCreateTime(LocalDateTime.now());
            fabricLeagueService.save(bean);
        }
        return R.success();
    }

    @Override
    @Transactional
    public R<?> removeLeague(Long id) {
        fabricLeagueService.removeById(id);
        //删除联盟下的绑定的其他信息
        Map<String, Object> map= new LinkedHashMap<>(1);
        map.put(FabricOrg.LEAGUE_ID,id);
        fabricOrgService.removeByMap(map);
        fabricOrdererService.removeByMap(map);
        fabricPeerService.removeByMap(map);
        fabricCaService.removeByMap(map);
        fabricChannelService.removeByMap(map);
        fabricBlockService.removeByMap(map);
        fabricChaincodeService.removeByMap(map);
        fabricAppService.removeByMap(map);
        return R.success();
    }

    @Override
    public R<FabricLeagueVO> getLeague(Long id) {
        FabricLeague fabricLeague;
        fabricLeague = fabricLeagueService.getById(id);
        FabricLeagueVO fabricLeagueVO = FabricLeagueDataMapper.INSTANCE.toVO(fabricLeague);
        return R.success(fabricLeagueVO);
    }

    @Override
    public R<Page<FabricLeagueVO>> page(FabricLeagueQueryDTO dto) {
        IPage<FabricLeague> page = new Page<>(dto.getCurrentPage(), dto.getSize());
        QueryWrapper<FabricLeague> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc(FabricLeague.CREATE_TIME);
        fabricLeagueService.page(page, wrapper);
        Page<FabricLeagueVO> pageResult = new Page<>(dto.getCurrentPage(), dto.getSize());
        pageResult.setTotal(page.getTotal());
        pageResult.setRecords(FabricLeagueDataMapper.INSTANCE.toVOList(page.getRecords()));
        return R.success(pageResult);
    }

    @Override
    public R<List<FabricLeagueVO>> allLeague() {
        List<FabricLeague> fabricLeagueList = fabricLeagueService.list();
        return R.success(FabricLeagueDataMapper.INSTANCE.toVOList(fabricLeagueList));
    }
}
