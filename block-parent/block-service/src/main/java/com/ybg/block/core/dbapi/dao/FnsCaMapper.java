package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FnsCa;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ybg.block.core.dbapi.vo.FnsCaVO;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsCaMapper extends BaseMapper<FnsCa> {
    @Insert("insert into fns_ca (name,sk,certificate,flag,peer_id,date) values " +
            "(#{c.name},#{c.sk},#{c.certificate},#{c.flag},#{c.peerId},#{c.date})")
    int add(@Param("c") FnsCaVO ca);

    @Update("update fns_ca set name=#{c.name},sk=#{c.sk},certificate=#{c.certificate},flag=#{c.flag} where id=#{c.id}")
    int update(@Param("c") FnsCaVO ca);

    @Update("update fns_ca set name=#{c.name},flag=#{c.flag} where id=#{c.id}")
    int updateWithNoFile(@Param("c") FnsCaVO ca);

    @Select("select count(name) from fns_ca where peer_id=#{id}")
    int count(@Param("id") int id);

    @Select("select count(name) from fns_ca")
    int countAll();

    @Delete("delete from fns_ca where id=#{id}")
    int delete(@Param("id") int id);

    @Delete("delete from fns_ca where peer_id=#{peerId}")
    int deleteAll(@Param("peerId") int peerId);

    @Select("select id,name,sk,certificate,flag,peer_id,date from fns_ca where name=#{c.name} and peer_id=#{c.peerId}")
//    @Results({
//            @Result(property = "peerId", column = "peer_id")
//    })
    FnsCaVO check(@Param("c") FnsCaVO ca);

    @Select("select id,name,sk,certificate,flag,peer_id,date from fns_ca where id=#{id}")
    @Results({
            @Result(property = "peerId", column = "peer_id")
    })
    FnsCa get(@Param("id") int id);

    @Select("select id,name,sk,certificate,flag,peer_id,date from fns_ca where flag=#{flag}")
    @Results({
            @Result(property = "peerId", column = "peer_id")
    })
    FnsCa getByFlag(@Param("flag") String flag);

    @Select("select id,name,sk,certificate,flag,peer_id,date from fns_ca where peer_id=#{id}")
    @Results({
            @Result(property = "peerId", column = "peer_id")
    })
    List<FnsCa> list(@Param("id") int id);

    @Select("select id,name,sk,certificate,flag,peer_id,date from fns_ca")
//    @Results({
//            @Result(property = "peerId", column = "peer_id")
//    })
    List<FnsCaVO> listAll();
}
