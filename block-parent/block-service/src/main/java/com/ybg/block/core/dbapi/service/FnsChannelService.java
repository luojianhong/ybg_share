package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FnsChannel;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.block.core.dbapi.vo.FnsChannelVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsChannelService extends IService<FnsChannel> {
    int add(FnsChannel channel);

    int update(FnsChannel channel);

    int updateHeight(int id, int height);

    List<FnsChannelVO> listAll();

    List<FnsChannelVO> listById(int id);

    FnsChannelVO get(int id);

    int countById(int id);

    int count();

    int delete(int id);
}
