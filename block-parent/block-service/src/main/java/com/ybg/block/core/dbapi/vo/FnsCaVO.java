/*
 * Copyright (c) 2018. Aberic - All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ybg.block.core.dbapi.vo;


import lombok.Data;

import java.io.Serializable;


/**
 * 作者：Aberic on 2018/7/12 21:02
 * @author yanyu 修改 2019年11月22日
 * 邮箱：abericyang@gmail.com
 */
@Data
public class FnsCaVO implements Serializable {

    private int id;
    private String name;
    private String sk;
    private String certificate;
    private String flag; // optional
    private int peerId;
    private String date; // optional
    private String peerName; // optional
    private String orgName; // optional
    private String leagueName; // optional

}
