package com.ybg.block.core.dbapi.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 组织
 * </p>
 *
 * @author yanyu
 * @since 2019-11-29
 */
@ApiModel(value="组织") 
public class FabricOrg extends Model<FabricOrg> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
@ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 节点是否开启TLS	根据自身创建网络情况选择true或false
     */
@ApiModelProperty(value = "节点是否开启TLS	根据自身创建网络情况选择true或false")
    private Boolean tls;
    /**
     * 节点所属组织ID
     */
@ApiModelProperty(value = "节点所属组织ID")
    private String mspId;
    /**
     * 联盟ID
     */
@ApiModelProperty(value = "联盟ID")
    private Long leagueId;
    /**
     * 创建日期
     */
@ApiModelProperty(value = "创建日期")
    private LocalDateTime createTime;
    /**
     * 联盟名称（冗余）
     */
@ApiModelProperty(value = "联盟名称（冗余）")
    private String leagueName;
    /**
     * 节点数量（冗余）
     */
@ApiModelProperty(value = "节点数量（冗余）")
    private Integer peerCount;
    /**
     * 排序服务数量（冗余）
     */
@ApiModelProperty(value = "排序服务数量（冗余）")
    private Integer ordererCount;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取节点是否开启TLS	根据自身创建网络情况选择true或false
     */
    public Boolean getTls() {
        return tls;
    }
    /**
     * 设置节点是否开启TLS	根据自身创建网络情况选择true或false
     */

    public void setTls(Boolean tls) {
        this.tls = tls;
    }

    /**
     * 获取节点所属组织ID
     */
    public String getMspId() {
        return mspId;
    }
    /**
     * 设置节点所属组织ID
     */

    public void setMspId(String mspId) {
        this.mspId = mspId;
    }

    /**
     * 获取联盟ID
     */
    public Long getLeagueId() {
        return leagueId;
    }
    /**
     * 设置联盟ID
     */

    public void setLeagueId(Long leagueId) {
        this.leagueId = leagueId;
    }

    /**
     * 获取创建日期
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }
    /**
     * 设置创建日期
     */

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取联盟名称（冗余）
     */
    public String getLeagueName() {
        return leagueName;
    }
    /**
     * 设置联盟名称（冗余）
     */

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    /**
     * 获取节点数量（冗余）
     */
    public Integer getPeerCount() {
        return peerCount;
    }
    /**
     * 设置节点数量（冗余）
     */

    public void setPeerCount(Integer peerCount) {
        this.peerCount = peerCount;
    }

    /**
     * 获取排序服务数量（冗余）
     */
    public Integer getOrdererCount() {
        return ordererCount;
    }
    /**
     * 设置排序服务数量（冗余）
     */

    public void setOrdererCount(Integer ordererCount) {
        this.ordererCount = ordererCount;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 节点是否开启TLS	根据自身创建网络情况选择true或false列的数据库字段名称
     */
    public static final String TLS = "tls";

    /**
     * 节点所属组织ID列的数据库字段名称
     */
    public static final String MSP_ID = "msp_id";

    /**
     * 联盟ID列的数据库字段名称
     */
    public static final String LEAGUE_ID = "league_id";

    /**
     * 创建日期列的数据库字段名称
     */
    public static final String CREATE_TIME = "create_time";

    /**
     * 联盟名称（冗余）列的数据库字段名称
     */
    public static final String LEAGUE_NAME = "league_name";

    /**
     * 节点数量（冗余）列的数据库字段名称
     */
    public static final String PEER_COUNT = "peer_count";

    /**
     * 排序服务数量（冗余）列的数据库字段名称
     */
    public static final String ORDERER_COUNT = "orderer_count";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FabricOrg{" +
        "id=" + id +
        ", tls=" + tls +
        ", mspId=" + mspId +
        ", leagueId=" + leagueId +
        ", createTime=" + createTime +
        ", leagueName=" + leagueName +
        ", peerCount=" + peerCount +
        ", ordererCount=" + ordererCount +
        "}";
    }
}
