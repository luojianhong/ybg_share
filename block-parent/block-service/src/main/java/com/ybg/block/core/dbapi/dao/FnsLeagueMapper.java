package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FnsLeague;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ybg.block.core.dbapi.vo.FnsLeagueVO;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsLeagueMapper extends BaseMapper<FnsLeague> {
    @Insert("insert into fns_league  (name,date) values (#{l.name},#{l.date})")
    int add(@Param("l") FnsLeague league);

    @Update("update fns_league set name=#{l.name} where id=#{l.id}")
    int update(@Param("l") FnsLeague league);

    @Delete("delete from fns_league where id=#{id}")
    int delete(@Param("id") int id);

    @Select("select id,name,date from fns_league where id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "date", column = "date")
    })
    FnsLeague get(@Param("id") int id);

    @Select("select id,name,date from fns_league")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "name", column = "name"),
//            @Result(property = "date", column = "date")
//    })
    List<FnsLeagueVO> listAll();
}
