package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FnsOrg;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.block.core.dbapi.vo.FnsLeagueVO;
import com.ybg.block.core.dbapi.vo.FnsOrgVO;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsOrgService extends IService<FnsOrg> {
    int add(FnsOrg org);

    int update(FnsOrg org);

    List<FnsOrgVO> listAll();

    List<FnsOrgVO> listById(int id);

    FnsOrg get(int id);

    int countById(int id);

    int count();

    int delete(int id);

    List<FnsLeagueVO> listAllLeague();
}
