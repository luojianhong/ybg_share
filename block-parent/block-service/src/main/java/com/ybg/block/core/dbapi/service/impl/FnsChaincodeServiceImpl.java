package com.ybg.block.core.dbapi.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ybg.block.core.fabricbak.base.BaseService;
import com.ybg.block.core.fabricbak.sdk.FabricManager;
import com.ybg.block.core.dbapi.service.FnsChaincodeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.block.core.dbapi.vo.FnsCaVO;
import com.ybg.block.core.dbapi.vo.FnsChaincodeVO;
import com.ybg.block.core.dbapi.vo.FnsChannelVO;
import com.ybg.block.core.dbapi.vo.FnsPeerVO;
import com.ybg.block.core.fabricbak.bean.Api;
import com.ybg.block.core.fabricbak.bean.ChainCodeIntent;
import com.ybg.block.core.fabricbak.bean.State;
import com.ybg.block.core.fabricbak.bean.Trace;
import com.ybg.block.core.dbapi.dao.FnsAppMapper;
import com.ybg.block.core.dbapi.dao.FnsCaMapper;
import com.ybg.block.core.dbapi.dao.FnsChaincodeMapper;
import com.ybg.block.core.dbapi.dao.FnsChannelMapper;
import com.ybg.block.core.dbapi.dao.FnsLeagueMapper;
import com.ybg.block.core.dbapi.dao.FnsOrdererMapper;
import com.ybg.block.core.dbapi.dao.FnsOrgMapper;
import com.ybg.block.core.dbapi.dao.FnsPeerMapper;
import com.ybg.block.core.dbapi.entity.FnsCa;
import com.ybg.block.core.dbapi.entity.FnsChaincode;
import com.ybg.block.core.dbapi.entity.FnsLeague;
import com.ybg.block.core.dbapi.entity.FnsOrg;
import com.ybg.block.core.fabricbak.utils.CacheUtil;
import com.ybg.block.core.fabricbak.utils.DateUtil;
import com.ybg.block.core.fabricbak.utils.DeleteUtil;
import com.ybg.block.core.fabricbak.utils.FabricHelper;
import com.ybg.block.core.fabricbak.utils.FileUtil;
import com.ybg.block.core.fabricbak.utils.MD5Util;
import com.ybg.block.core.fabricbak.utils.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
//@Service(version = "1.0.0", interfaceClass = FnsChaincodeService.class)
@Service
public class FnsChaincodeServiceImpl extends ServiceImpl<FnsChaincodeMapper, FnsChaincode> implements FnsChaincodeService {
    @Autowired
    private FnsLeagueMapper leagueMapper;
    @Autowired
    private FnsOrgMapper orgMapper;
    @Autowired
    private FnsOrdererMapper ordererMapper;
    @Autowired
    private FnsPeerMapper peerMapper;
    @Autowired
    private FnsCaMapper caMapper;
    @Autowired
    private FnsChannelMapper channelMapper;
    @Autowired
    private FnsChaincodeMapper chaincodeMapper;
    @Autowired
    private FnsAppMapper appMapper;
    @Autowired
    private Environment env;

    @Override
    public int add(FnsChaincodeVO chaincode) {
        if (StringUtils.isEmpty(chaincode.getName()) ||
                StringUtils.isEmpty(chaincode.getPath()) ||
                StringUtils.isEmpty(chaincode.getVersion()) ||
                chaincode.getProposalWaitTime() == 0 ||
                null != chaincodeMapper.check(chaincode)) {
            return 0;
        }
        chaincode.setCc(createCC(chaincode));
        chaincode.setDate(DateUtil.getCurrent("yyyy-MM-dd"));
        CacheUtil.removeHome();
        return chaincodeMapper.add(chaincode);
    }

    @Override
    public JSONObject install(FnsChaincodeVO chaincode, MultipartFile file, Api api, boolean init) {
        if (verify(chaincode) || null == file || null != chaincodeMapper.check(chaincode)) {
            return responseFailJson("install error, param has none value and source mush be uploaded or had the same chaincode");
        }
        if (!upload(chaincode, file)) {
            return responseFailJson("source unzip fail");
        }
        CacheUtil.removeHome();
        chaincode.setCc(createCC(chaincode));
        if (chaincodeMapper.add(chaincode) <= 0) {
            return responseFailJson("chaincode add fail");
        }
        chaincode.setId(chaincodeMapper.check(chaincode).getId());
        JSONObject jsonResult = chainCode(chaincode.getId(), caMapper.getByFlag(chaincode.getFlag()), ChainCodeIntent.INSTALL, new String[]{});
        if (jsonResult.getInteger("code") == BaseService.FAIL) {
            delete(chaincode.getId());
            return jsonResult;
        }
        return instantiate(chaincode, Arrays.asList(api.getExec().split(",")));
    }

    @Override
    public JSONObject upgrade(FnsChaincodeVO chaincode, MultipartFile file, Api api) {
        if (verify(chaincode) || null == file || null == chaincodeMapper.get(chaincode.getId())) {
            return responseFailJson("install error, param has none value and source mush be uploaded or had no chaincode to upgrade");
        }
        if (!upload(chaincode, file)) {
            return responseFailJson("source unzip fail");
        }
        FabricHelper.obtain().removeChaincodeManager(chaincode.getCc());
        CacheUtil.removeHome();
        if (chaincodeMapper.updateForUpgrade(chaincode) <= 0) {
            return responseFailJson("chaincode updateForUpgrade fail");
        }
        FnsCa ca = caMapper.getByFlag(chaincode.getFlag());
        JSONObject jsonResult = chainCode(chaincode.getId(), ca, ChainCodeIntent.INSTALL, new String[]{});
        if (jsonResult.getInteger("code") == BaseService.FAIL) {
            delete(chaincode.getId());
            return jsonResult;
        }
        List<String> strArray = Arrays.asList(api.getExec().split(","));
        int size = strArray.size();
        String[] args = new String[size];
        for (int i = 0; i < size; i++) {
            args[i] = strArray.get(i);
        }
        return chainCode(chaincode.getId(), ca, ChainCodeIntent.UPGRADE, args);
    }

    @Override
    public JSONObject instantiate(FnsChaincodeVO chaincode, List<String> strArray) {
        int size = strArray.size();
        String[] args = new String[size];
        for (int i = 0; i < size; i++) {
            args[i] = strArray.get(i);
        }
        // TODO
        return chainCode(chaincode.getId(), caMapper.getByFlag(chaincode.getFlag()), ChainCodeIntent.INSTANTIATE, args);
    }

    @Override
    public int update(FnsChaincodeVO chaincode) {
        chaincode.setCc(createCC(chaincode));
        FabricHelper.obtain().removeChaincodeManager(chaincode.getCc());
        CacheUtil.removeHome();
        return chaincodeMapper.update(chaincode);
    }

    @Override
    public List<FnsChaincodeVO> listAll() {
        return chaincodeMapper.listAll();
    }

    @Override
    public List<FnsChaincode> listById(int id) {
        return chaincodeMapper.list(id);
    }

    @Override
    public FnsChaincodeVO get(int id) {
        return chaincodeMapper.get(id);
    }

    @Override
    public int countById(int id) {
        return chaincodeMapper.count(id);
    }

    @Override
    public int count() {
        return chaincodeMapper.countAll();
    }

    @Override
    public int delete(int id) {
        return DeleteUtil.obtain().deleteChaincode(id, chaincodeMapper, appMapper);
    }

    @Override
    public int deleteAll(int channelId) {
        List<FnsChaincode> chaincodes = chaincodeMapper.list(channelId);
        for (FnsChaincode chaincode : chaincodes) {
            FabricHelper.obtain().removeChaincodeManager(chaincode.getCc());
            chaincodeMapper.delete(chaincode.getId());
        }
        return 0;
    }

    @Override
    public FnsChaincodeVO getInstantiateChaincode(Api api, int chaincodeId) {
        FnsChaincodeVO chaincode = chaincodeMapper.get(chaincodeId);
        FnsChannelVO channel = channelMapper.get(chaincode.getChannelId());
        FnsPeerVO peer = peerMapper.get(channel.getPeerId());
        FnsOrg org = orgMapper.get(peer.getOrgId());
        FnsLeague league = leagueMapper.get(org.getLeagueId());
        chaincode.setLeagueName(league.getName());
        chaincode.setOrgName(org.getMspId());
        chaincode.setPeerName(peer.getName());
        chaincode.setChannelName(channel.getName());
        chaincode.setFlag(api.getFlag());
        return chaincode;
    }

    @Override
    public FnsChaincodeVO getEditChaincode(int chaincodeId) {
        FnsChaincodeVO chaincode = chaincodeMapper.get(chaincodeId);
        FnsPeerVO peer = peerMapper.get(channelMapper.get(chaincode.getChannelId()).getPeerId());
        FnsOrg org = orgMapper.get(peer.getOrgId());
        FnsLeague league = leagueMapper.get(org.getLeagueId());
        chaincode.setPeerName(peer.getName());
        chaincode.setOrgName(org.getMspId());
        chaincode.setLeagueName(league.getName());
        return chaincode;
    }

    @Override
    public List<FnsChannelVO> getEditChannels(FnsChaincodeVO chaincode) {
        List<FnsChannelVO> channels = channelMapper.list(channelMapper.get(chaincode.getChannelId()).getPeerId());
        for (FnsChannelVO channel : channels) {
            channel.setPeerName(chaincode.getPeerName());
            channel.setOrgName(chaincode.getOrgName());
            channel.setLeagueName(chaincode.getLeagueName());
        }
        return channels;
    }

    @Override
    public FnsCa getCAByFlag(String flag) {
        return caMapper.getByFlag(flag);
    }

    @Override
    public List<Api> getApis() {
        List<Api> apis = new ArrayList<>();
        Api apiInvoke = new Api(SpringUtil.get("chaincode_invoke"), Api.Intent.INVOKE.getIndex());
        Api apiQuery = new Api(SpringUtil.get("chaincode_query"), Api.Intent.QUERY.getIndex());
        Api api = new Api(SpringUtil.get("chaincode_block_info"), Api.Intent.INFO.getIndex());
        Api apiHash = new Api(SpringUtil.get("chaincode_block_get_by_hash"), Api.Intent.HASH.getIndex());
        Api apiTxid = new Api(SpringUtil.get("chaincode_block_get_by_txid"), Api.Intent.TXID.getIndex());
        Api apiNumber = new Api(SpringUtil.get("chaincode_block_get_by_height"), Api.Intent.NUMBER.getIndex());
        apis.add(apiInvoke);
        apis.add(apiQuery);
        apis.add(api);
        apis.add(apiHash);
        apis.add(apiTxid);
        apis.add(apiNumber);
        return apis;
    }

    @Override
    public List<FnsCa> getCAs(int chaincodeId) {
        return caMapper.list(channelMapper.get(chaincodeMapper.get(chaincodeId).getChannelId()).getPeerId());
    }

    @Override
    public List<FnsCaVO> getAllCAs() {
        List<FnsCaVO> cas = caMapper.listAll();
        for (FnsCaVO ca : cas) {
            FnsPeerVO peer = peerMapper.get(ca.getPeerId());
            FnsOrg org = orgMapper.get(peer.getOrgId());
            ca.setPeerName(peer.getName());
            ca.setOrgName(org.getMspId());
            ca.setLeagueName(leagueMapper.get(org.getLeagueId()).getName());
        }
        return cas;
    }

    @Override
    public List<FnsChaincodeVO> getChaincodes() {
        List<FnsChaincodeVO> chaincodes = chaincodeMapper.listAll();
        for (FnsChaincodeVO chaincode : chaincodes) {
            FnsChannelVO channel = channelMapper.get(chaincode.getChannelId());
            chaincode.setChannelName(channel.getName());
            chaincode.setPeerName(peerMapper.get(channel.getPeerId()).getName());
        }
        return chaincodes;
    }

    @Override
    public List<FnsChannelVO> getChannelFullList() {
        List<FnsChannelVO> channels = channelMapper.listAll();
        for (FnsChannelVO channel : channels) {
            FnsPeerVO peer = peerMapper.get(channel.getPeerId());
            channel.setPeerName(peer.getName());
            FnsOrg org = orgMapper.get(peer.getOrgId());
            channel.setOrgName(org.getMspId());
            FnsLeague league = leagueMapper.get(org.getLeagueId());
            channel.setLeagueName(league.getName());
        }
        return channels;
    }

    @Override
    public State getState(int id, Api api) {
        FnsChaincodeVO chaincode = chaincodeMapper.get(id);
        State state = new State();
        state.setKey(api.getKey());
        state.setChannelId(chaincode.getChannelId());
        state.setFlag(api.getFlag());
        state.setVersion(api.getVersion());
        state.setStrArray(Arrays.asList(api.getExec().trim().split(",")));
        return state;
    }

    @Override
    public String formatState(State state) {
        JSONObject jsonObject = new JSONObject();
        if (org.apache.commons.lang3.StringUtils.isNotBlank(state.getKey())) {
            jsonObject.put("key", state.getKey());
            jsonObject.put("flag", state.getFlag());
        }
        JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(state.getStrArray()));
        jsonObject.put("strArray", jsonArray);
        return jsonObject.toJSONString();
    }

    @Override
    public Trace getTrace(Api api) {
        Trace trace = new Trace();
        trace.setFlag(api.getFlag());
        trace.setKey(api.getKey());
        trace.setVersion(api.getVersion());
        trace.setTrace(api.getExec().trim());
        return trace;
    }

    @Override
    public String formatTrace(Trace trace) {
        JSONObject jsonObject = new JSONObject();
        if (org.apache.commons.lang3.StringUtils.isNotBlank(trace.getKey())) {
            jsonObject.put("key", trace.getKey());
            jsonObject.put("flag", trace.getFlag());
        }
        jsonObject.put("trace", trace.getTrace());
        return jsonObject.toJSONString();
    }

    @Override
    public FnsChaincodeVO resetChaincode(FnsChaincodeVO chaincode) {
        FnsChannelVO channel = channelMapper.get(chaincode.getChannelId());
        FnsPeerVO peer = peerMapper.get(channel.getPeerId());
        FnsOrg org = orgMapper.get(peer.getOrgId());
        FnsLeague league = leagueMapper.get(org.getLeagueId());
        chaincode.setLeagueName(league.getName());
        chaincode.setOrgName(org.getMspId());
        chaincode.setPeerName(peer.getName());
        chaincode.setChannelName(channel.getName());
        return chaincode;
    }

//    enum ChainCodeIntent {
//        INSTALL, INSTANTIATE, UPGRADE
//    }

    private JSONObject chainCode(int chaincodeId, FnsCa ca, ChainCodeIntent intent, String[] args) {
        JSONObject jsonObject = null;
        try {
            FabricManager manager = FabricHelper.obtain().get(leagueMapper, orgMapper, channelMapper, chaincodeMapper, ordererMapper, peerMapper,
                    ca, chaincodeMapper.get(chaincodeId).getCc());
            switch (intent) {
                case INSTALL:
                    jsonObject = manager.install();
                    break;
                case INSTANTIATE:
                    jsonObject = manager.instantiate(args);
                    break;
                case UPGRADE:
                    jsonObject = manager.upgrade(args);
                    break;
            }
            return jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
            return responseFailJson(String.format("Request failed： %s", e.getMessage()));
        }
    }

    private boolean verify(FnsChaincodeVO chaincode) {
        return StringUtils.isEmpty(chaincode.getName()) ||
                StringUtils.isEmpty(chaincode.getVersion()) ||
                chaincode.getProposalWaitTime() == 0;
    }

    private boolean upload(FnsChaincodeVO chaincode, MultipartFile file) {
        String chaincodeSource = String.format("%s%s%s%s%s%s%s%s%s%schaincode",
                env.getProperty("config.dir"),
                File.separator,
                chaincode.getLeagueName(),
                File.separator,
                chaincode.getOrgName(),
                File.separator,
                chaincode.getPeerName(),
                File.separator,
                chaincode.getChannelName(),
                File.separator);
        String chaincodePath = Objects.requireNonNull(file.getOriginalFilename()).split("\\.")[0];
        String childrenPath = String.format("%s%ssrc%s%s", chaincodeSource, File.separator, File.separator, Objects.requireNonNull(file.getOriginalFilename()).split("\\.")[0]);
        chaincode.setSource(chaincodeSource);
        chaincode.setPath(chaincodePath);
        chaincode.setPolicy(String.format("%s%spolicy.yaml", childrenPath, File.separator));
        chaincode.setDate(DateUtil.getCurrent("yyyy-MM-dd"));
        try {
            FileUtil.unZipAndSave(file, String.format("%s%ssrc", chaincodeSource, File.separator), childrenPath);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private String createCC(FnsChaincodeVO chaincode) {
        FnsChannelVO channel = channelMapper.get(chaincode.getChannelId());
        FnsPeerVO peer = peerMapper.get(channel.getPeerId());
        FnsOrg org = orgMapper.get(peer.getOrgId());
        FnsLeague league = leagueMapper.get(org.getLeagueId());
        return MD5Util.md5(league.getName() + org.getMspId() + peer.getName() + channel.getName() + chaincode.getName());
    }
}
