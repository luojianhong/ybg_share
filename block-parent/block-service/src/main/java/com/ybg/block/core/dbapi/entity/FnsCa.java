package com.ybg.block.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 证书授权中心
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
@ApiModel(value = "证书授权中心")
public class FnsCa extends Model<FnsCa> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Integer id;
    /**
     * CA集合
     */
    @ApiModelProperty(value = "CA集合")
    private String name;
    /**
     * CA sk文件
     */
    @ApiModelProperty(value = "CA sk文件")
    private String sk;
    /**
     * CA 证书
     */
    @ApiModelProperty(value = "CA 证书")
    private String certificate;
    @ApiModelProperty(value = "")
    private String flag;
    /**
     * 节点ID
     */
    @ApiModelProperty(value = "节点ID")
    private Integer peerId;
    /**
     * 日期
     */
    @ApiModelProperty(value = "日期")
    private String date;


    /**
     * 获取主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取CA集合
     */
    public String getName() {
        return name;
    }

    /**
     * 设置CA集合
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取CA sk文件
     */
    public String getSk() {
        return sk;
    }

    /**
     * 设置CA sk文件
     */

    public void setSk(String sk) {
        this.sk = sk;
    }

    /**
     * 获取CA 证书
     */
    public String getCertificate() {
        return certificate;
    }

    /**
     * 设置CA 证书
     */

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * 获取节点ID
     */
    public Integer getPeerId() {
        return peerId;
    }

    /**
     * 设置节点ID
     */

    public void setPeerId(Integer peerId) {
        this.peerId = peerId;
    }

    /**
     * 获取日期
     */
    public String getDate() {
        return date;
    }

    /**
     * 设置日期
     */

    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * CA集合列的数据库字段名称
     */
    public static final String NAME = "name";

    /**
     * CA sk文件列的数据库字段名称
     */
    public static final String SK = "sk";

    /**
     * CA 证书列的数据库字段名称
     */
    public static final String CERTIFICATE = "certificate";

    public static final String FLAG = "flag";

    /**
     * 节点ID列的数据库字段名称
     */
    public static final String PEER_ID = "peer_id";

    /**
     * 日期列的数据库字段名称
     */
    public static final String DATE = "date";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FnsCa{" +
                "id=" + id +
                ", name=" + name +
                ", sk=" + sk +
                ", certificate=" + certificate +
                ", flag=" + flag +
                ", peerId=" + peerId +
                ", date=" + date +
                "}";
    }
}
