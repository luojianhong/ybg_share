package com.ybg.block.core.dbapi.service.impl;

import com.ybg.block.core.dbapi.entity.FabricOrderer;
import com.ybg.block.core.dbapi.dao.FabricOrdererMapper;
import com.ybg.block.core.dbapi.service.FabricOrdererService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 排序服务 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
//@Service(version = "1.0.0", interfaceClass = FabricOrdererService.class)
@Service
public class FabricOrdererServiceImpl extends ServiceImpl<FabricOrdererMapper, FabricOrderer> implements FabricOrdererService {


}
