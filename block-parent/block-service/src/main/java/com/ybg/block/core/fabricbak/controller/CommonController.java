/*
 * Copyright (c) 2018. Aberic - aberic@qq.com - All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ybg.block.core.fabricbak.controller;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.ybg.block.core.dbapi.service.FnsAppService;
import com.ybg.block.core.dbapi.service.FnsBlockService;
import com.ybg.block.core.dbapi.service.FnsCaService;
import com.ybg.block.core.dbapi.service.FnsChaincodeService;
import com.ybg.block.core.dbapi.service.FnsChannelService;
import com.ybg.block.core.dbapi.service.FnsLeagueService;
import com.ybg.block.core.dbapi.service.FnsOrdererService;
import com.ybg.block.core.dbapi.service.FnsOrgService;
import com.ybg.block.core.dbapi.service.FnsPeerService;
import com.ybg.block.core.fabricbak.bean.Home;
import com.ybg.block.core.fabricbak.utils.CacheUtil;
import com.ybg.block.core.fabricbak.utils.DataUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 描述：
 *
 * @author : Aberic 【2018/6/4 15:01】
 */
@RestController
@RequestMapping("")
public class CommonController {

    @Autowired
    private FnsLeagueService leagueService;
    @Autowired
    private FnsOrgService orgService;
    @Autowired
    private FnsOrdererService ordererService;
    @Autowired
    private FnsPeerService peerService;
    @Autowired
    private FnsCaService caService;
    @Autowired
    private FnsChannelService channelService;
    @Autowired
    private FnsChaincodeService chaincodeService;
    @Autowired
    private FnsAppService appService;
    @Autowired
    private FnsBlockService blockService;


    @GetMapping(value = "index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("index");

        Home home = CacheUtil.getHome();
        if (null == home) {
            home = DataUtil.obtain().home(leagueService, orgService, ordererService,
                    peerService, caService, channelService, chaincodeService,
                    appService, blockService);
            CacheUtil.putHome(home);
        }

        modelAndView.addObject("leagueCount", home.getLeagueCount());
        modelAndView.addObject("orgCount", home.getOrgCount());
        modelAndView.addObject("ordererCount", home.getOrdererCount());
        modelAndView.addObject("peerCount", home.getPeerCount());
        modelAndView.addObject("caCount", home.getCaCount());
        modelAndView.addObject("channelCount", home.getChannelCount());
        modelAndView.addObject("chaincodeCount", home.getChaincodeCount());
        modelAndView.addObject("appCount", home.getAppCount());
        modelAndView.addObject("blocks", home.getBlocks());
        //中间统计模块开始
        modelAndView.addObject("channelPercents", new JSONArray(home.getChannelPercents()).toString());
        modelAndView.addObject("channelBlockLists", new JSONArray(home.getChannelBlockLists()).toString());
        modelAndView.addObject("blockDaos", home.getBlockDaos());
        modelAndView.addObject("dayStatistics", home.getDayStatistics() != null ? home.getDayStatistics() : 0);
        modelAndView.addObject("platform", home.getPlatform());
        modelAndView.addObject("dayBlocks", home.getDayBlocks());
        modelAndView.addObject("dayTxs", home.getDayTxs());
        modelAndView.addObject("dayRWs", home.getDayRWs());
        modelAndView.addObject("dayBlocksJson", new JSONObject(home.getDayBlocks()));
        modelAndView.addObject("dayTxsJson", new JSONObject(home.getDayTxs()));
        modelAndView.addObject("dayRWsJson", new JSONObject(home.getDayRWs()));

        //中间统计模块结束

        return modelAndView;
    }


}
