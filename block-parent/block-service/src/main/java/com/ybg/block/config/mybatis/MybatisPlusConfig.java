package com.ybg.block.config.mybatis;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mybatis配置
 */
@Configuration
public class MybatisPlusConfig {


    /**
     * mybatis-plus分页插件<br>
     * 文档：http://mp.baomidou.com<br>
     */
    @Bean
    @Deprecated
    public PaginationInterceptor paginationInterceptor() {
       return new PaginationInterceptor();

    }


    /**
     * mybatis-plus SQL执行效率插件【生产环境可以关闭】
     */
    // @Bean
    public PerformanceInterceptor performanceInterceptor() {
        return new PerformanceInterceptor();
    }





}