package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.BlockWallet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 钱包表 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-15
 */
public interface BlockWalletMapper extends BaseMapper<BlockWallet> {

}
