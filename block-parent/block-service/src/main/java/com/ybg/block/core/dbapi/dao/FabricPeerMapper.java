package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FabricPeer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 节点 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricPeerMapper extends BaseMapper<FabricPeer> {

}
