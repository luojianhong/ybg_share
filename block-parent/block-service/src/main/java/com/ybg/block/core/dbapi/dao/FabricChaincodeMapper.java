package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FabricChaincode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 链码表 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricChaincodeMapper extends BaseMapper<FabricChaincode> {

}
