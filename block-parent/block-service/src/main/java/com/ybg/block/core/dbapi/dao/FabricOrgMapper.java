package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.dao.provider.FabricOrgSQL;
import com.ybg.block.core.dbapi.entity.FabricOrg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ybg.share.core.fabric.dto.FabircOrgQueryDTO;
import com.ybg.share.core.fabric.vo.FabricOrgVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

/**
 * <p>
 * 组织 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricOrgMapper extends BaseMapper<FabricOrg> {

    /**
     * 分页查询
     * @param dto
     * @return
     */
    @SelectProvider(type = FabricOrgSQL.class, method = "countPageQuery")
    Long countPageQuery(@Param("dto") FabircOrgQueryDTO dto);
    /**
     * 分页查询
     * @param dto
     * @return
     */
    @SelectProvider(type = FabricOrgSQL.class, method = "listPageQuery")
    List<FabricOrgVO> listPageQuery(@Param("dto") FabircOrgQueryDTO dto);
}
