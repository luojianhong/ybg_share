package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FabricOrderer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 排序服务 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricOrdererService extends IService<FabricOrderer> {

}
