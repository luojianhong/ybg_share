/*
 * Copyright (c) 2018. Aberic - aberic@qq.com - All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ybg.block.core.fabricbak.bean;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Api {

    /** API 意图 */
    public enum Intent {
        /**
         * 执行智能合约
         */
        INVOKE(1, "state/invoke"),
        /**
         * 查询智能合约
         */
        QUERY(2, "state/query"),
        /**
         * 查询智能合约
         */
        INFO(3, "trace/info"),
        /**
         * 根据交易hash查询区块
         */
        HASH(4, "trace/hash"),
        NUMBER(5, "trace/number"),
        /**
         * 根据当前智能合约id查询当前链信息
         */
        TXID(6, "trace/txid"),
        /**
         * 实例化只能合约
         */
        INSTANTIATE(7, "chaincode/instantiate"),
        /**
         * 升级只能合约
         */
        UPGRADE(8, "chaincode/upgrade");

        private int index;
        private String apiUrl;

        Intent(int index, String apiUrl) {
            this.index = index;
            this.apiUrl = apiUrl;
        }

        public static Intent get(int index) {
            for (Intent i : Intent.values()) {
                if (i.getIndex() == index) {
                    return i;
                }
            }
            return null;
        }

        public String getApiUrl() {
            return apiUrl;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }
    }

    /** 接口名称 */
    private String name = "";
    /** 接口意图 */
    private int index = 0;
    /** CA 标志*/
    private String flag = "";
    /** 请求app appKey */
    private String key = "";
    /** 请求Fabric版本号 */
    private String version = "";
    /** 接口执行参数 */
    private String exec = "";

    public Api() {
    }

    public Api(String name, int index) {
        this.name = name;
        this.index = index;
    }
}
