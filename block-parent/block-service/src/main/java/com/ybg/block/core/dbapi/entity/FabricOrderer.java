package com.ybg.block.core.dbapi.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 排序服务
 * </p>
 *
 * @author yanyu
 * @since 2019-11-29
 */
@ApiModel(value="排序服务") 
public class FabricOrderer extends Model<FabricOrderer> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
@ApiModelProperty(value = "主键")
    private Integer id;
    /**
     * 排序服务名称
     */
@ApiModelProperty(value = "排序服务名称")
    private String name;
    /**
     * 排序服务访问路径 根据自身设置实际情况修改，host:port的格式
     */
@ApiModelProperty(value = "排序服务访问路径 根据自身设置实际情况修改，host:port的格式")
    private String location;
    /**
     * Orderer TLS证书，上传Orderer中的TLS证书文件server.crt，如未开启TLS，则不用上传
     */
@ApiModelProperty(value = "Orderer TLS证书，上传Orderer中的TLS证书文件server.crt，如未开启TLS，则不用上传")
    private String serverCrtPath;
    /**
     * Orderer User Client Cert
     */
@ApiModelProperty(value = "Orderer User Client Cert")
    private String clientCertPath;
    /**
     * Orderer User Client Key
     */
@ApiModelProperty(value = "Orderer User Client Key")
    private String clientKeyPath;
    /**
     * 组织ID
     */
@ApiModelProperty(value = "组织ID")
    private Long orgId;
    /**
     * 创建日期
     */
@ApiModelProperty(value = "创建日期")
    private LocalDateTime createTime;
    /**
     * 联盟ID(冗余)
     */
@ApiModelProperty(value = "联盟ID(冗余)")
    private Long leagueId;
    /**
     * 联盟名称(冗余)
     */
@ApiModelProperty(value = "联盟名称(冗余)")
    private String leagueName;
    /**
     * 组织名称(冗余)
     */
@ApiModelProperty(value = "组织名称(冗余)")
    private String orgName;


    /**
     * 获取主键
     */
    public Integer getId() {
        return id;
    }
    /**
     * 设置主键
     */

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取排序服务名称
     */
    public String getName() {
        return name;
    }
    /**
     * 设置排序服务名称
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取排序服务访问路径 根据自身设置实际情况修改，host:port的格式
     */
    public String getLocation() {
        return location;
    }
    /**
     * 设置排序服务访问路径 根据自身设置实际情况修改，host:port的格式
     */

    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * 获取Orderer TLS证书，上传Orderer中的TLS证书文件server.crt，如未开启TLS，则不用上传
     */
    public String getServerCrtPath() {
        return serverCrtPath;
    }
    /**
     * 设置Orderer TLS证书，上传Orderer中的TLS证书文件server.crt，如未开启TLS，则不用上传
     */

    public void setServerCrtPath(String serverCrtPath) {
        this.serverCrtPath = serverCrtPath;
    }

    /**
     * 获取Orderer User Client Cert
     */
    public String getClientCertPath() {
        return clientCertPath;
    }
    /**
     * 设置Orderer User Client Cert
     */

    public void setClientCertPath(String clientCertPath) {
        this.clientCertPath = clientCertPath;
    }

    /**
     * 获取Orderer User Client Key
     */
    public String getClientKeyPath() {
        return clientKeyPath;
    }
    /**
     * 设置Orderer User Client Key
     */

    public void setClientKeyPath(String clientKeyPath) {
        this.clientKeyPath = clientKeyPath;
    }

    /**
     * 获取组织ID
     */
    public Long getOrgId() {
        return orgId;
    }
    /**
     * 设置组织ID
     */

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 获取创建日期
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }
    /**
     * 设置创建日期
     */

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取联盟ID(冗余)
     */
    public Long getLeagueId() {
        return leagueId;
    }
    /**
     * 设置联盟ID(冗余)
     */

    public void setLeagueId(Long leagueId) {
        this.leagueId = leagueId;
    }

    /**
     * 获取联盟名称(冗余)
     */
    public String getLeagueName() {
        return leagueName;
    }
    /**
     * 设置联盟名称(冗余)
     */

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    /**
     * 获取组织名称(冗余)
     */
    public String getOrgName() {
        return orgName;
    }
    /**
     * 设置组织名称(冗余)
     */

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 排序服务名称列的数据库字段名称
     */
    public static final String NAME = "name";

    /**
     * 排序服务访问路径 根据自身设置实际情况修改，host:port的格式列的数据库字段名称
     */
    public static final String LOCATION = "location";

    /**
     * Orderer TLS证书，上传Orderer中的TLS证书文件server.crt，如未开启TLS，则不用上传列的数据库字段名称
     */
    public static final String SERVER_CRT_PATH = "server_crt_path";

    /**
     * Orderer User Client Cert列的数据库字段名称
     */
    public static final String CLIENT_CERT_PATH = "client_cert_path";

    /**
     * Orderer User Client Key列的数据库字段名称
     */
    public static final String CLIENT_KEY_PATH = "client_key_path";

    /**
     * 组织ID列的数据库字段名称
     */
    public static final String ORG_ID = "org_id";

    /**
     * 创建日期列的数据库字段名称
     */
    public static final String CREATE_TIME = "create_time";

    /**
     * 联盟ID(冗余)列的数据库字段名称
     */
    public static final String LEAGUE_ID = "league_id";

    /**
     * 联盟名称(冗余)列的数据库字段名称
     */
    public static final String LEAGUE_NAME = "league_name";

    /**
     * 组织名称(冗余)列的数据库字段名称
     */
    public static final String ORG_NAME = "org_name";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FabricOrderer{" +
        "id=" + id +
        ", name=" + name +
        ", location=" + location +
        ", serverCrtPath=" + serverCrtPath +
        ", clientCertPath=" + clientCertPath +
        ", clientKeyPath=" + clientKeyPath +
        ", orgId=" + orgId +
        ", createTime=" + createTime +
        ", leagueId=" + leagueId +
        ", leagueName=" + leagueName +
        ", orgName=" + orgName +
        "}";
    }
}
