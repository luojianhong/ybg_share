package com.ybg.block.core.dbapi.service;

import com.ybg.block.core.dbapi.entity.FabricChaincode;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 链码表 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricChaincodeService extends IService<FabricChaincode> {

}
