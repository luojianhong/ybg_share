package com.ybg.block.core.dbapi.service.impl;

import com.ybg.block.core.dbapi.entity.BlockWalletAccount;
import com.ybg.block.core.dbapi.dao.BlockWalletAccountMapper;
import com.ybg.block.core.dbapi.service.BlockWalletAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 钱包账户 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-16
 */
//@Service(version = "1.0.0", interfaceClass = BlockWalletAccountService.class)
@Service
public class BlockWalletAccountServiceImpl extends ServiceImpl<BlockWalletAccountMapper, BlockWalletAccount> implements BlockWalletAccountService {

}
