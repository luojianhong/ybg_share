/*
 * Copyright (c) 2018. Aberic - All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ybg.block.core.dbapi.vo;

import lombok.Data;


import java.io.Serializable;

/**
 * 作者：Aberic on 2018/6/27 21:15
 * 邮箱：abericyang@gmail.com
 */
@Data
public class FnsOrdererVO implements Serializable {

    private int id; // required
    private String name; // required
    private String location; // required
    private String serverCrtPath;
    private String clientCertPath;
    private String clientKeyPath;
    private int orgId; // required
    private String date; // required

    private String leagueName;
    private String orgName; // required

}
