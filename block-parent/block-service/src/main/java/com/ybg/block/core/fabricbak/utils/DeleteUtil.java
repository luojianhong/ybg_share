/*
 * Copyright (c) 2018. Aberic - All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ybg.block.core.fabricbak.utils;



import com.ybg.block.core.dbapi.dao.FnsAppMapper;
import com.ybg.block.core.dbapi.dao.FnsBlockMapper;
import com.ybg.block.core.dbapi.dao.FnsCaMapper;
import com.ybg.block.core.dbapi.dao.FnsChaincodeMapper;
import com.ybg.block.core.dbapi.dao.FnsChannelMapper;
import com.ybg.block.core.dbapi.dao.FnsLeagueMapper;
import com.ybg.block.core.dbapi.dao.FnsOrdererMapper;
import com.ybg.block.core.dbapi.dao.FnsOrgMapper;
import com.ybg.block.core.dbapi.dao.FnsPeerMapper;
import com.ybg.block.core.dbapi.entity.FnsChaincode;
import com.ybg.block.core.dbapi.vo.FnsChannelVO;
import com.ybg.block.core.dbapi.vo.FnsOrgVO;
import com.ybg.block.core.dbapi.vo.FnsPeerVO;

import java.util.List;

/**
 * 作者：Aberic on 2018/7/7 18:18
 * 邮箱：abericyang@gmail.com
 */
public class DeleteUtil {

    private static DeleteUtil instance;

    public static DeleteUtil obtain() {
        if (null == instance) {
            synchronized (DeleteUtil.class) {
                if (null == instance) {
                    instance = new DeleteUtil();
                }
            }
        }
        return instance;
    }

    public int deleteLeague(int leagueId, FnsLeagueMapper leagueMapper, FnsOrgMapper orgMapper,
                            FnsOrdererMapper ordererMapper, FnsPeerMapper peerMapper, FnsCaMapper caMapper,
                            FnsChannelMapper channelMapper, FnsChaincodeMapper chaincodeMapper, FnsAppMapper appMapper, FnsBlockMapper blockMapper) {
        List<FnsOrgVO> orgs = orgMapper.list(leagueId);
        if (orgs.size() == 0) {
            CacheUtil.removeHome();
        } else {
            for (FnsOrgVO org : orgs) {
                deleteOrg(org.getId(), orgMapper, ordererMapper, peerMapper, caMapper, channelMapper, chaincodeMapper, appMapper, blockMapper);
            }
        }
        return leagueMapper.delete(leagueId);
    }

    public int deleteOrg(int orgId, FnsOrgMapper orgMapper, FnsOrdererMapper ordererMapper,
                         FnsPeerMapper peerMapper, FnsCaMapper caMapper, FnsChannelMapper channelMapper,
                         FnsChaincodeMapper chaincodeMapper, FnsAppMapper appMapper, FnsBlockMapper blockMapper) {
        List<FnsPeerVO> peers = peerMapper.list(orgId);
        if (peers.size() == 0) {
            CacheUtil.removeHome();
        } else {
            for (FnsPeerVO peer : peers) {
                deletePeer(peer.getId(), peerMapper, caMapper, channelMapper, chaincodeMapper, appMapper, blockMapper);
            }
        }
        ordererMapper.deleteAll(orgId);
        return orgMapper.delete(orgId);
    }

    public int deletePeer(int peerId, FnsPeerMapper peerMapper, FnsCaMapper caMapper, FnsChannelMapper channelMapper,
                          FnsChaincodeMapper chaincodeMapper, FnsAppMapper appMapper, FnsBlockMapper blockMapper) {
        List<FnsChannelVO> channels = channelMapper.list(peerId);
        if (channels.size() == 0) {
            CacheUtil.removeHome();
        } else {
            for (FnsChannelVO channel : channels) {
                deleteChannel(channel.getId(), channelMapper, chaincodeMapper, appMapper, blockMapper);
            }
        }
        caMapper.deleteAll(peerId);
        return peerMapper.delete(peerId);
    }

    public int deleteChannel(int channelId, FnsChannelMapper channelMapper, FnsChaincodeMapper chaincodeMapper, FnsAppMapper appMapper, FnsBlockMapper blockMapper) {
        blockMapper.deleteByChannelId(channelId);
        BlockUtil.obtain().removeChannel(channelId);
        List<FnsChaincode> chaincodes = chaincodeMapper.list(channelId);
        if (chaincodes.size() == 0) {
            FabricHelper.obtain().removeChannelManager(channelId);
            CacheUtil.removeHome();
        } else {
            for (FnsChaincode chaincode : chaincodes) {
                deleteChaincode(chaincode.getId(), chaincodeMapper, appMapper);
            }
        }
        return channelMapper.delete(channelId);
    }

    public int deleteChaincode(int chaincodeId, FnsChaincodeMapper chaincodeMapper, FnsAppMapper appMapper) {
        appMapper.deleteAll(chaincodeId);
        FabricHelper.obtain().removeChaincodeManager(chaincodeMapper.get(chaincodeId).getCc());
        CacheUtil.removeHome();
        return chaincodeMapper.delete(chaincodeId);
    }

}
