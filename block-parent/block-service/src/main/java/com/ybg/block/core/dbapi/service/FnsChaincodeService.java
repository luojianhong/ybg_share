package com.ybg.block.core.dbapi.service;

import com.alibaba.fastjson.JSONObject;
import com.ybg.block.core.dbapi.entity.FnsCa;
import com.ybg.block.core.dbapi.entity.FnsChaincode;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.block.core.dbapi.vo.FnsCaVO;
import com.ybg.block.core.dbapi.vo.FnsChaincodeVO;
import com.ybg.block.core.dbapi.vo.FnsChannelVO;
import com.ybg.block.core.fabricbak.bean.Api;
import com.ybg.block.core.fabricbak.bean.State;
import com.ybg.block.core.fabricbak.bean.Trace;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsChaincodeService extends IService<FnsChaincode> {
    int add(FnsChaincodeVO chaincode);

    JSONObject install(FnsChaincodeVO chaincode, MultipartFile file, Api api, boolean init);

    JSONObject instantiate(FnsChaincodeVO chaincode, List<String> strArray);

    JSONObject upgrade(FnsChaincodeVO chaincode, MultipartFile file, Api api);

    int update(FnsChaincodeVO chaincode);

    List<FnsChaincodeVO> listAll();

    List<FnsChaincode> listById(int id);

    FnsChaincodeVO get(int id);

    int countById(int id);

    int count();

    int delete(int id);

    int deleteAll(int channelId);

    FnsChaincodeVO getInstantiateChaincode(Api api, int chaincodeId);

    FnsChaincodeVO getEditChaincode(int chaincodeId);

    List<FnsChannelVO> getEditChannels(FnsChaincodeVO chaincode);

    FnsCa getCAByFlag(String flag);

    List<Api> getApis();

    List<FnsCa> getCAs(int chaincodeId);

    List<FnsCaVO> getAllCAs();

    List<FnsChaincodeVO> getChaincodes();

    List<FnsChannelVO> getChannelFullList();

    State getState(int id, Api api);

    String formatState(State state);

    Trace getTrace(Api api);

    String formatTrace(Trace trace);

    FnsChaincodeVO resetChaincode(FnsChaincodeVO chaincode);

    int FAIL = 9999;

    default String responseFail(String result) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", FAIL);
        jsonObject.put("error", result);
        return jsonObject.toString();
    }

    default JSONObject responseFailJson(String result) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", FAIL);
        jsonObject.put("error", result);
        return jsonObject;
    }
}
