package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FnsPeer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ybg.block.core.dbapi.vo.FnsPeerVO;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsPeerMapper extends BaseMapper<FnsPeer> {
    @Insert("insert into fns_peer (name,location,event_hub_location,server_crt_path,client_cert_path,client_key_path,org_id,date) values" +
            " (#{p.name},#{p.location},#{p.eventHubLocation},#{p.serverCrtPath},#{p.clientCertPath},#{p.clientKeyPath},#{p.orgId},#{p.date})")
    int add(@Param("p") FnsPeerVO peer);

    @Update("update fns_peer set name=#{p.name}, location=#{p.location}" +
            ", event_hub_location=#{p.eventHubLocation}" +
            ", server_crt_path=#{p.serverCrtPath}, " +
            "client_cert_path=#{p.clientCertPath}, " +
            "client_key_path=#{p.clientKeyPath} where id=#{p.id}")
    int update(@Param("p") FnsPeerVO peer);

    @Update("update fns_peer set name=#{p.name}, location=#{p.location}" +
            ", event_hub_location=#{p.eventHubLocation} where id=#{p.id}")
    int updateWithNoFile(@Param("p") FnsPeerVO peer);

    @Select("select count(name) from fns_peer where org_id=#{id}")
    int count(@Param("id") int id);

    @Select("select count(name) from fns_peer")
    int countAll();

    @Delete("delete from fns_peer where id=#{id}")
    int delete(@Param("id") int id);

    @Delete("delete from fns_peer where org_id=#{orgId}")
    int deleteAll(@Param("orgId") int orgId);

    @Select("select id,name,location,event_hub_location,server_crt_path,client_cert_path,client_key_path,org_id,date from fns_peer where id=#{id}")
//    @Results({
//            @Result(property = "eventHubLocation", column = "event_hub_location"),
//            @Result(property = "serverCrtPath", column = "server_crt_path"),
//            @Result(property = "clientCertPath", column = "client_cert_path"),
//            @Result(property = "clientKeyPath", column = "client_key_path"),
//            @Result(property = "orgId", column = "org_id")
//    })
    FnsPeerVO get(@Param("id") int id);

    @Select("select id,name,location,event_hub_location,server_crt_path,client_cert_path,client_key_path,org_id,date from fns_peer where org_id=#{id}")
//    @Results({
//            @Result(property = "eventHubLocation", column = "event_hub_location"),
//            @Result(property = "serverCrtPath", column = "server_crt_path"),
//            @Result(property = "clientCertPath", column = "client_cert_path"),
//            @Result(property = "clientKeyPath", column = "client_key_path"),
//            @Result(property = "orgId", column = "org_id")
//    })
    List<FnsPeerVO> list(@Param("id") int id);

    @Select("select id,name,location,event_hub_location,server_crt_path,client_cert_path,client_key_path,org_id,date from fns_peer")
//    @Results({
//            @Result(property = "eventHubLocation", column = "event_hub_location"),
//            @Result(property = "serverCrtPath", column = "server_crt_path"),
//            @Result(property = "clientCertPath", column = "client_cert_path"),
//            @Result(property = "clientKeyPath", column = "client_key_path"),
//            @Result(property = "orgId", column = "org_id")
//    })
    List<FnsPeerVO> listAll();
}
