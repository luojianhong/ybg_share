package com.ybg.block.core.dbapi.service.impl;

import com.ybg.block.core.dbapi.entity.FnsLeague;
import com.ybg.block.core.dbapi.entity.FnsOrderer;
import com.ybg.block.core.dbapi.entity.FnsOrg;
import com.ybg.block.core.dbapi.service.FnsOrdererService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.block.core.dbapi.vo.FnsOrdererVO;
import com.ybg.block.core.dbapi.vo.FnsOrgVO;
import com.ybg.block.core.fabricbak.utils.CacheUtil;
import com.ybg.block.core.fabricbak.utils.DateUtil;
import com.ybg.block.core.fabricbak.utils.FabricHelper;
import com.ybg.block.core.fabricbak.utils.FileUtil;
import com.ybg.block.core.dbapi.dao.FnsChaincodeMapper;
import com.ybg.block.core.dbapi.dao.FnsChannelMapper;
import com.ybg.block.core.dbapi.dao.FnsLeagueMapper;
import com.ybg.block.core.dbapi.dao.FnsOrdererMapper;
import com.ybg.block.core.dbapi.dao.FnsOrgMapper;
import com.ybg.block.core.dbapi.dao.FnsPeerMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
//@Service(version = "1.0.0", interfaceClass = FnsOrdererService.class)
@Service
public class FnsOrdererServiceImpl extends ServiceImpl<FnsOrdererMapper, FnsOrderer> implements FnsOrdererService {
    @Autowired
    private FnsLeagueMapper leagueMapper;
    @Autowired
    private FnsOrgMapper orgMapper;
    @Autowired
    private FnsOrdererMapper ordererMapper;
    @Autowired
    private FnsPeerMapper peerMapper;
    @Autowired
    private FnsChannelMapper channelMapper;
    @Autowired
    private FnsChaincodeMapper chaincodeMapper;
    @Autowired
    private Environment env;

    @Override
    public int add(FnsOrdererVO orderer, MultipartFile serverCrtFile, MultipartFile clientCertFile, MultipartFile clientKeyFile) {
        if (StringUtils.isEmpty(orderer.getName()) ||
                StringUtils.isEmpty(orderer.getLocation())) {
            throw new RuntimeException("orderer name and location can not be null");
        }
        if (StringUtils.isNotEmpty(serverCrtFile.getOriginalFilename()) &&
                StringUtils.isNotEmpty(clientCertFile.getOriginalFilename()) &&
                StringUtils.isNotEmpty(clientKeyFile.getOriginalFilename())) {
            if (saveFileFail(orderer, serverCrtFile, clientCertFile, clientKeyFile)) {
                throw new RuntimeException("tls file save fail");
            }
        }
        orderer.setDate(DateUtil.getCurrent("yyyy-MM-dd"));
        CacheUtil.removeHome();
        return ordererMapper.add(orderer);
    }

    @Override
    public int update(FnsOrdererVO orderer, MultipartFile serverCrtFile, MultipartFile clientCertFile, MultipartFile clientKeyFile) {
        if (null == serverCrtFile || null == clientCertFile || null == clientKeyFile) {
            FabricHelper.obtain().removeChaincodeManager(peerMapper.list(orderer.getOrgId()), channelMapper, chaincodeMapper);
            CacheUtil.removeHome();
            return ordererMapper.updateWithNoFile(orderer);
        }
        if (saveFileFail(orderer, serverCrtFile, clientCertFile, clientKeyFile)) {
            throw new RuntimeException("tls file save fail");
        }
        FabricHelper.obtain().removeChaincodeManager(peerMapper.list(orderer.getOrgId()), channelMapper, chaincodeMapper);
        CacheUtil.removeHome();
        return ordererMapper.update(orderer);
    }

    @Override
    public List<FnsOrdererVO> listAll() {
        List<FnsOrdererVO> orderers = ordererMapper.listAll();
        for (FnsOrdererVO orderer : orderers) {
            FnsOrg org = orgMapper.get(orderer.getOrgId());
            orderer.setLeagueName(leagueMapper.get(org.getLeagueId()).getName());
            orderer.setOrgName(org.getMspId());
        }
        return orderers;
    }

    @Override
    public List<FnsOrderer> listById(int id) {
        return ordererMapper.list(id);
    }

    @Override
    public FnsOrderer get(int id) {
        return ordererMapper.get(id);
    }

    @Override
    public int countById(int id) {
        return ordererMapper.count(id);
    }

    @Override
    public int count() {
        return ordererMapper.countAll();
    }

    @Override
    public int delete(int id) {
        CacheUtil.removeHome();
        return ordererMapper.delete(id);
    }

    @Override
    public List<FnsOrgVO> listOrgById(int orgId) {
        FnsLeague league = leagueMapper.get(orgMapper.get(orgId).getLeagueId());
        List<FnsOrgVO> orgs = orgMapper.list(league.getId());
        for (FnsOrgVO org : orgs) {
            org.setLeagueName(league.getName());
        }
        return orgs;
    }

    @Override
    public List<FnsOrgVO> listAllOrg() {
        List<FnsOrgVO> orgs = orgMapper.listAll();
        for (FnsOrgVO org : orgs) {
            org.setLeagueName(leagueMapper.get(org.getLeagueId()).getName());
        }
        return orgs;
    }

    @Override
    public FnsOrdererVO resetOrderer(FnsOrdererVO orderer) {
        FnsOrg org = orgMapper.get(orderer.getOrgId());
        FnsLeague league = leagueMapper.get(org.getLeagueId());
        orderer.setLeagueName(league.getName());
        orderer.setOrgName(org.getMspId());
        return orderer;
    }

    private boolean saveFileFail(FnsOrdererVO orderer, MultipartFile serverCrtFile, MultipartFile clientCertFile, MultipartFile clientKeyFile) {
        String ordererTlsPath = String.format("%s%s%s%s%s%s%s%s",
                env.getProperty("config.dir"),
                File.separator,
                orderer.getLeagueName(),
                File.separator,
                orderer.getOrgName(),
                File.separator,
                orderer.getName(),
                File.separator);
        String serverCrtPath = String.format("%s%s", ordererTlsPath, serverCrtFile.getOriginalFilename());
        String clientCertPath = String.format("%s%s", ordererTlsPath, clientCertFile.getOriginalFilename());
        String clientKeyPath = String.format("%s%s", ordererTlsPath, clientKeyFile.getOriginalFilename());
        orderer.setServerCrtPath(serverCrtPath);
        orderer.setClientCertPath(clientCertPath);
        orderer.setClientKeyPath(clientKeyPath);
        try {
            FileUtil.save(serverCrtFile, clientCertFile, clientKeyFile, serverCrtPath, clientCertPath, clientKeyPath);
        } catch (IOException e) {
            e.printStackTrace();
            return true;
        }
        return false;
    }
}
