package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FabricChannel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 通道 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricChannelMapper extends BaseMapper<FabricChannel> {

}
