package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FabricApp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 应用 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-22
 */
public interface FabricAppMapper extends BaseMapper<FabricApp> {

}
