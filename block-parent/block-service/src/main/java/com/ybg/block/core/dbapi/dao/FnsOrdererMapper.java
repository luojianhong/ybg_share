package com.ybg.block.core.dbapi.dao;

import com.ybg.block.core.dbapi.entity.FnsOrderer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ybg.block.core.dbapi.vo.FnsOrdererVO;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-11-21
 */
public interface FnsOrdererMapper extends BaseMapper<FnsOrderer> {
    @Insert("insert into fns_orderer (name,location,server_crt_path,client_cert_path,client_key_path,org_id,date) values " +
            "(#{o.name},#{o.location},#{o.serverCrtPath},#{o.clientCertPath},#{o.clientKeyPath},#{o.orgId},#{o.date})")
    int add(@Param("o") FnsOrdererVO orderer);

    @Update("update fns_orderer set name=#{o.name}, location=#{o.location}, server_crt_path=#{o.serverCrtPath}, " +
            "client_cert_path=#{o.clientCertPath}, client_key_path=#{o.clientKeyPath} where id=#{o.id}")
    int update(@Param("o") FnsOrdererVO orderer);

    @Update("update fns_orderer set name=#{o.name}, location=#{o.location} where id=#{o.id}")
    int updateWithNoFile(@Param("o") FnsOrdererVO orderer);

    @Select("select count(name) from fns_orderer where org_id=#{id}")
    int count(@Param("id") int id);

    @Select("select count(name) from fns_orderer")
    int countAll();

    @Delete("delete from fns_orderer where id=#{id}")
    int delete(@Param("id") int id);

    @Delete("delete from fns_orderer where org_id=#{orgId}")
    int deleteAll(@Param("orgId") int orgId);

    @Select("select id,name,location,server_crt_path,client_cert_path,client_key_path,org_id,date from fns_orderer where id=#{id}")
//    @Results({
//            @Result(property = "serverCrtPath", column = "server_crt_path"),
//            @Result(property = "clientCertPath", column = "client_cert_path"),
//            @Result(property = "clientKeyPath", column = "client_key_path"),
//            @Result(property = "orgId", column = "org_id")
//    })
    FnsOrderer get(@Param("id") int id);

    @Select("select id,name,location,server_crt_path,client_cert_path,client_key_path,org_id,date from fns_orderer where org_id=#{id}")
//    @Results({
//            @Result(property = "serverCrtPath", column = "server_crt_path"),
//            @Result(property = "clientCertPath", column = "client_cert_path"),
//            @Result(property = "clientKeyPath", column = "client_key_path"),
//            @Result(property = "orgId", column = "org_id")
//    })
    List<FnsOrderer> list(@Param("id") int id);

    @Select("select id,name,location,server_crt_path,client_cert_path,client_key_path,org_id,date from fns_orderer")
//    @Results({
//            @Result(property = "serverCrtPath", column = "server_crt_path"),
//            @Result(property = "clientCertPath", column = "client_cert_path"),
//            @Result(property = "clientKeyPath", column = "client_key_path"),
//            @Result(property = "orgId", column = "org_id")
//    })
    List<FnsOrdererVO> listAll();
}
