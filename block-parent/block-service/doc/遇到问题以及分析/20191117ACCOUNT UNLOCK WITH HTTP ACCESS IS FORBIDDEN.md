使用最新版本geth客户，当执行personal.unlockAccount()或在程序中调用personal_unlockAccount接口时，会出现：account unlock with HTTP access is forbidden异常。

#异常原因
新版本geth，出于安全考虑，默认禁止了HTTP通道解锁账户，相关issue：https://github.com/ethereum/go-ethereum/pull/17037
#解决方案
如果已经了解打开此功能的风险，可通启动命令中添加参数：
```$xslt
--allow-insecure-unlock
```
来进行打开操作。

示例：
```$xslt

geth --rpc --rpcapi eth,web3,personal --allow-insecure-unlock

```