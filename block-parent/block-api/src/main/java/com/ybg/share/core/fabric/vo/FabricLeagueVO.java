package com.ybg.share.core.fabric.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class FabricLeagueVO implements Serializable {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 联盟名称
     */
    @ApiModelProperty(value = "联盟名称")
    private String name;
    /**
     * 日期
     */
    @ApiModelProperty(value = "日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 组织数量（冗余）
     */
    @ApiModelProperty(value = "组织数量（冗余）")
    private Integer orgCount;
}
