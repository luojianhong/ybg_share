package com.ybg.share.core.fabric;

import com.ybg.share.core.fabric.dto.FabricChannelEditDTO;
import com.ybg.share.core.fabric.dto.FabricChannelQueryDTO;
import com.ybg.share.core.fabric.vo.FabricChannelVO;
import com.ybg.framework.vo.R;

import java.util.List;

public interface FabricChannelRemoteApi {
    /**
     * 编辑通道
     *
     * @param dto
     * @return
     */
    R<?> editFabricChannel(FabricChannelEditDTO dto);

    /**
     * 删除通道
     *
     * @param id
     * @return
     */
    R<?> deleteById(Long id);

    /**
     * 查询通道
     *
     * @param dto
     * @return
     */
    R<List<FabricChannelVO>> query(FabricChannelQueryDTO dto);

    /**
     * 单个信息
     *
     * @param id
     * @return
     */
    R<FabricChannelVO> getById(Long id);
}
