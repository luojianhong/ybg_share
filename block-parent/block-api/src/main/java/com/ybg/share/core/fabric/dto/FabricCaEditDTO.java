package com.ybg.share.core.fabric.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("用户凭证")
public class FabricCaEditDTO implements Serializable {
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * CA集合
     */
    @ApiModelProperty(value = "CA集合")
    private String name;
    /**
     * CA sk文件
     */
    @ApiModelProperty(value = "CA sk文件")
    private String sk;
    /**
     * CA 证书
     */
    @ApiModelProperty(value = "CA 证书")
    private String certificate;

    /**
     * 节点ID
     */
    @ApiModelProperty(value = "节点ID")
    private Long peerId;
}
