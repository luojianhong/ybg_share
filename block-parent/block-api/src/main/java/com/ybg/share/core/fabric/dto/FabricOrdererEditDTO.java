package com.ybg.share.core.fabric.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ApiModel("排序服务")
public class FabricOrdererEditDTO implements Serializable {
    @ApiModelProperty(value = "主键")
    private Integer id;
    /**
     * 排序服务名称
     */
    @ApiModelProperty(value = "排序服务名称")
    private String name;
    /**
     * 排序服务访问路径 根据自身设置实际情况修改，host:port的格式
     */
    @ApiModelProperty(value = "排序服务访问路径 根据自身设置实际情况修改，host:port的格式")
    private String location;
    /**
     * Orderer TLS证书，上传Orderer中的TLS证书文件server.crt，如未开启TLS，则不用上传
     */
    @ApiModelProperty(value = "Orderer TLS证书，上传Orderer中的TLS证书文件server.crt，如未开启TLS，则不用上传")
    private String serverCrtPath;
    /**
     * Orderer User Client Cert
     */
    @ApiModelProperty(value = "Orderer User Client Cert")
    private String clientCertPath;
    /**
     * Orderer User Client Key
     */
    @ApiModelProperty(value = "Orderer User Client Key")
    private String clientKeyPath;
    /**
     * 组织ID
     */
    @ApiModelProperty(value = "组织ID,不可修改")
    private Long orgId;

}
