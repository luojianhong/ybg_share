package com.ybg.share.core.eth.bean;

import lombok.Data;

import java.io.Serializable;
import java.math.BigInteger;
@Data
public class Eth implements Serializable {
    BigInteger difficulty;
    String genesis;
    String head;
    Integer network;
    BigInteger version;
}