package com.ybg.share.core.eth.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("以太坊钱包地址")
public class EthWallet implements Serializable {
    /**
     * 钱包地址
     */
    String address;
    /**
     * 公钥
     */
    String publicKey;
    /**
     * 私钥
     */
    String privateKey;
}
