package com.ybg.share.core.eth.bean;

import lombok.Data;

import java.io.Serializable;

@Data
public class Network implements Serializable {
    String localAddress;
    String remoteAddress;

}
