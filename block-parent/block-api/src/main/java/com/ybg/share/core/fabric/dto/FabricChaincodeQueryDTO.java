package com.ybg.share.core.fabric.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("链码")
public class FabricChaincodeQueryDTO implements Serializable {
    @ApiModelProperty(value = "主键")
    private Long id;

    /**
     * 通道ID
     */
    @ApiModelProperty(value = "通道ID")
    private Long channelId;

    /**
     * 联盟ID（冗余）
     */
    @ApiModelProperty(value = "联盟ID（冗余）")
    private Long leagueId;
    /**
     * 组织ID（冗余）
     */
    @ApiModelProperty(value = "组织ID（冗余）")
    private Long orgId;
    /**
     * 节点id(冗余)
     */
    @ApiModelProperty(value = "节点id(冗余)")
    private Long peerId;
}
