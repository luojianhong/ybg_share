package com.ybg.share.core.fabric;

import com.ybg.share.core.fabric.dto.FabricOrdererEditDTO;
import com.ybg.share.core.fabric.vo.FabricOrdererVO;
import com.ybg.framework.vo.R;

/**
 * 排序服务API
 */
public interface FabricOrdererRemoteApi {

    R<?> editOrderer(FabricOrdererEditDTO dto);

    R<?> deleteById(Long id);

    R<FabricOrdererVO> getById(Long id);

    //R<Page<FabricOrdererVO>> page(FabricOrderQueryDTO dto);

    //R<List<FabricOrdererVO>> list();
}
