package com.ybg.share.core.eth.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * 交易事务
 *
 * @author yanyu
 */
@Data
@ApiModel("交易事务")
public class EthTransactionDTO implements Serializable {
    /**
     * 当nonce小于之前已经有交易使用的nonce值，交易会被拒绝；
     * 当nonce大于当前应该使用的nonce时，交易会一直处于队列之中进行等待，交易依次执行，直到补齐中间间隔的nonce值，才可以执行。
     * 当有一笔处于pending状态的交易，新的一笔交易与其拥有相同的nonce值，如果新交易的gas price太小，无法覆盖pending状态的交易，如果新交易的gas price高于原交易的110%，则原交易会被覆盖掉。
     */
    @ApiModelProperty("交易订单号")
    private BigInteger nonce;

    /**
     * 发送账户地址
     */
    String from;
    /**
     * 承诺的gas价格，可选
     */
    BigInteger gasPrice;
    /**
     * 交易gas用量，可选
     */
    BigInteger gasLimit;
    /**
     * 目标账户地址，可选
     */
    String to;
    /**
     * 交易金额，可选
     */
    BigInteger value;
    /**
     * 交易附加数据或合约调用的ABI编码数据，可选
     */
    String data;


}
