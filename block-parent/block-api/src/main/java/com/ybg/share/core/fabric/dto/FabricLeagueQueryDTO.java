package com.ybg.share.core.fabric.dto;

import com.ybg.framework.dto.BasePageDTO;
import io.swagger.annotations.ApiModel;

/**
 *
 * @author yanyu
 * @since 2019年11月25日
 */
@ApiModel("联盟 分页查询")
public class FabricLeagueQueryDTO extends BasePageDTO {

}
