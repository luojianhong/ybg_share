package com.ybg.share.core.fabric.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel("通道接口")
@Data
public class FabricChannelEditDTO implements Serializable {
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 通道名称
     */
    @ApiModelProperty(value = "通道名称")
    private String name;
    /**
     * 同步区块监听
     */
    @ApiModelProperty(value = "同步区块监听")
    private Boolean blockListener;
    /**
     * 同步区块监听回调地址
     */
    @ApiModelProperty(value = "同步区块监听回调地址")
    private String callbackLocation;
    /**
     * 节点ID
     */
    @ApiModelProperty(value = "节点ID")
    private Long peerId;
}
