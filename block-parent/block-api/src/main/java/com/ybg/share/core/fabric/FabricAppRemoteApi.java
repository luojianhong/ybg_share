package com.ybg.share.core.fabric;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.share.core.fabric.dto.FabricAppEditDTO;
import com.ybg.share.core.fabric.dto.FabricAppQueryDTO;
import com.ybg.share.core.fabric.vo.FabricAppVO;
import com.ybg.framework.vo.R;

public interface FabricAppRemoteApi {

    /**
     * 编辑
     * @param dto
     * @return
     */
    R<?> edit(FabricAppEditDTO dto);

    /**
     * 删除
     * @param id
     * @return
     */
    R<?> remove(Long id);

    /**
     * 获取
     * @param id
     * @return
     */
    R<FabricAppVO> getById(Long id);

    /**
     * 分页查询
     * @param dto
     * @return
     */
    R<Page<FabricAppVO>> query(FabricAppQueryDTO dto);


}
