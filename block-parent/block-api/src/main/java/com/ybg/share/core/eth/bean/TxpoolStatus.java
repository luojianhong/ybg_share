package com.ybg.share.core.eth.bean;

import lombok.Data;

import java.io.Serializable;
import java.math.BigInteger;

@Data
public class TxpoolStatus implements Serializable {
    BigInteger pending;
    BigInteger queued;
}
