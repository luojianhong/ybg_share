package com.ybg.share.core.enums;

/**
 * 钱包类型
 */
public enum WalletType {
    /**
     * 以太坊
     */
    ETH("eth");
    String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    WalletType(String value) {
        this.value = value;
    }
}
