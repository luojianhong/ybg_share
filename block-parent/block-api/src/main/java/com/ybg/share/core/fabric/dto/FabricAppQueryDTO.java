package com.ybg.share.core.fabric.dto;

import com.ybg.framework.dto.BasePageDTO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author  yanyu
 */
@Data
@ApiModel("应用分页查询表单")
public class FabricAppQueryDTO extends BasePageDTO {
}
