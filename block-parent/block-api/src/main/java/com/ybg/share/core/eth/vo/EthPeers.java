package com.ybg.share.core.eth.vo;

import com.ybg.share.core.eth.bean.Network;
import com.ybg.share.core.eth.bean.Protocols;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class EthPeers implements Serializable {
    List<String> caps;
    String id;
    String name;
    Network network;
    Protocols protocols;

}



