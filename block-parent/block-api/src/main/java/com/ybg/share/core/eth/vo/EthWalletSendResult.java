package com.ybg.share.core.eth.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;



@Data
@ApiModel("发送结果")
public class EthWalletSendResult {
    private String transactionHash;
    private String transactionIndex;
    private String blockHash;
    private String blockNumber;
    private String cumulativeGasUsed;
    private String gasUsed;
    private String contractAddress;
    private String root;
    private String status;
    private String from;
    private String to;
   // private List<Log> logs;
    private String logsBloom;
}
