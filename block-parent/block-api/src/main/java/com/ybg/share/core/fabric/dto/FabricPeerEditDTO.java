package com.ybg.share.core.fabric.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * <p>
 * 节点
 * </p>
 *
 * @author yanyu
 * @since 2019-11-26
 */
@Data
@ApiModel(value = "节点")
public class FabricPeerEditDTO implements  Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 节点名称
     */
    @ApiModelProperty(value = "节点名称")
    private String name;
    /**
     * 节点访问地址
     */
    @ApiModelProperty(value = "节点访问地址")
    private String location;
    /**
     * 节点服务事件路径 根据自身设置实际情况修改，host:port的格式
     */
    @ApiModelProperty(value = "节点服务事件路径 根据自身设置实际情况修改，host:port的格式")
    private String eventHubLocation;
    /**
     * 组织ID
     */
    @ApiModelProperty(value = "组织ID")
    private Long orgId;
    /**
     * Peer TLS证书
     */
    @ApiModelProperty(value = "Peer TLS证书")
    private String serverCrtPath;
    /**
     * Peer User Client Cert
     */
    @ApiModelProperty(value = "Peer User Client Cert")
    private String clientCertPath;
    /**
     * Peer User Client Key
     */
    @ApiModelProperty(value = "Peer User Client Key")
    private String clientKeyPath;





}
