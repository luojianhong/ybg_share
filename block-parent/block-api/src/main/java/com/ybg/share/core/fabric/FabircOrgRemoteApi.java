package com.ybg.share.core.fabric;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.share.core.fabric.dto.FabircOrgEditDTO;
import com.ybg.share.core.fabric.dto.FabircOrgQueryDTO;
import com.ybg.share.core.fabric.vo.FabricOrgVO;
import com.ybg.framework.vo.R;

/**
 * Fabric组织API
 */
public interface FabircOrgRemoteApi {
    /**
     * 编辑 组织
     * @param dto
     * @return
     */
    R<?>  editOrg(FabircOrgEditDTO dto);


    R<FabricOrgVO> getById(Long id);

    R<?> deleteById(Long id);

    R<Page<FabricOrgVO>> page(FabircOrgQueryDTO dto);
}
