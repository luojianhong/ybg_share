package com.ybg.share.core.fabric;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.share.core.fabric.dto.FabricLeagueEditDTO;
import com.ybg.share.core.fabric.dto.FabricLeagueQueryDTO;
import com.ybg.share.core.fabric.vo.FabricLeagueVO;
import com.ybg.framework.vo.R;

import java.util.List;

/**
 * 联盟接口
 * @author  yanyu
 * @since  2019年11月25日
 */
public interface FabricLeagueRemoteApi {

    /**
     * 编辑联盟链
     *
     * @return
     */
    R<?> editLeague(FabricLeagueEditDTO dto);

    /**
     * 删除联盟
     *
     * @param id
     * @return
     */
    R<?> removeLeague(Long id);

    /***
     * 获取联盟信息
     * @param id
     * @return
     */
    R<FabricLeagueVO> getLeague(Long id);

    /**
     * 分页查询
     *
     * @return
     */
    R<Page<FabricLeagueVO>> page(FabricLeagueQueryDTO dto);
    /**
     * 所有联盟下拉框列表
     *
     */
    R<List<FabricLeagueVO>> allLeague();
}
