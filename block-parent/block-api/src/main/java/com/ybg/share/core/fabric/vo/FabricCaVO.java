package com.ybg.share.core.fabric.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ApiModel("用户凭证接口")
public class FabricCaVO implements Serializable {
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * CA集合
     */
    @ApiModelProperty(value = "CA集合")
    private String name;
    /**
     * CA sk文件
     */
    @ApiModelProperty(value = "CA sk文件")
    private String sk;
    /**
     * CA 证书
     */
    @ApiModelProperty(value = "CA 证书")
    private String certificate;
    @ApiModelProperty(value = "")
    private String flag;
    /**
     * 节点ID
     */
    @ApiModelProperty(value = "节点ID")
    private Long peerId;
    /**
     * 日期
     */
    @ApiModelProperty(value = "日期")
    private LocalDateTime createTime;
    /**
     * 联盟ID(冗余)
     */
    @ApiModelProperty(value = "联盟ID(冗余)")
    private Long leagueId;
    /**
     * 组织ID(冗余)
     */
    @ApiModelProperty(value = "组织ID(冗余)")
    private Long orgId;
    /**
     * 节点名称（冗余）
     */
    @ApiModelProperty(value = "节点名称（冗余）")
    private String peerName;
    /**
     * 组织名称（冗余）
     */
    @ApiModelProperty(value = "组织名称（冗余）")
    private String orgName;
    /**
     * 联盟名称（冗余）
     */
    @ApiModelProperty(value = "联盟名称（冗余）")
    private String leagueName;
}
