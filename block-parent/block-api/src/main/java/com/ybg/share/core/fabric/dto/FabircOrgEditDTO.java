package com.ybg.share.core.fabric.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Fabric 组织
 *
 * @author yanyu
 */
@Data
@ApiModel("Fabric 组织")
public class FabircOrgEditDTO implements Serializable {
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 节点是否开启TLS	根据自身创建网络情况选择true或false
     */
    @ApiModelProperty(value = "节点是否开启TLS	根据自身创建网络情况选择true或false")
    private Boolean tls;
    /**
     * 节点所属组织ID
     */
    @ApiModelProperty(value = "节点所属组织ID 参见configtx文件中 -> Organizations-&Org1-Name")
    private String mspId;
    /**
     * 联盟ID,此项无法修改操作
     */
    @ApiModelProperty(value = "联盟ID,此项无法修改操作")
    private Long leagueId;


}
