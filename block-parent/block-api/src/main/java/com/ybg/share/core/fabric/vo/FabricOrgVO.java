package com.ybg.share.core.fabric.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 组织
 */
@Data
@ApiModel("组织")
public class FabricOrgVO implements Serializable {
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 节点是否开启TLS	根据自身创建网络情况选择true或false
     */
    @ApiModelProperty(value = "节点是否开启TLS	根据自身创建网络情况选择true或false")
    private Boolean tls;
    /**
     * 节点所属组织ID
     */
    @ApiModelProperty(value = "节点所属组织ID")
    private String mspId;
    /**
     * 联盟ID
     */
    @ApiModelProperty(value = "联盟ID")
    private Long leagueId;
    /**
     * 创建日期
     */
    @ApiModelProperty(value = "创建日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 联盟名称（冗余）
     */
    @ApiModelProperty(value = "联盟名称（冗余）")
    private String leagueName;
    /**
     * 节点数量（冗余）
     */
    @ApiModelProperty(value = "节点数量（冗余）")
    private Integer peerCount;
    /**
     * 排序服务数量（冗余）
     */
    @ApiModelProperty(value = "排序服务数量（冗余）")
    private Integer ordererCount;
}
