package com.ybg.share.core.fabric.dto;

import com.ybg.framework.dto.BasePageDTO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("排序服务查询类")
public class FabricOrderQueryDTO extends BasePageDTO {

}
