package com.ybg.share.core.fabric.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
@Data
@ApiModel(value = "节点")
public class FabricPeerVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 节点名称
     */
    @ApiModelProperty(value = "节点名称")
    private String name;
    /**
     * 节点访问地址
     */
    @ApiModelProperty(value = "节点访问地址")
    private String location;
    /**
     * 节点服务事件路径 根据自身设置实际情况修改，host:port的格式
     */
    @ApiModelProperty(value = "节点服务事件路径 根据自身设置实际情况修改，host:port的格式")
    private String eventHubLocation;
    /**
     * 组织ID
     */
    @ApiModelProperty(value = "组织ID")
    private Long orgId;
    /**
     * Peer TLS证书
     */
    @ApiModelProperty(value = "Peer TLS证书")
    private String serverCrtPath;
    /**
     * Peer User Client Cert
     */
    @ApiModelProperty(value = "Peer User Client Cert")
    private String clientCertPath;
    /**
     * Peer User Client Key
     */
    @ApiModelProperty(value = "Peer User Client Key")
    private String clientKeyPath;
    /**
     * 创建日期
     */
    @ApiModelProperty(value = "创建日期")
    private LocalDateTime createTime;
    /**
     * 联盟ID(冗余)
     */
    @ApiModelProperty(value = "联盟ID(冗余)")
    private Long leagueId;
    /**
     * 组织名称(冗余)
     */
    @ApiModelProperty(value = "组织名称(冗余)")
    private String orgName;
    /**
     * 联盟名称(冗余)
     */
    @ApiModelProperty(value = "联盟名称(冗余)")
    private String leagueName;
    /**
     * 通道数量(冗余)
     */
    @ApiModelProperty(value = "通道数量(冗余)")
    private Integer channelCount;
}
