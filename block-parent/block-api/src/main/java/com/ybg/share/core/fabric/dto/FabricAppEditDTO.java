package com.ybg.share.core.fabric.dto;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * @author yanyu
 */
@ApiModel("应用表单")
public class FabricAppEditDTO implements Serializable {
}
