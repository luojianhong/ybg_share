package com.ybg.share.core.eth;

import com.ybg.framework.vo.R;
import com.ybg.share.core.eth.dto.EthTransactionDTO;

import java.util.List;

/**
 * 以太坊钱包服务
 *
 * @author yanyu
 * @since 2019-11-16
 */
public interface EthAccountRemoteApi {
    /**
     * 获取所有账户
     *
     * @return
     */
    R<List<String>> getAccounts();

    /**
     * 创建账户
     *
     * @param password 密码
     * @return 账户ID
     */
    R<String> addAccount(String password);

    /**
     * 解锁账户
     *
     * @param account    账户
     * @param passphrase 密码？密语？
     * @return
     */
    R<Boolean> unlockAccount(String account, String passphrase);

    /**
     * 发送普通交易
     * {@see http://cw.hubwiz.com/card/c/parity-rpc-api/1/1/4/}
     * @param password 密码
     * @param dto      交易参数
     * @return
     */
    R<String> send(String password, EthTransactionDTO dto);
}
