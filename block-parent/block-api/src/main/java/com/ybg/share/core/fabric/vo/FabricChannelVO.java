package com.ybg.share.core.fabric.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 通道
 */
@Data
@ApiModel("通道")
public class FabricChannelVO implements Serializable {
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 通道名称
     */
    @ApiModelProperty(value = "通道名称")
    private String name;
    /**
     * 同步区块监听
     */
    @ApiModelProperty(value = "同步区块监听")
    private Boolean blockListener;
    /**
     * 同步区块监听回调地址
     */
    @ApiModelProperty(value = "同步区块监听回调地址")
    private String callbackLocation;
    /**
     * 节点ID
     */
    @ApiModelProperty(value = "节点ID")
    private Long peerId;
    /**
     * 区块高度
     */
    @ApiModelProperty(value = "区块高度")
    private Integer height;
    /**
     * 日期
     */
    @ApiModelProperty(value = "日期")
    private LocalDateTime createTime;
    /**
     * 联盟ID(冗余)
     */
    @ApiModelProperty(value = "联盟ID(冗余)")
    private Long leagueId;
    /**
     * 组织ID(冗余)
     */
    @ApiModelProperty(value = "组织ID(冗余)")
    private Long orgId;
    /**
     * 节点名称冗余
     */
    @ApiModelProperty(value = "节点名称冗余")
    private String peerName;
    /**
     * 组织名称（冗余）
     */
    @ApiModelProperty(value = "组织名称（冗余）")
    private String orgAme;
    /**
     * 联盟名称（冗余）
     */
    @ApiModelProperty(value = "联盟名称（冗余）")
    private String leagueName;
    /**
     * 链码数量(冗余）
     */
    @ApiModelProperty(value = "链码数量(冗余）")
    private Integer chaincodeCount;
}
