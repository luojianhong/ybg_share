package com.ybg.share.core.fabric;

import com.ybg.share.core.fabric.dto.FabricChaincodeEditDTO;
import com.ybg.share.core.fabric.dto.FabricChaincodeQueryDTO;
import com.ybg.share.core.fabric.vo.FabricChaincodeVO;
import com.ybg.framework.vo.R;

import java.util.List;

public interface FabricChaincodeRemoteApi {

    R<?> editFabricChaincode(FabricChaincodeEditDTO dto);

    R<?> deleteById(Long id);

    R<FabricChaincodeVO> getById(Long id);

    R<List<FabricChaincodeVO>> query(FabricChaincodeQueryDTO dto);

//    /**
//     * 安装链码
//     *
//     * @return
//     */
//    R<?> install();
//
//    /**
//     * 升级链码
//     * @return
//     */
//    R<?> upgrade();
//
//
//
//    /**
//     * 实例化链码
//     * @return
//     */
//    R<?> instantiate();
//    /**
//     * 校验链码
//     * @return
//     */
//    R<?> verify();
}
