package com.ybg.share.core.fabric;

import com.ybg.share.core.fabric.dto.FabricCaEditDTO;
import com.ybg.share.core.fabric.dto.FabricCaQueryDTO;
import com.ybg.share.core.fabric.vo.FabricCaVO;
import com.ybg.framework.vo.R;

import java.util.List;

public interface FabricCaRemoteApi {

    R<?> editFabricCa(FabricCaEditDTO dto);

    R<?> deleteFabricCa(Long id);

    R<FabricCaVO> getById(Long id);

    R<List<FabricCaVO>> query(FabricCaQueryDTO dto);
}
