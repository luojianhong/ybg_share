package com.ybg.share.core.fabric.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yanyu
 * @since 2019年11月25日
 */
@Data
@ApiModel("联盟")
public class FabricLeagueEditDTO implements Serializable {
    @ApiModelProperty(value = "主键，新增的时候不用传", example = "1")
    private Long id;
    /**
     * 联盟名称
     */
    @ApiModelProperty(value = "联盟名称", example = "测试联盟")
    private String name;
}
