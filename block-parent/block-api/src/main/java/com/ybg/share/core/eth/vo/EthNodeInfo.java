package com.ybg.share.core.eth.vo;

import com.ybg.share.core.eth.bean.Ports;
import com.ybg.share.core.eth.bean.Protocols;
import lombok.Data;

import java.io.Serializable;

/**
 * 节点信息
 * @author yanyu
 */
@Data
public class EthNodeInfo implements  Serializable{
    String enode;
    String id;
    String ip;
    String listenAddr;
    String name;
    Ports ports;
    Protocols protocols;

}





