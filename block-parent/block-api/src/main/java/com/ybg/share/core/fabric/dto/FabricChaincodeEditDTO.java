package com.ybg.share.core.fabric.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
@Data
@ApiModel("链码")
public class FabricChaincodeEditDTO implements Serializable {
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 链码名称
     */
    @ApiModelProperty(value = "链码名称")
    private String name;
//    @ApiModelProperty(value = "")
//    private String source;
    /**
     * 智能合约路径 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02
     */
    @ApiModelProperty(value = "智能合约路径 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02")
    private String path;

    /**
     * 智能合约版本 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的1.0
     */
    @ApiModelProperty(value = "智能合约版本 如：peer chaincode install -n testcc -p github.com/hyperledger/fabric/aberic/chaincode/go/chaincode_example02 -v 1.0 命令中的1.0")
    private String version;
    /**
     * 提案请求超时时间以毫秒为单位 默认90000
     */
    @ApiModelProperty(value = "提案请求超时时间以毫秒为单位 默认90000")
    private Integer proposalWaitTime;
    /**
     * 通道ID
     */
    @ApiModelProperty(value = "通道ID")
    private Long channelId;

    /**
     * 链码事件监听
     */
    @ApiModelProperty(value = "链码事件监听")
    private Boolean  chaincodeEventListener;
    /**
     * 链码事件监听回调地址
     */
    @ApiModelProperty(value = "链码事件监听回调地址")
    private String callbackLocation;
    /**
     * 链码监听事件名集合
     */
    @ApiModelProperty(value = "链码监听事件名集合")
    private String events;

}
