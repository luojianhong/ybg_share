package com.ybg.share.core.fabric;

import com.ybg.share.core.fabric.dto.FabricPeerEditDTO;
import com.ybg.share.core.fabric.dto.FabricPeerQueryDTO;
import com.ybg.share.core.fabric.vo.FabricPeerVO;
import com.ybg.framework.vo.R;

import java.util.List;

/**
 * 节点API
 */
public interface FabricPeerRemoteApi {

    R<?> editPeer(FabricPeerEditDTO dto);

    R<?> deletePeer(Long id);

    R<FabricPeerVO> getPeer(Long id);

    /**
     * 根据查询条件查询
     *
     * @param dto
     * @return
     */
    R<List<FabricPeerVO>> query(FabricPeerQueryDTO dto);

}
