package com.ybg.share.core.fabric.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 节点查询
 */
@Data
@ApiModel(value = "节点")
public class FabricPeerQueryDTO implements Serializable {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long id;

    /**
     * 联盟ID(冗余)
     */
    @ApiModelProperty(value = "联盟ID(冗余)")
    private Long leagueId;

    /**
     * 组织ID
     */
    @ApiModelProperty(value = "组织ID")
    private Long orgId;

}
