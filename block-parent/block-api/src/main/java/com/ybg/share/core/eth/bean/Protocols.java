package com.ybg.share.core.eth.bean;

import lombok.Data;

import java.io.Serializable;

@Data
public class Protocols implements Serializable {
    Eth eth;
}