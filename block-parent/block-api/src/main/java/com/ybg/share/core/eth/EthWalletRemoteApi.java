package com.ybg.share.core.eth;

import com.ybg.share.core.eth.vo.EthWallet;
import com.ybg.share.core.eth.vo.EthWalletSendResult;
import com.ybg.framework.vo.R;

import java.math.BigDecimal;

/**
 * 钱包服务
 *
 * @author yanyu
 * @since 2019-11-16
 */
public interface EthWalletRemoteApi {
    /**
     * 创建钱包
     *
     * @param password 密码
     * @return 钱包地址
     */
    public R<String> addWallet(String password);


    /**
     * 加载钱包
     *
     * @param password 密码
     * @param address  钱包地址
     * @return
     */
    public R<EthWallet> loadWallet(String password, String address);

    /**
     * 发送金额
     *
     * @param sendAddress    钱包地址
     * @param password       钱包地址的密码
     * @param receiveAddress 接受的地址
     * @param value          发送的数量
     * @return
     */
    public R<EthWalletSendResult> tradeTo(String sendAddress, String password, String receiveAddress, BigDecimal value);

    /**
     * 获取钱包地址的余额
     *
     * @param address
     * @return
     */
    public R<String> getAddressBalance(String address);
}
