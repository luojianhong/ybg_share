package com.ybg.share.core.eth.bean;

import lombok.Data;

import java.io.Serializable;

public @Data
class Ports implements Serializable {
    Integer discovery;
    Integer listener;

}
