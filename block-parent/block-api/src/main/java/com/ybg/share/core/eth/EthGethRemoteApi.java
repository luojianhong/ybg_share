package com.ybg.share.core.eth;

import com.ybg.share.core.eth.vo.EthNodeInfo;
import com.ybg.framework.vo.R;

import java.math.BigInteger;

/**
 * Geth 操作接口
 *
 * @author yanyu
 * @since 2019-11-16
 */
public interface EthGethRemoteApi {
    /**
     * 开始挖矿
     *
     * @param threadNum 线程数
     * @return
     */
    R<Boolean> minerStart(int threadNum);

    /**
     * 停止挖矿
     *
     * @return
     */
    R<Boolean> minerStop();

    /**
     * 方法将指定的未加密私钥（16进制字符串表示）导入到密钥库中，并使用密码加密。
     *
     * @param keydata  私钥
     * @param password 密码
     * @return
     */
    R<String> importRawKey(String keydata, String password);

    /**
     * 锁定账户
     *
     * @param accountId
     * @return
     */
    R<Boolean> lockAccount(String accountId);

    /**
     * 方法从签名中提取签名私钥对应的以太坊地址
     * {> personal.sign("0xdeadbeaf", "0x9b2055d370f73ec7d8a03e965129118dc8f5bf83", "")
     * "0xa3f20717a250c2b0b729b7e5becbff67fdaef7e0699da4de7ca5895b02a170a12d887fd3b17bfdce3481f10bea41f45ba9f709d39ce8325427b57afcfc994cee1b"
     * > personal.ecRecover("0xdeadbeaf", "0xa3f20717a250c2b0b729b7e5becbff67fdaef7e0699da4de7ca5895b02a170a12d887fd3b17bfdce3481f10bea41f45ba9f709d39ce8325427b57afcfc994cee1b")
     * "0x9b2055d370f73ec7d8a03e965129118dc8f5bf83"}
     *
     * @param message
     * @param signiture
     * @return 提取签名私钥对应的以太坊地址
     */
    R<String> ecRecover(String message, String signiture);

    /**
     * 方法计算指定消息的以太坊特定的签名，计算方法如下
     * <p>
     * 通过添加消息前缀，从而让结果可以被识别以太坊特定的签名格式。这避免了一个恶意的 DApp对任意数据（例如交易）进行签名并使用签名冒充受攻击者。
     * > personal.sign("0xdeadbeaf", "0x9b2055d370f73ec7d8a03e965129118dc8f5bf83", "")
     * "0xa3f20717a250c2b0b729b7e5becbff67fdaef7e0699da4de7ca5895b02a170a12d887fd3b17bfdce3481f10bea41f45ba9f709d39ce8325427b57afcfc994cee1b"
     * </p>
     *
     * @param message
     * @param accountId
     * @param password
     * @return
     */
    R<String> sign(String message, String accountId, String password);

    /**
     * 查询挖矿账户
     *
     * @return 挖矿账户地址
     */
    R<String> getCoinbase();

    /**
     * 查询是否正在挖矿
     *
     * @return 是 或者否
     */
    R<Boolean> mining();

    /**
     * 指定挖矿账户
     *
     * @param accountId
     * @return
     */
    R<Boolean> setEtherbase(String accountId);

    /**
     * 查询区块高度
     *
     * @return
     */
    R<BigInteger> getBlockNumber();

    /**
     * 获取 挖矿地址
     *
     * @return
     */
    R<String> getGethUrl();

    /**
     * 获取存储的目录
     */
    R<String > adminDatadir();

    /**
     * 前geth节点已连接的远程节点的相关信息
     * @return
     */
    R<String> adminPeers();

    /**
     * 属性可用来查询当前运行的geth节点旳网络相关信息，包括ÐΞVp2p协议 信息以及运行中的应用协议信息，例如eth、les、shh、bzz等。
     * @return
     */
    R<EthNodeInfo> adminNodeInfo();
}
