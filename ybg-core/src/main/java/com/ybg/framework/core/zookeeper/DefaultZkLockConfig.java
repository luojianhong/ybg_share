package com.ybg.framework.core.zookeeper;

public class DefaultZkLockConfig implements IZkLockConfig {
    private String zkServers;
    private Integer zkConnecctTimeout;
    private String basePath;
    private Long retryMaxTime;

    @Override
    public String getZkServers() {
        return zkServers;
    }

    @Override
    public Integer getZkConnectTimeout() {
        return zkConnecctTimeout;
    }

    @Override
    public String getBasePath() {
        return basePath;
    }

    public Integer getZkConnecctTimeout() {
        return zkConnecctTimeout;
    }

    public void setZkConnecctTimeout(Integer zkConnecctTimeout) {
        this.zkConnecctTimeout = zkConnecctTimeout;
    }

    public void setZkServers(String zkServers) {
        this.zkServers = zkServers;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    @Override
    public Long getRetryMaxTime() {
        return retryMaxTime;
    }

    public void setRetryMaxTime(Long retryMaxTime) {
        this.retryMaxTime = retryMaxTime;
    }

}
