package com.ybg.framework.core.zookeeper;

public interface IZkLockConfig {
    /**
     *
     * 获取zookeeper服务器地址
     * @return String
     */
    String getZkServers();
    /**
     *
     * 获取zookeeper服务器连接超时时间
     * @return Integer
     */
    Integer getZkConnectTimeout();
    /**
     * 获取锁存储路径
     * @return String
     */
    String getBasePath();
    /**
     *
     * 获取主动重试最大间隔
     * @return Long
     */
    Long getRetryMaxTime();
}
