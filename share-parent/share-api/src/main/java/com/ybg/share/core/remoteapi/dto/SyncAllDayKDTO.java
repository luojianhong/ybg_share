package com.ybg.share.core.remoteapi.dto;

import java.io.Serializable;

/**
 * 同步日KDTO
 */
public class SyncAllDayKDTO implements Serializable {
    Long stockId;
    String market;

    Long minStockId;
    Long maxStockId;

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }


    public Long getMinStockId() {
        return minStockId;
    }

    public void setMinStockId(Long minStockId) {
        this.minStockId = minStockId;
    }

    public Long getMaxStockId() {
        return maxStockId;
    }

    public void setMaxStockId(Long maxStockId) {
        this.maxStockId = maxStockId;
    }
}
