package com.ybg.share.core.remoteapi.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * <p>
 * 股票信息
 * </p>
 *
 * @author yanyu
 * @since 2019-10-30
 */
@Data
public class ShareStockVO implements Serializable {

    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 股票名称
     */
    @ApiModelProperty(value = "股票名称")
    private String name;
    /**
     * 1 可用，0禁用
     */
    @ApiModelProperty(value = "1 可用，0禁用")
    private String code;
    /**
     * 所属市场
     */
    @ApiModelProperty(value = "所属市场")
    private String market;
    /**
     * 1 可用 0不可用
     */
    @ApiModelProperty(value = "1 可用 0不可用")
    private Boolean status;
    /**
     * 股票拼音
     */
    @ApiModelProperty(value = "股票拼音")
    private String spell;
}
