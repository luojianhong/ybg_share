package com.ybg.share.core.remoteapi;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.share.core.remoteapi.vo.StockIndexDayKVO;
import com.ybg.framework.vo.R;
import com.ybg.share.core.remoteapi.dto.StockIndexDTO;

import java.util.List;

/**
 * 大盘日K接口
 */
public interface StockIndexRemoteApi {
    /**
     * 获取大盘数据列表
     * @param market
     * @param startDate
     * @param endDate
     * @return
     */
    public List<StockIndexDayKVO> getStockIndexDayK(String market, String startDate, String endDate);

    /**
     * 同步大盘数据

     */
    public R syncStockIndex();

    /**
     * 获取大盘数据
     * @param market
     * @param date
     * @return
     */
    public StockIndexDayKVO getStockIndexDayK(String market, String date);


    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    public Page<StockIndexDayKVO> stockIndexDayKpage(StockIndexDTO dto);
}
