package com.ybg.share.core.remoteapi.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel("机器线程信息")
@Data
public class SemaphoreVO implements Serializable {
    /**
     * 返回当前有多少个有用的许可证数量
     */
    @ApiModelProperty("返回当前有多少个有用的许可证数量")
    int availablePermits;
    /**
     * 获得并返回所有立即可用的许可证数量
     */
    @ApiModelProperty("获得并返回所有立即可用的许可证数量")
    int drainPermits;
    /**
     * 返回当前可能在阻塞获取许可证线程的数量
     */
    @ApiModelProperty("返回当前可能在阻塞获取许可证线程的数量")
    int queueLength;
    /**
     * 查询是否有线程正在等待获取许可证
     */
    @ApiModelProperty("查询是否有线程正在等待获取许可证")
    boolean hasQueuedThreads;
    /**
     * 机器信息
     */
    @ApiModelProperty("机器信息")
    String ipAddr;
}
