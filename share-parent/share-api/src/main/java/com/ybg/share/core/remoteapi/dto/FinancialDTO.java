package com.ybg.share.core.remoteapi.dto;

import com.ybg.framework.dto.BasePageDTO;

public class FinancialDTO extends BasePageDTO {

    private Long stockId;
    private String code;
    private String reportDate;
    private String market;

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }
}
