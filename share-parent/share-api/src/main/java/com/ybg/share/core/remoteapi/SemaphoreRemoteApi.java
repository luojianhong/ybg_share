package com.ybg.share.core.remoteapi;

import com.ybg.share.core.remoteapi.vo.SemaphoreVO;

/**
 * 获取线程里面的信息
 */
public interface SemaphoreRemoteApi {
    /**
     * 返回当前有多少个有用的许可证数量
     **/
    public SemaphoreVO getThreadSemaphore();


}
