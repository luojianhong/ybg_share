package com.ybg.share.core.remoteapi;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.share.core.remoteapi.dto.StockDayKDTO;
import com.ybg.share.core.remoteapi.dto.StockPageQueryDTO;
import com.ybg.share.core.remoteapi.dto.SyncAllDayKDTO;
import com.ybg.share.core.remoteapi.vo.ShareStockVO;
import com.ybg.framework.vo.R;
import com.ybg.share.core.remoteapi.vo.StockDayKVO;

import java.util.List;

/**
 * 股票接口
 */
public interface StockRemoteApi {
    /**
     * 搜索框查询股票
     *
     * @param input
     * @return
     */
    public List<ShareStockVO> queryShare(String input);

    /**
     * 根据股票代码查询
     *
     * @param code
     * @return
     */
    public ShareStockVO getByCode(String code);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    public ShareStockVO getById(Long id);

    /**
     * 根据拼音简写查询
     *
     * @param pinyin
     * @return
     */
    public ShareStockVO getByPinyin(String pinyin);

    /**
     * 同步所有股票
     */
    public R<?> syncAllStock();

    /**
     * 更新全部股票信息
     *
     *
     */
    public R<?> updateAllStockInfo();



    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    public Page<ShareStockVO> page(StockPageQueryDTO dto);

    /**
     * 获取股票日K
     *
     * @param stockId
     * @param startDate
     * @param endDate
     * @return
     */
    public List<StockDayKVO> getDayK(Long stockId, String startDate, String endDate);

    /**
     * 同步股票日K
     *
     * @param stockId
     */
    public R<?> syncOneDayK(Long stockId);

    /**
     * 同步所有股票日K
     *
     * @param dto
     */
    public R<?> syncAllDayK(SyncAllDayKDTO dto);

    public R<?> banStock(Long stockId);

    public Page<StockDayKVO> pageDayK(StockDayKDTO dto);
}
