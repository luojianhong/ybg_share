package com.ybg.share.core.remoteapi;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.share.core.remoteapi.vo.FinancialVO;
import com.ybg.framework.vo.R;
import com.ybg.share.core.remoteapi.dto.FinancialDTO;

import java.util.List;

/**
 * 财报接口
 */
public interface FinancialRemoteApi {


    /**
     * 获取年度数据
     * @param stockId
     * @param year
     * @return
     */
    public FinancialVO getYearFinancialInfo(Long stockId, String year);

    /**
     * 获取年度的所有季度数据
     * @param stockId
     * @param year
     * @return
     */
    public List<FinancialVO> getAllMonthInfo(Long stockId,String year);

    /**
     *获取所有年度数据
     * @param stockId
     * @return
     */
    public List<FinancialVO> getAllYearInfo(Long stockId);

    /**
     * 拉取季度数据到数据库
     * @param stockId
     */
    public R syncMonthFinancialInfo(Long stockId);

    /**
     *拉取年度数据到数据库
     * @param stockId
     */
    public R syncYearFinancialInfo(Long stockId);

    /**
     * 同步所有财报（季报和年报）
     */
    public R syncAllFinancial();

    public Page<FinancialVO> getFinancialOfQuarterByPage(FinancialDTO dto);

    public Page<FinancialVO> getFinancialOfYearByPage(FinancialDTO dto);
}
