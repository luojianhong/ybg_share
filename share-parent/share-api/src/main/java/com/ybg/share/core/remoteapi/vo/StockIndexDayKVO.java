package com.ybg.share.core.remoteapi.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 大盘日K
 */
@Data
public class StockIndexDayKVO implements Serializable {
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 日期，2019-10-16
     */
    @ApiModelProperty(value = "日期，2019-10-16")
    private String refDate;
    /**
     * 股票代码
     */
    @ApiModelProperty(value = "股票代码")
    private String code;
    /**
     * 所属市场
     */
    @ApiModelProperty(value = "所属市场")
    private String market;
    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String stockName;
    /**
     * 收盘价
     */
    @ApiModelProperty(value = "收盘价")
    private BigDecimal closePrice;
    /**
     * 最高价
     */
    @ApiModelProperty(value = "最高价")
    private BigDecimal maxPrice;
    /**
     * 最低价
     */
    @ApiModelProperty(value = "最低价")
    private BigDecimal minPrice;
    /**
     * 开盘价
     */
    @ApiModelProperty(value = "开盘价")
    private BigDecimal openPrice;
    /**
     * 前收盘
     */
    @ApiModelProperty(value = "前收盘")
    private BigDecimal beforeClose;
    /**
     * 涨跌额
     */
    @ApiModelProperty(value = "涨跌额")
    private BigDecimal changeAmount;
    /**
     * 涨跌幅
     */
    @ApiModelProperty(value = "涨跌幅")
    private BigDecimal changeRange;
    /**
     * 成交量
     */
    @ApiModelProperty(value = "成交量")
    private Long tradeNum;
    /**
     * 成交金额
     */
    @ApiModelProperty(value = "成交金额")
    private BigDecimal tradeMoney;
}
