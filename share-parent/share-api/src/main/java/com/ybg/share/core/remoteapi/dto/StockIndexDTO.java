package com.ybg.share.core.remoteapi.dto;

import com.ybg.framework.dto.BasePageDTO;

public class StockIndexDTO extends BasePageDTO {
    private String stockName;
    private String refDate;
    private String code;
    private String market;

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getRefDate() {
        return refDate;
    }

    public void setRefDate(String refDate) {
        this.refDate = refDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

}
