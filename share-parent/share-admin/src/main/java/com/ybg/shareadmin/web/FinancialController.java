package com.ybg.shareadmin.web;

import com.ybg.framework.vo.R;
import com.ybg.share.core.remoteapi.FinancialRemoteApi;
import com.ybg.share.core.remoteapi.dto.FinancialDTO;
import com.ybg.share.core.remoteapi.vo.FinancialVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@Api(description = "财报接口")
@RestController
public class FinancialController {
    @Reference(version = "${dubbo.provider.version}", check = false)
    FinancialRemoteApi financialRemoteApi;


    @ApiOperation("分页获取股票的（季度）财报数据接口")
    @PostMapping("/share/financial/quarter/page")
    public R<FinancialVO> getFinancialOfQuarterByPage(FinancialDTO dto) {
        return R.success(financialRemoteApi.getFinancialOfYearByPage(dto));
    }

    @ApiOperation("分页获取股票的（年度）财报数据接口")
    @PostMapping("/share/financial/year/page")
    public R<FinancialVO> getFinancialOfYearByPage(FinancialDTO dto) {
        return R.success(financialRemoteApi.getFinancialOfYearByPage(dto));
    }

    @ApiOperation("获取股票的（季度）财报接口")
    @PostMapping("/share/financial/getAllMonthInfo")
    public R<FinancialVO> getAllMonthInfo(Long stockId, String year) {
        return R.success(financialRemoteApi.getAllMonthInfo(stockId, year));
    }

    @ApiOperation("获取股票的（年度）财报接口")
    @PostMapping("/share/financial/getAllYearInfo")
    public R<List<FinancialVO>> getAllYearInfo(Long stockId) {
        return R.success(financialRemoteApi.getAllYearInfo(stockId));
    }

    @ApiOperation("获取年度财报信息")
    @PostMapping("/share/financial/getYearFinancialInfo")
    public R<FinancialVO> getYearFinancialInfo(Long stockId, String year) {
        return R.success(financialRemoteApi.getYearFinancialInfo(stockId, year));
    }

    @ApiOperation("同步年度和季度的财报信息")
    @PostMapping("/share/financial/syncAllFinancial")
    public R<?> syncAllFinancial(Long stockId, String year) {
        return financialRemoteApi.syncAllFinancial();
    }

    @ApiOperation("同步单个股票季度财报信息")
    @PostMapping("/share/financial/syncMonthFinancialInfo")
    public R<?> syncMonthFinancialInfo(Long stockId) {
        return financialRemoteApi.syncMonthFinancialInfo(stockId);
    }

    @ApiOperation("同步单个股票年度财报信息")
    @PostMapping("/share/financial/syncYearFinancialInfo")
    public R<?> syncYearFinancialInfo(Long stockId) {
        return financialRemoteApi.syncYearFinancialInfo(stockId);
    }
}
