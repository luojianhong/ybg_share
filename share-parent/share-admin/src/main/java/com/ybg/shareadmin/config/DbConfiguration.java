package com.ybg.shareadmin.config;

import com.alibaba.druid.pool.DruidDataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.*;


/**
 * 数据库分表配置
 */
@Configuration
public class DbConfiguration {

    /**
     * 单库分表
     *
     * @return
     */
    @Bean
    public DataSource dataSource(@Value("${sharding.spring.datasource.url}") String url,
                                 @Value("${sharding.spring.datasource.username}") String username,
                                 @Value("${sharding.spring.datasource.password}") String password, @Value("${sql.show}") String showSQL) {
        DruidDataSource db = new DruidDataSource();
        db.setUrl(url);
        db.setUsername(username);
        db.setPassword(password);
        db.setDriverClassName("com.mysql.cj.jdbc.Driver");
        db.setMaxActive(200);
        db.setMinIdle(8);
        ArrayList<String> connectionInitSqls = new ArrayList<String>();
        connectionInitSqls.add("set names utf8mb4;");

        db.setConnectionInitSqls(connectionInitSqls);
        //  如果不想支持分表，则直接return db；
         return db;


    }

}
