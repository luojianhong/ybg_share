package com.ybg.shareadmin.web;

import com.ybg.framework.vo.R;
import com.ybg.share.core.remoteapi.StockIndexRemoteApi;
import com.ybg.share.core.remoteapi.dto.StockIndexDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "大盘接口")
@RestController
@Slf4j
public class ShareStockIndexController {
    @Reference(version = "${dubbo.provider.version}",check = false)
    private StockIndexRemoteApi stockIndexRemoteApi;
//
    @ApiOperation("分页查询大盘日k信息")
    @PostMapping("/share/stockIndex/page")
    public R page(@RequestBody StockIndexDTO dto) {
        return R.success(stockIndexRemoteApi.stockIndexDayKpage(dto));
    }

    @ApiOperation("获取大盘日K")
    @PostMapping("/share/stockIndex/getStockIndexDayK")
    public R getStockIndexDayK(String market, String startDate, String endDate) {
        return R.success(stockIndexRemoteApi.getStockIndexDayK(market, startDate, endDate));
    }

    @ApiOperation("获取具体的大盘日K")
    @PostMapping("/share/stockIndex/getStockIndexDayKDetail")
    public R getStockIndexDayK(String market, String date) {
        return R.success(stockIndexRemoteApi.getStockIndexDayK(market, date));
    }

    @ApiOperation("同步大盘日K")
    @PostMapping("/share/stockIndex/syncStockIndex")
    public R<?> syncStockIndex() {
       return  stockIndexRemoteApi.syncStockIndex();
    }


}
