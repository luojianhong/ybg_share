package com.ybg.shareadmin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

/**
 * @author janti
 * reids 相关bean的配置
 */
@Configuration
@EnableCaching
public class RedisConfig extends CachingConfigurerSupport {



    //////////jredis/////////
    @Bean

    public JedisPool jedisPool(JedisPoolConfig config, @Value("${spring.redis.host}") String host,
                               @Value("${spring.redis.port}") int port, @Value("${spring.redis.password}") String password,
                               @Value("${spring.redis.database}") int database) {
        return new JedisPool(config, host, port, Protocol.DEFAULT_TIMEOUT, password, database);
    }

    @Bean
    public JedisPoolConfig jedisPoolConfig(@Value("${spring.redis.lettuce.pool.max-active}") int maxTotal,
                                           @Value("${spring.redis.lettuce.pool.max-idle}") int maxIdle,
                                           @Value("${spring.redis.lettuce.pool.max-wait}") int maxWaitMillis) {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(maxTotal);
        config.setMaxIdle(maxIdle);
        config.setMaxWaitMillis(maxWaitMillis);
        return config;
    }
}