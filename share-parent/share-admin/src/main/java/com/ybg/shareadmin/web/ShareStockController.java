package com.ybg.shareadmin.web;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.framework.vo.R;
import com.ybg.share.core.remoteapi.StockRemoteApi;
import com.ybg.share.core.remoteapi.dto.StockDayKDTO;
import com.ybg.share.core.remoteapi.dto.StockPageQueryDTO;
import com.ybg.share.core.remoteapi.vo.ShareStockVO;
import com.ybg.share.core.remoteapi.vo.StockDayKVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(description = "股票接口")
@Slf4j
@RestController
public class ShareStockController {
    @Reference(version = "${dubbo.provider.version}", check = false)
    StockRemoteApi stockRemoteApi;

    @ApiOperation("获取单个股票日K")
    @PostMapping("/share/stock/syncOneDayK")
    public R<?> syncOneDayK(@ApiParam("股票id") @RequestParam(value = "stockId", required = true) Long stockId) {
        return stockRemoteApi.syncOneDayK(stockId);
    }

    @ApiOperation("同步所有股票列表")
    @PostMapping("/share/stock/syncAllStock")
    public R<?> syncAllStock() {
        return stockRemoteApi.syncAllStock();
    }

    @ApiOperation("分页查询股票列表信息")
    @PostMapping("/share/stock/page")
    public R<Page<ShareStockVO>> page(@RequestBody StockPageQueryDTO dto) {

        return R.success(stockRemoteApi.page(dto));
    }

    @ApiOperation("根据股票代码查询股票")
    @PostMapping("/share/stock/getByCode")
    public R<ShareStockVO> getByCode(@ApiParam("股票代码") @RequestParam(value = "code", required = true) String code) {
        return R.success(stockRemoteApi.getByCode(code));
    }

    @ApiOperation("根据ID查询股票")
    @PostMapping("/share/stock/getById")
    public R<ShareStockVO> getById(@ApiParam("股票ID") @RequestParam(value = "stockId", required = true) Long stockId) {
        return R.success(stockRemoteApi.getById(stockId));
    }

    @ApiOperation("输入股票代码，拼音简写，名称搜索股票")
    @PostMapping("/share/stock/queryShare")
    public R<ShareStockVO> queryShare(@ApiParam("输入内容") @RequestParam(value = "input", required = true) String input) {
        return R.success(stockRemoteApi.queryShare(input));
    }

    @ApiOperation("分页查询股票日K信息")
    @PostMapping("/share/stockDayK/page")
    public R<Page<ShareStockVO>> pageDayK(@RequestBody StockDayKDTO dto) {
        return R.success(stockRemoteApi.pageDayK(dto));
    }

    @ApiOperation("获取股票日K")
    @PostMapping("/share/stock/getDayK")
    public R<List<StockDayKVO>> getDayK(Long stockId, String startDate, String endDate) {
        return R.success(stockRemoteApi.getDayK(stockId, startDate, endDate));
    }

    @ApiOperation("更新单个股票信息")
    @PostMapping("/share/stock/updateAllStockInfo")
    public R<?> updateAllStockInfo() {
        return stockRemoteApi.updateAllStockInfo();
    }

    @ApiOperation("通过拼音搜索股票")
    @PostMapping("/share/stock/getByPinyin")
    public R<StockDayKVO> getByPinyin(@ApiParam("股票拼音") @RequestParam(value = "pinyin", required = true) String pinyin) {
        return R.success(stockRemoteApi.getByPinyin(pinyin));
    }

    @ApiOperation("启用或者禁用")
    @PostMapping("/share/stock/banStock")
    public R<?> banStock(@ApiParam("股票ID") @RequestParam(value = "stockId", required = true) Long stockId) {
        return stockRemoteApi.banStock(stockId);
    }
}
