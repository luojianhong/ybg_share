package com.ybg;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@Slf4j
@MapperScan("com.ybg.shareuser.dao")
@EnableSwagger2
public class ShareAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShareAdminApplication.class, args);
        log.info("股票超管端启动完毕");
    }
}
