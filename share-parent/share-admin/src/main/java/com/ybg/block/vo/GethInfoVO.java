package com.ybg.block.vo;

import com.ybg.share.core.eth.vo.EthNodeInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * 链信息
 *
 * @author yanyu
 * @since 2019-11-17
 */
@Data
@ApiModel("链的信息")
public class GethInfoVO implements Serializable {
    @ApiModelProperty("链的信息")
    String gethAddr;
    @ApiModelProperty("挖矿状态")
    Boolean mining;
    @ApiModelProperty("挖矿收益账户")
    String coinbase;
    @ApiModelProperty("区块高度")
    BigInteger blockNumber;
    //xxx 其他 待添加
    @ApiModelProperty("数据目录")
    String datadir;
    /**
     * 节点信息
     */
    @ApiModelProperty("节点信息")
    EthNodeInfo ethNodeInfo;
    /**
     * 前geth节点已连接的远程节点的相关信息
     */
//    @ApiModelProperty("前geth节点已连接的远程节点的相关信息")
//    JSONObject ethPeers;
}
