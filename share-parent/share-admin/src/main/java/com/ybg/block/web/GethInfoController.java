package com.ybg.block.web;

import com.ybg.block.vo.GethInfoVO;
import com.ybg.framework.vo.R;
import com.ybg.share.core.eth.EthGethRemoteApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "链节点信息")
@RestController
@Slf4j
public class GethInfoController {

    @Reference(version = "${dubbo.provider.version}", check = false)
    EthGethRemoteApi ethGethRemoteApi;

    @ApiOperation("节点信息")
    @PostMapping("/eth/gethInfo")
    public R<GethInfoVO> ethGethInfo() {
        log.info("节点信息");
        GethInfoVO vo = new GethInfoVO();
        vo.setGethAddr(ethGethRemoteApi.getGethUrl().getData());
        vo.setMining(ethGethRemoteApi.mining().getData());
        vo.setCoinbase(ethGethRemoteApi.getCoinbase().getData());
        vo.setBlockNumber(ethGethRemoteApi.getBlockNumber().getData());
        vo.setDatadir(ethGethRemoteApi.adminDatadir().getData());
        vo.setEthNodeInfo(ethGethRemoteApi.adminNodeInfo().getData());

        return R.success(vo);
    }

    @ApiOperation("开启挖矿,挖矿后 区块高度会增加 注意查看，是否真的挖矿成功，dev模式下，每秒增加1，前置条件，必须有账户，并且是已经解锁的 ")
    @PostMapping("/eth/minerStart")
    public R<Boolean> minerStart() {
        log.info("开启挖矿");
        return ethGethRemoteApi.minerStart(1);
    }

    @ApiOperation("停止挖矿，查看挖矿状态是否成功停止")
    @PostMapping("/eth/minerStop")
    public R<Boolean> minerStop() {
        log.info("停止挖矿");
        return ethGethRemoteApi.minerStop();
    }
}
