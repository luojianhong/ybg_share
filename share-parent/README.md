#股票模块
启动 share-service 即可 不需要启动 share-admin 模块
启动之前需要启动zookeeper，redis ，mysql
修改share-service 的配置文件
把 application.properties 的内容spring.profiles.active=pro 改成 spring.profiles.active=dev
并且修改好你的配置文件
application-dev.properties

### 必须要主义的是 zookeeper的配置，reids 的配置，mysql的配置,还有你的网易邮箱配置
```
#mysql 使用的版本是5.7+
sharding.spring.datasource.url = jdbc:mysql://127.0.0.1:3306/ybg_share?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&useSSL=true&allowMultiQueries=true&autoReconnect=true&failOverReadOnly=false
sharding.spring.datasource.username = root
sharding.spring.datasource.password = 123456

#redis的配置，不推荐使用windows的redis 会产生假死的问题
spring.redis.database=3

spring.redis.host=127.0.0.1
spring.redis.port=6379
spring.redis.password=


dubbo.registry.address=zookeeper://127.0.0.1:${embedded.zookeeper.port}
#网易邮箱配置 如果不填，发送邮箱的定时任务将无法进行
email.netaset.account=
email.netaset.password=

```
