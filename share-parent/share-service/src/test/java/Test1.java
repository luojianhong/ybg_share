import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.ShareApplication;
import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.ybg.share.core.dbapi.service.ShareStockDayKService;
import com.ybg.share.core.dbapi.service.ShareStockIndexService;
import com.ybg.share.core.dbapi.service.ShareStockService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest(classes = ShareApplication.class)
@RunWith(SpringRunner.class)
public class Test1{
    @Autowired
    ShareStockDayKService shareStockDayKService;
    @Autowired
    ShareStockService shareStockService;
    @Autowired
    ShareStockIndexService shareStockIndexService;

    @Test
    public void test(){
        IPage<ShareStockDayK> page = new Page<>(0,10);
        ShareStockDayK shareStockDayK = new ShareStockDayK();
        shareStockDayK.setStockId(shareStockService.getByCode("000005").getId());
        QueryWrapper<ShareStockDayK> queryWrapper = new QueryWrapper<>();
        queryWrapper.setEntity(shareStockDayK);
        IPage<ShareStockDayK> page1 = shareStockDayKService.page(page, queryWrapper);
        System.out.println(page1.toString());
    }

    @Test
    public void test2(){
        IPage<ShareStockIndex> page = new Page<>(0,20);
        ShareStockIndex shareStockIndex = new ShareStockIndex();
        QueryWrapper<ShareStockIndex> queryWrapper = new QueryWrapper<>();
        queryWrapper.setEntity(shareStockIndex);
        IPage<ShareStockIndex> page1 = shareStockIndexService.page(page, queryWrapper);
        List<ShareStockIndex> records = page1.getRecords();
        for (ShareStockIndex record : records) {
            System.out.println(record.toString());
        }
        System.out.println(page1.getRecords().size());
    }
}