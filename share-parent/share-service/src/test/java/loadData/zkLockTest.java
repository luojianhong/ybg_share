package loadData;

import com.ybg.ShareApplication;
import com.ybg.framework.core.zookeeper.DefaultZkLockConfig;
import com.ybg.framework.core.zookeeper.ZkDistributedLock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


public class zkLockTest {


    public static void main (String args[]){

        DefaultZkLockConfig config = new DefaultZkLockConfig();
        config.setZkServers("127.0.0.1:2181");
        config.setBasePath("/lock/");
        new Thread(new Runnable() {
            @Override
            public void run() {

                ZkDistributedLock lock = new ZkDistributedLock(config,"test");
                boolean trylock= lock.tryLock();
                if(!trylock){
                    System.out.println("已有其他服务获取1");
                    return ;
                }
                try{
                    System.out.println("线程1");
                    Thread.sleep(3000);
                    //这里放业务代码;
                }catch(Exception ex){
                    //异常处理
                }finally{
                    lock.unlock();
                }


            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {

                ZkDistributedLock lock = new ZkDistributedLock(config,"test");
               boolean trylock= lock.tryLock();
               if(!trylock){
                   System.out.println("已有其他服务获取2");
                   return ;
               }
                try{
                    System.out.println("线程2");
                    Thread.sleep(3000);
                    //这里放业务代码;
                }catch(Exception ex){
                    //异常处理
                }finally{
                    lock.unlock();
                }


            }
        }).start();


        while (true){
            try {
                Thread.sleep(3000L);
              //  System.out.println("休息中");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
