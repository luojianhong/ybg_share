package loadData;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.ShareApplication;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.core.pbest.service.SimplePbestService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest(classes = ShareApplication.class)
@RunWith(SpringRunner.class)
public class PbestServiceHistoryImplTest {

    @Autowired
    ShareStockService shareStockService;
    @Autowired
    SimplePbestService pbestService;

    static volatile int i=1991;

    /**
     * 得到历史记录
     */
    @Test
    public void test() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(ShareStock.STATUS, 1);
//        queryWrapper.gt(ShareStock.ID,5788);
//        queryWrapper.lt(ShareStock.ID,5842);
       // queryWrapper.orderByDesc(ShareStock.ID);
        List<ShareStock> list = shareStockService.list(queryWrapper);

        for (ShareStock shareStock : list) {
            //for ( i = 2010; i <= 2018; i++) {
//                ExecutorService dayKThreadPool = ThreadUtil.getDayKThreadPool();
//                dayKThreadPool.submit(()-> {pbestService.simplePbest(shareStock, i);});
               // pbestService.simplePbest(shareStock, i);

           // }
            pbestService.simplePbest(shareStock, 2019);
        }
    }

}
