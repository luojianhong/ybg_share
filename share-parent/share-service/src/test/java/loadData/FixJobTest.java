package loadData;

import com.ybg.ShareApplication;
import com.ybg.share.schedule.FixDataJob;
import com.ybg.share.schedule.StockFixDayKJob;
import com.ybg.share.utils.ThreadUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = ShareApplication.class)
@RunWith(SpringRunner.class)
public class FixJobTest {
    @Autowired
    FixDataJob fixDataJob;
    @Test
    public  void test(){
        fixDataJob.fixStockState();
        while (true){
            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    @Autowired
    StockFixDayKJob stockFixDayKJob;
    @Test
    public void test2(){
        stockFixDayKJob.fixDayK();
    }
}
