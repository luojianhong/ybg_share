package neuroph;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.ShareApplication;
import com.ybg.framework.enums.MarketEnum;
import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.ybg.share.core.dbapi.service.ShareStockIndexService;
import com.ybg.share.framework.neural.listener.RBFListener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.RBFNetwork;
import org.neuroph.nnet.learning.RBFLearning;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest(classes = ShareApplication.class)
@RunWith(SpringRunner.class)
public class StockIndex {
    @Autowired
    ShareStockIndexService shareStockIndexService;


    @Test
    public void indexGenNeuroph() {
        try {
            QueryWrapper<ShareStockIndex> wrapper = new QueryWrapper<>();
            wrapper.eq(ShareStockIndex.MARKET, MarketEnum.sh.getCode());
            wrapper.gt(ShareStockIndex.REF_DATE, "2008-01-01");
            wrapper.orderByAsc(ShareStockIndex.REF_DATE);

            List<ShareStockIndex> list = shareStockIndexService.list(wrapper);
            System.out.println("数据量:"+list.size());
            NeuralNetwork neuralNetwork = new RBFNetwork(2, 40, 1);
            DataSet trainingSet = new DataSet(2, 1);
            for (ShareStockIndex shareStockIndex : list) {
                //    System.out.println(shareStockIndex.getChangeAmount().doubleValue());

                trainingSet.add(new DataSetRow(new double[]{shareStockIndex.getOpenPrice().doubleValue(), shareStockIndex.getClosePrice().doubleValue(),}, new double[]{shareStockIndex.getChangeAmount().doubleValue()}));


            }
            RBFLearning learningRule = ((RBFLearning) neuralNetwork.getLearningRule());
            System.out.println("默认学习率"+learningRule.getLearningRate());
            System.out.println("默认最大错误数"+learningRule.getMaxError());
            learningRule.setLearningRate(0.00001);
           learningRule.setMaxError(3000);
            learningRule.addListener(new RBFListener());
            System.out.println("循环结束" + DateUtil.now());
            neuralNetwork.learn(trainingSet);
            System.out.println("学习结束" + DateUtil.now());
            //  neuralNetwork.save("/test/shareText1.nnet");
            System.out.println("保存结束" + DateUtil.now());
            neuralNetwork.setInput(2945, 2895);
            neuralNetwork.calculate();
            System.out.println("准备输出结果" + DateUtil.now());
            double[] networkOutput = neuralNetwork.getOutput();
            System.out.println("结果=" + (networkOutput[0] == 0d ? "跌或持平" : "涨") + networkOutput[0] + networkOutput);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
