import com.ybg.ShareApplication;
import com.ybg.share.schedule.StockFixDayKJob;
import com.ybg.share.schedule.StockJob;
import com.ybg.share.utils.ThreadUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = ShareApplication.class)
@RunWith(SpringRunner.class)
public class TestLoadDate {

    @Autowired
    StockFixDayKJob job;
    @Autowired
    StockJob stockJob;

    /**
     * 测试定时任务
     */
    @Test
    public void test() {
      //  job.updateStockDayK();

     //   job.updatePbest();
        stockJob.updateStockDataByWangyi();
        while (true){
            try {
                System.out.println("执行完毕");
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(ThreadUtil.THREAD_SEMAPHORE.availablePermits()+","+ThreadUtil.THREAD_SEMAPHORE.getQueueLength());
        }
    }

}
