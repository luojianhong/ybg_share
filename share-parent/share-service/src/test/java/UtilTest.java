import com.ybg.share.framework.shock.ChartDataCalculateUtils;
import com.ybg.share.framework.shock.entity.base.Candle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UtilTest {

    public static void main(String[] args) {
        //注意事项
        // 使用需知 chart_base_entity所有的类是你选用图表时候对应的数据 需要去掉
        //现在为了和图表隔离 提供单纯的算法工具类 所以才提供了临时的Entry Candle BarEntry等
        List<Candle> a= new ArrayList<>();
        ChartDataCalculateUtils calculateUtils = new ChartDataCalculateUtils(a);

        //部分指标需要选择周期 如EMA7 EMA10
        List<Integer> periods = Arrays.asList(7, 10);
        System.out.println( calculateUtils.getEMA(periods));
        System.out.println(  calculateUtils.getBollData(1, 2));
        System.out.println( calculateUtils.getCCI(1, 1));
        System.out.println(  calculateUtils.getDMI(1, 1, 1));
        System.out.println(  calculateUtils.getKDJ(periods));
        System.out.println(  calculateUtils.getMACD(1, 1, 1));
        System.out.println(  calculateUtils.getOBV(1, 2));
        System.out.println(  calculateUtils.getRSI(periods));
        System.out.println(  calculateUtils.getSARLineDatas(1f, 2f, 1, 1));
        System.out.println(  calculateUtils.getVol());
        System.out.println(  calculateUtils.getWR(1, 2));

    }
}
