package com.ybg;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@Slf4j
@MapperScan("com.ybg.share.core.dbapi.dao")
@EnableScheduling //开启定时任务
public class ShareApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShareApplication.class, args);
        log.info("股票分析系统启动完毕！更新日期2020-01-03 09:22");
    }
}
