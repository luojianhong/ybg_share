package com.ybg.share.config;

import cn.hutool.core.util.StrUtil;
import com.ybg.framework.constant.RedisKey;
import com.ybg.framework.core.RedisClient;
import com.ybg.framework.dto.AbstractJob;
import com.ybg.framework.enums.SyncPolicyEnum;
import com.ybg.share.core.dbapi.service.ShareStockDayKService;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.utils.ThreadUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import java.nio.charset.StandardCharsets;

/**
 * 系统初始化 个性定制
 */
@Slf4j
@Component
public class SystemInit implements ApplicationListener<ApplicationReadyEvent> {
    /**
     * 个股日K ， 分表个数
     */
    public static final int SHARE_STOCK_DAYK_TABLESHARDING = 40;

    /**
     * 月度统计，分表个数
     *
     * @param applicationReadyEvent
     */
    public static final int SHARE_STOCK_COUNT_MONTH_TABLESHARDING = 5;


    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        log.info("系统初始化 ");
        if (RedisClient.get(RedisKey.INIT_TABLE_FINISH) != null) {
            log.info("系统不需要再初始化");
            return;
        }
        initTable();
        initSyncPolicyRedisKey();
        RedisClient.set(RedisKey.INIT_TABLE_FINISH,"1",0) ;
        log.info("系统初始化完毕");
    }

    @Autowired
    ShareStockDayKService shareStockDayKService;
    @Autowired
    ShareStockService shareStockService;

    public void initTable() {

        try {
            Resource resource = new ClassPathResource("init.sql");
            byte[] bdata = FileCopyUtils.copyToByteArray(resource.getInputStream());
            String sql = new String(bdata, StandardCharsets.UTF_8);
//            ThreadUtil.execute(() -> {
//                shareStockService.initSystem(sql);
//            });

            ThreadUtil.execute(new AbstractJob(sql) {
                @Override
                public void execute() {
                    String sql = (String) getParams();
                    shareStockService.initSystem(sql);
                }
            });

        } catch (Exception e) {
            log.error("初始化表失败！！！！！", e);
        }
        shareStockDayKService.initTable();
    }


    /**
     * 初始化 同步数据redis Key
     */
    public void initSyncPolicyRedisKey() {
        //初始化 执行策略
        String syncDaykPolicy = RedisClient.get(RedisKey.SYNC_DAY_K_POLICY);
        if (StrUtil.isBlank(syncDaykPolicy)) {
            RedisClient.set(RedisKey.SYNC_DAY_K_POLICY, SyncPolicyEnum.all_rewrite.getValue() + "", 0);
        }

    }

}
