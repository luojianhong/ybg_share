package com.ybg.share.core.remoteapi.datamapper;

import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.ybg.share.core.remoteapi.vo.StockIndexDayKVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface StockIndexDataMapper {
    StockIndexDataMapper INSTANCE = Mappers.getMapper(StockIndexDataMapper.class);

    StockIndexDayKVO toVO(ShareStockIndex bean);

    List<StockIndexDayKVO> toVOList(List<ShareStockIndex> list);
}
