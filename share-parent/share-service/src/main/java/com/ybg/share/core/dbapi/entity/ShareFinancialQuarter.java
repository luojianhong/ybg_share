package com.ybg.share.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 财务报表-季度
 * </p>
 *
 * @author yanyu
 * @since 2019-10-27
 */
@ApiModel(value = "财务报表-季度")
public class ShareFinancialQuarter extends Model<ShareFinancialQuarter> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 报告月份 格式yyyy-MM-dd
     */
    @ApiModelProperty(value = "报告月份 格式yyyy-MM-dd")
    private String reportDate;
    /**
     * 个股ID
     */
    @ApiModelProperty(value = "个股ID")
    private Long stockId;
    /**
     * 股票代码
     */
    @ApiModelProperty(value = "股票代码")
    private String code;
    /**
     * 基本每股收益(元)
     */
    @ApiModelProperty(value = "基本每股收益(元)")
    private BigDecimal basicEps;
    /**
     * 每股净资产(元)
     */
    @ApiModelProperty(value = "每股净资产(元)")
    private BigDecimal eps;
    /**
     * 每股经营活动产生的现金流量净额(元)
     */
    @ApiModelProperty(value = "每股经营活动产生的现金流量净额(元)")
    private String cashFlow;
    /**
     * 主营业务收入(万元)
     */
    @ApiModelProperty(value = "主营业务收入(万元)")
    private BigDecimal revenue;
    /**
     * 主营业务利润(万元)
     */
    @ApiModelProperty(value = "主营业务利润(万元)")
    private BigDecimal profitability;
    /**
     * 营业利润(万元)
     */
    @ApiModelProperty(value = "营业利润(万元)")
    private BigDecimal operatingProfit;
    /**
     * 投资收益
     */
    @ApiModelProperty(value = "投资收益")
    private BigDecimal investmentReturn;
    /**
     * 营业外收支净额(万元)
     */
    @ApiModelProperty(value = "营业外收支净额(万元)")
    private BigDecimal nonOperatingIncome;
    /**
     * 利润总额(万元)
     */
    @ApiModelProperty(value = "利润总额(万元)")
    private BigDecimal totalProfit;
    /**
     * 净利润(万元)
     */
    @ApiModelProperty(value = "净利润(万元)")
    private BigDecimal netProfit;
    /**
     * 净利润(扣除非经常性损益后)(万元)
     */
    @ApiModelProperty(value = "净利润(扣除非经常性损益后)(万元)")
    private BigDecimal deductProfit;
    /**
     * 经营活动产生的现金流量净额(万元)
     */
    @ApiModelProperty(value = "经营活动产生的现金流量净额(万元)")
    private BigDecimal netCashFlow;
    /**
     * 现金及现金等价物净增加额(万元)
     */
    @ApiModelProperty(value = "现金及现金等价物净增加额(万元)")
    private BigDecimal netIncreaseCash;
    /**
     * 总资产(万元)
     */
    @ApiModelProperty(value = "总资产(万元)")
    private BigDecimal totalAssets;
    /**
     * 流动资产(万元)
     */
    @ApiModelProperty(value = "流动资产(万元)")
    private BigDecimal currentAssets;
    /**
     * 总负债（万元）
     */
    @ApiModelProperty(value = "总负债（万元）")
    private BigDecimal totalLiabilities;
    /**
     * 流动负债(万元)
     */
    @ApiModelProperty(value = "流动负债(万元)")
    private BigDecimal currentLiabilities;
    /**
     * 股东权益不含少数股东权益(万元)
     */
    @ApiModelProperty(value = "股东权益不含少数股东权益(万元)")
    private BigDecimal stockholderEquity;
    /**
     * 净资产收益率加权(%)
     */
    @ApiModelProperty(value = "净资产收益率加权(%)")
    private BigDecimal weightedReturnEquity;
    /**
     * 交易市场
     */
    @ApiModelProperty(value = "交易市场")
    private String market;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取报告月份 格式yyyy-MM-dd
     */
    public String getReportDate() {
        return reportDate;
    }

    /**
     * 设置报告月份 格式yyyy-MM-dd
     */

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    /**
     * 获取个股ID
     */
    public Long getStockId() {
        return stockId;
    }

    /**
     * 设置个股ID
     */

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    /**
     * 获取股票代码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置股票代码
     */

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取基本每股收益(元)
     */
    public BigDecimal getBasicEps() {
        return basicEps;
    }

    /**
     * 设置基本每股收益(元)
     */

    public void setBasicEps(BigDecimal basicEps) {
        this.basicEps = basicEps;
    }

    /**
     * 获取每股净资产(元)
     */
    public BigDecimal getEps() {
        return eps;
    }

    /**
     * 设置每股净资产(元)
     */

    public void setEps(BigDecimal eps) {
        this.eps = eps;
    }

    /**
     * 获取每股经营活动产生的现金流量净额(元)
     */
    public String getCashFlow() {
        return cashFlow;
    }

    /**
     * 设置每股经营活动产生的现金流量净额(元)
     */

    public void setCashFlow(String cashFlow) {
        this.cashFlow = cashFlow;
    }

    /**
     * 获取主营业务收入(万元)
     */
    public BigDecimal getRevenue() {
        return revenue;
    }

    /**
     * 设置主营业务收入(万元)
     */

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    /**
     * 获取主营业务利润(万元)
     */
    public BigDecimal getProfitability() {
        return profitability;
    }

    /**
     * 设置主营业务利润(万元)
     */

    public void setProfitability(BigDecimal profitability) {
        this.profitability = profitability;
    }

    /**
     * 获取营业利润(万元)
     */
    public BigDecimal getOperatingProfit() {
        return operatingProfit;
    }

    /**
     * 设置营业利润(万元)
     */

    public void setOperatingProfit(BigDecimal operatingProfit) {
        this.operatingProfit = operatingProfit;
    }

    /**
     * 获取投资收益
     */
    public BigDecimal getInvestmentReturn() {
        return investmentReturn;
    }

    /**
     * 设置投资收益
     */

    public void setInvestmentReturn(BigDecimal investmentReturn) {
        this.investmentReturn = investmentReturn;
    }

    /**
     * 获取营业外收支净额(万元)
     */
    public BigDecimal getNonOperatingIncome() {
        return nonOperatingIncome;
    }

    /**
     * 设置营业外收支净额(万元)
     */

    public void setNonOperatingIncome(BigDecimal nonOperatingIncome) {
        this.nonOperatingIncome = nonOperatingIncome;
    }

    /**
     * 获取利润总额(万元)
     */
    public BigDecimal getTotalProfit() {
        return totalProfit;
    }

    /**
     * 设置利润总额(万元)
     */

    public void setTotalProfit(BigDecimal totalProfit) {
        this.totalProfit = totalProfit;
    }

    /**
     * 获取净利润(万元)
     */
    public BigDecimal getNetProfit() {
        return netProfit;
    }

    /**
     * 设置净利润(万元)
     */

    public void setNetProfit(BigDecimal netProfit) {
        this.netProfit = netProfit;
    }

    /**
     * 获取净利润(扣除非经常性损益后)(万元)
     */
    public BigDecimal getDeductProfit() {
        return deductProfit;
    }

    /**
     * 设置净利润(扣除非经常性损益后)(万元)
     */

    public void setDeductProfit(BigDecimal deductProfit) {
        this.deductProfit = deductProfit;
    }

    /**
     * 获取经营活动产生的现金流量净额(万元)
     */
    public BigDecimal getNetCashFlow() {
        return netCashFlow;
    }

    /**
     * 设置经营活动产生的现金流量净额(万元)
     */

    public void setNetCashFlow(BigDecimal netCashFlow) {
        this.netCashFlow = netCashFlow;
    }

    /**
     * 获取现金及现金等价物净增加额(万元)
     */
    public BigDecimal getNetIncreaseCash() {
        return netIncreaseCash;
    }

    /**
     * 设置现金及现金等价物净增加额(万元)
     */

    public void setNetIncreaseCash(BigDecimal netIncreaseCash) {
        this.netIncreaseCash = netIncreaseCash;
    }

    /**
     * 获取总资产(万元)
     */
    public BigDecimal getTotalAssets() {
        return totalAssets;
    }

    /**
     * 设置总资产(万元)
     */

    public void setTotalAssets(BigDecimal totalAssets) {
        this.totalAssets = totalAssets;
    }

    /**
     * 获取流动资产(万元)
     */
    public BigDecimal getCurrentAssets() {
        return currentAssets;
    }

    /**
     * 设置流动资产(万元)
     */

    public void setCurrentAssets(BigDecimal currentAssets) {
        this.currentAssets = currentAssets;
    }

    /**
     * 获取总负债（万元）
     */
    public BigDecimal getTotalLiabilities() {
        return totalLiabilities;
    }

    /**
     * 设置总负债（万元）
     */

    public void setTotalLiabilities(BigDecimal totalLiabilities) {
        this.totalLiabilities = totalLiabilities;
    }

    /**
     * 获取流动负债(万元)
     */
    public BigDecimal getCurrentLiabilities() {
        return currentLiabilities;
    }

    /**
     * 设置流动负债(万元)
     */

    public void setCurrentLiabilities(BigDecimal currentLiabilities) {
        this.currentLiabilities = currentLiabilities;
    }

    /**
     * 获取股东权益不含少数股东权益(万元)
     */
    public BigDecimal getStockholderEquity() {
        return stockholderEquity;
    }

    /**
     * 设置股东权益不含少数股东权益(万元)
     */

    public void setStockholderEquity(BigDecimal stockholderEquity) {
        this.stockholderEquity = stockholderEquity;
    }

    /**
     * 获取净资产收益率加权(%)
     */
    public BigDecimal getWeightedReturnEquity() {
        return weightedReturnEquity;
    }

    /**
     * 设置净资产收益率加权(%)
     */

    public void setWeightedReturnEquity(BigDecimal weightedReturnEquity) {
        this.weightedReturnEquity = weightedReturnEquity;
    }

    /**
     * 获取交易市场
     */
    public String getMarket() {
        return market;
    }

    /**
     * 设置交易市场
     */

    public void setMarket(String market) {
        this.market = market;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 报告月份 格式yyyy-MM-dd列的数据库字段名称
     */
    public static final String REPORT_DATE = "report_date";

    /**
     * 个股ID列的数据库字段名称
     */
    public static final String STOCK_ID = "stock_id";

    /**
     * 股票代码列的数据库字段名称
     */
    public static final String CODE = "code";

    /**
     * 基本每股收益(元)列的数据库字段名称
     */
    public static final String BASIC_EPS = "basic_eps";

    /**
     * 每股净资产(元)列的数据库字段名称
     */
    public static final String EPS = "eps";

    /**
     * 每股经营活动产生的现金流量净额(元)列的数据库字段名称
     */
    public static final String CASH_FLOW = "cash_flow";

    /**
     * 主营业务收入(万元)列的数据库字段名称
     */
    public static final String REVENUE = "revenue";

    /**
     * 主营业务利润(万元)列的数据库字段名称
     */
    public static final String PROFITABILITY = "profitability";

    /**
     * 营业利润(万元)列的数据库字段名称
     */
    public static final String OPERATING_PROFIT = "operating_profit";

    /**
     * 投资收益列的数据库字段名称
     */
    public static final String INVESTMENT_RETURN = "investment_return";

    /**
     * 营业外收支净额(万元)列的数据库字段名称
     */
    public static final String NON_OPERATING_INCOME = "non_operating_income";

    /**
     * 利润总额(万元)列的数据库字段名称
     */
    public static final String TOTAL_PROFIT = "total_profit";

    /**
     * 净利润(万元)列的数据库字段名称
     */
    public static final String NET_PROFIT = "net_profit";

    /**
     * 净利润(扣除非经常性损益后)(万元)列的数据库字段名称
     */
    public static final String DEDUCT_PROFIT = "deduct_profit";

    /**
     * 经营活动产生的现金流量净额(万元)列的数据库字段名称
     */
    public static final String NET_CASH_FLOW = "net_cash_flow";

    /**
     * 现金及现金等价物净增加额(万元)列的数据库字段名称
     */
    public static final String NET_INCREASE_CASH = "net_increase_cash";

    /**
     * 总资产(万元)列的数据库字段名称
     */
    public static final String TOTAL_ASSETS = "total_assets";

    /**
     * 流动资产(万元)列的数据库字段名称
     */
    public static final String CURRENT_ASSETS = "current_assets";

    /**
     * 总负债（万元）列的数据库字段名称
     */
    public static final String TOTAL_LIABILITIES = "total_liabilities";

    /**
     * 流动负债(万元)列的数据库字段名称
     */
    public static final String CURRENT_LIABILITIES = "current_liabilities";

    /**
     * 股东权益不含少数股东权益(万元)列的数据库字段名称
     */
    public static final String STOCKHOLDER_EQUITY = "stockholder_equity";

    /**
     * 净资产收益率加权(%)列的数据库字段名称
     */
    public static final String WEIGHTED_RETURN_EQUITY = "weighted_return_equity";

    /**
     * 交易市场列的数据库字段名称
     */
    public static final String MARKET = "market";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareFinancialQuarter{" +
                "id=" + id +
                ", reportDate=" + reportDate +
                ", stockId=" + stockId +
                ", code=" + code +
                ", basicEps=" + basicEps +
                ", eps=" + eps +
                ", cashFlow=" + cashFlow +
                ", revenue=" + revenue +
                ", profitability=" + profitability +
                ", operatingProfit=" + operatingProfit +
                ", investmentReturn=" + investmentReturn +
                ", nonOperatingIncome=" + nonOperatingIncome +
                ", totalProfit=" + totalProfit +
                ", netProfit=" + netProfit +
                ", deductProfit=" + deductProfit +
                ", netCashFlow=" + netCashFlow +
                ", netIncreaseCash=" + netIncreaseCash +
                ", totalAssets=" + totalAssets +
                ", currentAssets=" + currentAssets +
                ", totalLiabilities=" + totalLiabilities +
                ", currentLiabilities=" + currentLiabilities +
                ", stockholderEquity=" + stockholderEquity +
                ", weightedReturnEquity=" + weightedReturnEquity +
                ", market=" + market +
                "}";
    }
}
