package com.ybg.share.core.remoteapi;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.share.core.remoteapi.datamapper.StockIndexDataMapper;
import com.ybg.framework.vo.R;
import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.ybg.share.core.dbapi.service.ShareStockIndexService;
import com.ybg.share.core.remoteapi.dto.StockIndexDTO;
import com.ybg.share.core.remoteapi.vo.StockIndexDayKVO;
import com.ybg.share.core.shareapi.impl.ShanghaiStockIndexApi;
import com.ybg.share.core.shareapi.impl.ShenzhenStockIndexApi;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(version = "${dubbo.provider.version}",timeout = 120000,cache = "false")
public class StockIndexRemoteApiImpl implements StockIndexRemoteApi {
    @Autowired
    private ShareStockIndexService shareStockIndexService;
    @Autowired
    ShanghaiStockIndexApi shanghaiStockIndexApi;
    @Autowired
    ShenzhenStockIndexApi shenZhenStockIndexApi;

    @Override
    public List<StockIndexDayKVO> getStockIndexDayK(String market, String startDate, String endDate) {
        QueryWrapper<ShareStockIndex> wrapper = new QueryWrapper<>();
        wrapper.eq(ShareStockIndex.MARKET, market);
        wrapper.ge(StrUtil.isNotBlank(startDate), ShareStockIndex.REF_DATE, startDate);
        wrapper.ge(StrUtil.isNotBlank(endDate), ShareStockIndex.REF_DATE, endDate);
        List<ShareStockIndex> list = shareStockIndexService.list(wrapper);
        return StockIndexDataMapper.INSTANCE.toVOList(list);
    }

    @Override
    public R syncStockIndex() {
        //加入锁
        shanghaiStockIndexApi.execute();
        shenZhenStockIndexApi.execute();
        return R.success();
    }

    @Override
    public StockIndexDayKVO getStockIndexDayK(String market, String date) {
        QueryWrapper<ShareStockIndex> wrapper = new QueryWrapper<>();
        wrapper.eq(ShareStockIndex.MARKET, market);
        wrapper.eq(ShareStockIndex.REF_DATE, date);
        ShareStockIndex bean = shareStockIndexService.getOne(wrapper);
        return StockIndexDataMapper.INSTANCE.toVO(bean);

    }

    @Override
    public Page<StockIndexDayKVO> stockIndexDayKpage(StockIndexDTO dto) {
        return shareStockIndexService.pageStock(dto);
    }
}
