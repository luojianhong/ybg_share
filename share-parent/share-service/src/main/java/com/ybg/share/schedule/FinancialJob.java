package com.ybg.share.schedule;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.framework.core.zookeeper.DefaultZkLockConfig;
import com.ybg.framework.core.zookeeper.ZkDistributedLock;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.core.shareapi.impl.FinancialQuarterApi;
import com.ybg.share.core.shareapi.impl.FinancialYearApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 财报数据定时任务
 */
@Component
@Slf4j
public class FinancialJob {
    @Autowired
    private FinancialQuarterApi financialQuarterApi;
    @Autowired
    private ShareStockService shareStockService;
    @Autowired
    private FinancialYearApi financialYearApi;
    @Autowired
    private    DefaultZkLockConfig defaultZkLockConfig;

    /**
     * 更新季度财报数据
     */
    @Scheduled(cron = "1 30 8 1 1/1 ?")
    public void updateQuarter() {
        ZkDistributedLock lock = new ZkDistributedLock(defaultZkLockConfig,"updateQuarter");
        boolean trylock= lock.tryLock();
        if(!trylock){
            return ;
        }
        try {
            QueryWrapper<ShareStock> wrapper = new QueryWrapper<>();
            wrapper.eq(ShareStock.STATUS, 1);
            List<ShareStock> shareStocks = shareStockService.list(wrapper);
            for (ShareStock shareStock : shareStocks) {
                financialQuarterApi.execute(shareStock);
                financialYearApi.execute(shareStock);
            }
        }catch (Exception e){
            log.info("{}",e);
        }finally {
            lock.unlock();
        }
    }




}
