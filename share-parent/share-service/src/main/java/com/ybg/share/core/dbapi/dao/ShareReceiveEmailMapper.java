package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareReceiveEmail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员接收邮箱的列表 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-12-30
 */
public interface ShareReceiveEmailMapper extends BaseMapper<ShareReceiveEmail> {

}
