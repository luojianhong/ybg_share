package com.ybg.share.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;


/**
 * 暴力破解
 *  * @Auther: ZLF，yanyu
 *  * @Date: 2018/6/28 10:03
 *  * @Description:zip文件解压缩工具类
 *  
 */
public class UnZipUtils {
    private static ExecutorService executorDayKThreadPool = new ThreadPoolExecutor(100000, 100000,
            0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(100000), new ThreadPoolExecutor.CallerRunsPolicy());

    public static void main(String[] args) throws IOException {
        UnZipUtils z = new UnZipUtils();
        String source = "C:\\Users\\Administrator\\Desktop\\b.rar";
        String dest = "D:\\";
        String password3 = z.unZip(source, dest, "123456");

            System.out.println("解压密码是1111111----------:" + password3);


//        for(long i=123455;i<1000000;i++){
//
//            String password =TestGenInviteCode.encode(i);
//            System.out.println(DateUtil.now() +"i="+i+","+password);
//            if(i%10000==0){
//                System.out.println(DateUtil.now() +"i="+i+","+password);
//                break;
//
//            }
//
//           // executorDayKThreadPool.submit(()->{
//                String password2 = z.unZip(source, dest, password);
//                if (StrUtil.isNotBlank(password2)) {
//                    System.out.println("解压密码是" + password2);
//                    break;
//                }
//           // });
//
//        }


    }


    /**
     * @param source   原始文件路径
     * @param dest     解压路径
     * @param password 解压文件密码(可以为空)
     */
    public String unZip(String source, String dest, String password) {
        try {
            File zipFile = new File(source);
            ZipFile zFile = new ZipFile(zipFile); // 首先创建ZipFile指向磁盘上的.zip文件
            zFile.setFileNameCharset("GBK");
            File destDir = new File(dest); // 解压目录
            if (!destDir.exists()) {// 目标目录不存在时，创建该文件夹
                destDir.mkdirs();
            }
            if (zFile.isEncrypted()) {
                zFile.setPassword(password.toCharArray()); // 设置密码
            }
            zFile.extractAll(dest); // 将文件抽出到解压目录(解压)
            return password;
        } catch (ZipException e) {
          //  e.printStackTrace();
        }
        return "";
    }
}

