package com.ybg.share.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 最佳买卖算法记录表
 * </p>
 *
 * @author yanyu
 * @since 2019-11-04
 */
@ApiModel(value = "最佳买卖算法记录表")
public class ShareStockPbest extends Model<ShareStockPbest> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 股票ID
     */
    @ApiModelProperty(value = "股票ID")
    private Long stockId;
    /**
     * 年份
     */
    @ApiModelProperty(value = "年份")
    private Integer refYear;
    /**
     * 买入价格
     */
    @ApiModelProperty(value = "买入价格")
    private BigDecimal buyPrice;
    /**
     * 卖出价格
     */
    @ApiModelProperty(value = "卖出价格")
    private BigDecimal sellPrice;
    /**
     * 算法名称
     */
    @ApiModelProperty(value = "算法名称")
    private String pbestName;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 最大次数
     */
    @ApiModelProperty(value = "最大次数")
    private Integer times;
    /**
     * 盈利
     */
    @ApiModelProperty(value = "盈利")
    private BigDecimal profit;
    /**
     * 利润率
     */
    @ApiModelProperty(value = "利润率")
    private BigDecimal profitRate;
    /**
     * 股票代码
     */
    @ApiModelProperty(value = "股票代码")
    private String stockCode;
    /**
     * 市场
     */
    @ApiModelProperty(value = "市场")
    private String market;
    /**
     * 股票名称
     */
    @ApiModelProperty(value = "股票名称")
    private String stockName;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取股票ID
     */
    public Long getStockId() {
        return stockId;
    }

    /**
     * 设置股票ID
     */

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    /**
     * 获取年份
     */
    public Integer getRefYear() {
        return refYear;
    }

    /**
     * 设置年份
     */

    public void setRefYear(Integer refYear) {
        this.refYear = refYear;
    }

    /**
     * 获取买入价格
     */
    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    /**
     * 设置买入价格
     */

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    /**
     * 获取卖出价格
     */
    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    /**
     * 设置卖出价格
     */

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    /**
     * 获取算法名称
     */
    public String getPbestName() {
        return pbestName;
    }

    /**
     * 设置算法名称
     */

    public void setPbestName(String pbestName) {
        this.pbestName = pbestName;
    }

    /**
     * 获取更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     */

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取创建时间
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     */

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取最大次数
     */
    public Integer getTimes() {
        return times;
    }

    /**
     * 设置最大次数
     */

    public void setTimes(Integer times) {
        this.times = times;
    }

    /**
     * 获取盈利
     */
    public BigDecimal getProfit() {
        return profit;
    }

    /**
     * 设置盈利
     */

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    /**
     * 获取利润率
     */
    public BigDecimal getProfitRate() {
        return profitRate;
    }

    /**
     * 设置利润率
     */

    public void setProfitRate(BigDecimal profitRate) {
        this.profitRate = profitRate;
    }

    /**
     * 获取股票代码
     */
    public String getStockCode() {
        return stockCode;
    }

    /**
     * 设置股票代码
     */

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    /**
     * 获取市场
     */
    public String getMarket() {
        return market;
    }

    /**
     * 设置市场
     */

    public void setMarket(String market) {
        this.market = market;
    }

    /**
     * 获取股票名称
     */
    public String getStockName() {
        return stockName;
    }

    /**
     * 设置股票名称
     */

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 股票ID列的数据库字段名称
     */
    public static final String STOCK_ID = "stock_id";

    /**
     * 年份列的数据库字段名称
     */
    public static final String REF_YEAR = "ref_year";

    /**
     * 买入价格列的数据库字段名称
     */
    public static final String BUY_PRICE = "buy_price";

    /**
     * 卖出价格列的数据库字段名称
     */
    public static final String SELL_PRICE = "sell_price";

    /**
     * 算法名称列的数据库字段名称
     */
    public static final String PBEST_NAME = "pbest_name";

    /**
     * 更新时间列的数据库字段名称
     */
    public static final String UPDATE_TIME = "update_time";

    /**
     * 创建时间列的数据库字段名称
     */
    public static final String CREATE_TIME = "create_time";

    /**
     * 最大次数列的数据库字段名称
     */
    public static final String TIMES = "times";

    /**
     * 盈利列的数据库字段名称
     */
    public static final String PROFIT = "profit";

    /**
     * 利润率列的数据库字段名称
     */
    public static final String PROFIT_RATE = "profit_rate";

    /**
     * 股票代码列的数据库字段名称
     */
    public static final String STOCK_CODE = "stock_code";

    /**
     * 市场列的数据库字段名称
     */
    public static final String MARKET = "market";

    /**
     * 股票名称列的数据库字段名称
     */
    public static final String STOCK_NAME = "stock_name";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareStockPbest{" +
                "id=" + id +
                ", stockId=" + stockId +
                ", refYear=" + refYear +
                ", buyPrice=" + buyPrice +
                ", sellPrice=" + sellPrice +
                ", pbestName=" + pbestName +
                ", updateTime=" + updateTime +
                ", createTime=" + createTime +
                ", times=" + times +
                ", profit=" + profit +
                ", profitRate=" + profitRate +
                ", stockCode=" + stockCode +
                ", market=" + market +
                ", stockName=" + stockName +
                "}";
    }
}
