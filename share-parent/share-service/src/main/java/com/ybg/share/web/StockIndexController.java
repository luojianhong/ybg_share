package com.ybg.share.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ybg.framework.vo.R;
import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.ybg.share.core.dbapi.service.ShareStockIndexService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "大盘信息数据接口")
//@RestController
public class StockIndexController {
    @Autowired
    ShareStockIndexService shareStockIndexService;

    @ApiOperation(value = "股票大盘数据分页查询", notes = "股票大盘数据分页查询，参数：current(long),size(long)", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("/stockIndex/list")
    public R query(@ModelAttribute IPage<ShareStockIndex> page, @ModelAttribute ShareStockIndex shareStockIndex) {
        QueryWrapper<ShareStockIndex> queryWrapper = new QueryWrapper<>();
        queryWrapper.setEntity(shareStockIndex);
        IPage<ShareStockIndex> page1 = shareStockIndexService.page(page, queryWrapper);
        return new R<>(page1);
    }

}
