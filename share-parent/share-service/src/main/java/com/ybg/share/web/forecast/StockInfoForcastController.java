package com.ybg.share.web.forecast;

import com.ybg.framework.vo.R;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareStockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//@RestController
@Api(description = "个股股价预测")
public class StockInfoForcastController {

    @Autowired
    ShareStockService stockService;

    @ApiOperation("普通预测股价走势")
    @GetMapping("/api/forcast/stock/price")
    public R forcastStockPrice(
            @RequestParam("code") @ApiParam(value = "股票代码", example = "601069") String code,
            @RequestParam(value = "startDate", required = false) @ApiParam(value = "采取的数据范围-开始日期 格式yyyy-MM-dd", example = "2017-01-01") String startDate,
            @RequestParam(value = "endDate", required = false) @ApiParam(value = "采取的数据范围-结束日期 格式yyyy-MM-dd", example = "2017-01-01") String endDate,
            @RequestParam(value = "step", required = true, defaultValue = "1") @ApiParam(value = "K线跨越的天数 ，要求大于1", example = "20") int step
    ) {
        ShareStock stock = stockService.getByCode(code);
        if (stock == null) {
            return new R("股票代码不合法");
        }
        if (step < 2) {
            return new R("跨越天数不合法");
        }
        return new R();
    }

    //public R monthPrice();


}
