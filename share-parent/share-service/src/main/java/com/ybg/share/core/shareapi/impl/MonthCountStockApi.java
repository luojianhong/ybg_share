package com.ybg.share.core.shareapi.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.share.core.shareapi.AbstractCountStockApi;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.entity.ShareStockCountMonth;
import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.dbapi.service.ShareStockCountMonthService;
import com.ybg.share.core.dbapi.service.ShareStockDayKService;
import com.ybg.share.core.shareapi.bo.ShareStockCountBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
@Service
public class MonthCountStockApi extends AbstractCountStockApi {
    @Autowired
    ShareStockDayKService shareStockDayKService;
    @Autowired
    ShareStockCountMonthService shareStockCountMonthService;

    @Override
    public List<ShareStockDayK> getdayKList(String timeStr) {
        ShareStock shareStock= getStockThreadLocal().get();
        QueryWrapper<ShareStockDayK> wrapper= new QueryWrapper<>();
        wrapper.eq(ShareStockDayK.STOCK_ID,shareStock.getId());
        wrapper.ge(ShareStockDayK.REF_DATE,timeStr+"-01");
        wrapper.le(ShareStockDayK.REF_DATE,timeStr+"-31");
        return  shareStockDayKService.list(wrapper);
    }

    @Override
    public void saveToDB(ShareStockCountBO bo) {
        ShareStockCountMonth entity= new ShareStockCountMonth();
        entity.setStockId(bo.getStockId());
        entity.setRefDate(getTimeThreadLocal().get());
        entity.setCode(bo.getCode());
        entity.setMarket(bo.getMarket());
        entity.setUpTimes(bo.getUpTimes());
        entity.setDownTimes(bo.getDownTimes());
        entity.setTopPrice(bo.getTopPrice());
        entity.setLowPrice(bo.getLowPrice());
        entity.setTopDate(bo.getTopDate());
        entity.setLowDate(bo.getLowDate());
        entity.setCreateTime(LocalDateTime.now());
        shareStockCountMonthService.saveReplace(entity);
    }
}
