package com.ybg.share.schedule;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.framework.core.zookeeper.DefaultZkLockConfig;
import com.ybg.framework.core.zookeeper.ZkDistributedLock;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.core.pbest.service.Day250PbestService;
import com.ybg.share.core.pbest.service.SimplePbestService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 计算最大利益定时任务
 * @author  Deament
 * @since 2020.1.27
 */
@Component
@Slf4j
public class StockPbestJob {

    @Autowired
    private ShareStockService shareStockService;
    @Autowired
    private SimplePbestService pbestService;
    @Autowired
    private Day250PbestService day250PbestService;
    @Autowired
    private DefaultZkLockConfig defaultZkLockConfig;

    /**
     * 计算最大利益
     * 固定买入法 （不变应万变）
     */
    @Scheduled(cron = "0 30 5 * * ?")
    public void updatePbest() {
        ZkDistributedLock lock = new ZkDistributedLock(defaultZkLockConfig, "updatePbest");
        boolean trylock = lock.tryLock();
        if (!trylock) {
            return;
        }
        try {
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq(ShareStock.STATUS, 1);
            List<ShareStock> list = shareStockService.list(queryWrapper);
            int year = DateUtil.year(new Date());
            for (ShareStock shareStock : list) {
                //按照年份计算最大利益
                pbestService.simplePbest(shareStock, year);
                //按照250个交易日来计算最大利益
                day250PbestService.dat250Pbest(shareStock);
            }
        } catch (Exception e) {
            log.info("error{}", e);
        } finally {
            lock.unlock();
        }
    }
}
