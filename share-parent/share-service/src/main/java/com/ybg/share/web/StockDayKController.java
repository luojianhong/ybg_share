package com.ybg.share.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ybg.framework.enums.MarketEnum;
import com.ybg.framework.enums.SyncPolicyEnum;
import com.ybg.framework.vo.R;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.dbapi.service.ShareStockDayKService;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.core.shareapi.impl.ShanghaiStockDayKApi;
import com.ybg.share.core.shareapi.impl.ShenzhenStockDayKApi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Api(description = "个股日K接口")
//@RestController
public class StockDayKController {

    @Autowired
    ShareStockDayKService shareStockDayKService;
    @Autowired
    ShareStockService shareStockService;

    @ApiOperation(value = "日K数据分页查询", notes = "日K数据分页查询，参数：code(String),current(long),size(long)", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("/stockDayK/list")
    public R<IPage<ShareStockDayK>> query(@ModelAttribute IPage<ShareStockDayK> page, @ModelAttribute ShareStockDayK shareStockDayK) {
        QueryWrapper<ShareStockDayK> queryWrapper = new QueryWrapper<>();
        if (null == shareStockDayK.getCode()) throw new RuntimeException("股票代码不能为空");
        shareStockDayK.setStockId(shareStockService.getByCode(shareStockDayK.getCode()).getId());
        queryWrapper.setEntity(shareStockDayK);
        return new R<>(shareStockDayKService.page(page, queryWrapper));
    }

    @Autowired
    ShanghaiStockDayKApi shanghaiStockDayKApi;
    @Autowired
    ShenzhenStockDayKApi shenzhenStockDayKApi;

    @ApiOperation("拉取日K数据")
    @PostMapping("/stockDayK/syncStockData")
    public R syncStockData(String code) {
        ShareStock shareStock = shareStockService.getByCode(code);
        if (shareStock == null) {
            return new R("不存在");
        }
        if (shareStock.getMarket().equals(MarketEnum.sh.getCode())) {
            shanghaiStockDayKApi.execute(shareStock, SyncPolicyEnum.all_ignore);
        }
        if (shareStock.getMarket().equals(MarketEnum.sz.getCode())) {
            shenzhenStockDayKApi.execute(shareStock,SyncPolicyEnum.all_ignore);
        }
        return new R(R.SUCCESS);

    }
}
