package com.ybg.share.core.dbapi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.share.core.dbapi.dao.sql.ShareFinancialSQL;
import com.ybg.share.core.dbapi.entity.ShareFinancialQuarter;
import com.ybg.share.core.remoteapi.dto.FinancialDTO;
import com.ybg.share.core.remoteapi.vo.FinancialVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

/**
 * <p>
 * 财务报表-季度 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-10-27
 */
public interface ShareFinancialQuarterMapper extends BaseMapper<ShareFinancialQuarter> {

    @Select("INSERT IGNORE INTO share_financial_quarter(report_date,stock_id,code,basic_eps,eps,cash_flow,revenue,profitability,operating_profit,investment_return,non_operating_income,total_profit,net_profit,deduct_profit,net_cash_flow,net_increase_cash,total_assets,current_assets,total_liabilities,current_liabilities,stockholder_equity,weighted_return_equity,market)" +
            " VALUES(#{reportDate},#{stockId},#{code},#{basicEps},#{eps},#{cashFlow},#{revenue},#{profitability},#{operatingProfit},#{investmentReturn},#{nonOperatingIncome},#{totalProfit},#{netProfit},#{deductProfit},#{netCashFlow},#{netIncreaseCash},#{totalAssets},#{currentAssets},#{totalLiabilities},#{currentLiabilities},#{stockholderEquity},#{weightedReturnEquity},#{market})")
    boolean saveIgnore(ShareFinancialQuarter bean);

    @SelectProvider(type = ShareFinancialSQL.class,method = "getFinancialOfQuarterByPage")
    Page<FinancialVO> getFinancialOfQuarterByPage(@Param("dto") FinancialDTO dto);


    @SelectProvider(type = ShareFinancialSQL.class,method = "countQuarter")
    public Long countQuarter(@Param("dto") FinancialDTO dto);
}
