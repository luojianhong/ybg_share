package com.ybg.share.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ybg.framework.vo.R;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareStockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "个股股票信息接口")
//@RestController
public class StockInfoController {
    @Autowired
    ShareStockService shareStockService;

    @ApiOperation(value = "股票信息分页查询", notes = "股票信息分页查询，参数：current(long),size(long)")
    @PostMapping("/stockInfo/list")
    public R query(@ModelAttribute IPage<ShareStock> page, @ModelAttribute ShareStock shareStock) {
        QueryWrapper<ShareStock> queryWrapper = new QueryWrapper<>();
        queryWrapper.setEntity(shareStock);
        return new R<>(shareStockService.page(page, queryWrapper));
    }
}
