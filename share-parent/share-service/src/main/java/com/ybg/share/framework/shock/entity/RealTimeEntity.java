package com.ybg.share.framework.shock.entity;

import com.ybg.share.framework.shock.entity.base.*;

import java.util.ArrayList;

/**
 * @author Hugh.HYS
 * @date 2018/11/12
 */
public class RealTimeEntity implements ChartEntity {
    public ArrayList<Entry> price;
    public ArrayList<Entry> priceMa;

    public RealTimeEntity() {
        this.price = new ArrayList<>();
        this.priceMa = new ArrayList<>();
    }

    @Override
    public void clearValues() {
        if (price != null) {
            price.clear();
        }
        if (priceMa != null) {
            priceMa.clear();
        }
    }
}
