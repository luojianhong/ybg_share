package com.ybg.share.core.shareapi.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.ybg.share.core.shareapi.AbstractStockDayKApi;
import com.ybg.framework.enums.MarketEnum;
import com.ybg.framework.enums.SyncPolicyEnum;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.dbapi.service.ShareStockDayKService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 拉取上海日K数据
 *
 * @author Maokun.zhong
 */
@Service
public class ShanghaiStockDayKApi extends AbstractStockDayKApi {
    @Autowired
    ShareStockDayKService shareStockDayKService;

    @Override
    public String getUrl(ShareStock stock, SyncPolicyEnum policy) {
        String today = DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN);
        return "http://quotes.money.163.com/service/chddata.html?" +
                "code=0" + stock.getCode() + "&" +
                "start=" + getStartTime(stock,policy) + "&end=" + today +
                "&fields=TCLOSE;HIGH;LOW;TOPEN;LCLOSE;CHG;PCHG;TURNOVER;VOTURNOVER;VATURNOVER;TCAP;MCAP";
    }

    @Override
    public String getMarket() {
        return MarketEnum.sh.getCode();
    }

    @Override
    public String getStartTime(ShareStock stock,SyncPolicyEnum policyEnum) {

        if (policyEnum.getValue() == SyncPolicyEnum.all_rewrite.getValue()) {
            MarketEnum.sh.getStartTime();
        }
        if (policyEnum.getValue() == SyncPolicyEnum.all_ignore.getValue()) {
            MarketEnum.sh.getStartTime();
        }
        if (policyEnum.getValue() == SyncPolicyEnum.increment.getValue()) {
            ShareStockDayK maxDateByCode = shareStockDayKService.getMaxDateByCode(stock.getCode(), stock.getId());
            if (null == maxDateByCode) {
                return MarketEnum.sh.getStartTime();
            }
            return maxDateByCode.getRefDate();
        }
        return MarketEnum.sh.getStartTime();
    }

}
