package com.ybg.share.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TestGenInviteCode {
    static int length = 6;

    public static void setLength(int length) {
        TestGenInviteCode.length = length;
    }

    private static final char STUFFS[] = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
//            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
//            'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
//            'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
//            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
//            'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
//            'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
           // '!','@','#','$','%','^','&','*','(',')','_','+','-','=','/','.',','
    };

    static Map<String, Integer> map = new LinkedHashMap<>();

    static {
        for (int j = 0; j < STUFFS.length; j++) {
            map.put(String.valueOf(STUFFS[j]), j);
        }
    }

    public static String encode(Long val) {
      //  System.out.println("最大数字是" + Math.pow(STUFFS.length, length));
        if (val > Math.pow(STUFFS.length, length)) {
          //  System.out.println("error生成失败最大数字是" + Math.pow(STUFFS.length, length));
        }
        List<Long> list = new ArrayList<>(length);
        StringBuilder code = new StringBuilder();
        int bit = getMaxBit(val);
       // System.out.println("位数是" + bit);
        long cVal = val;

        long next = 0;
        do {
            Long at = cVal % STUFFS.length;
            next = cVal / STUFFS.length;
           // System.out.println("at=" + at);
            cVal = next;
           // System.out.println("next=" + next);
            list.add(at);




        } while (next > 0);


        for (int i = list.size()-1; i >-1; i--) {
            code.append(String.valueOf(STUFFS[list.get(i).intValue()]));
        }
//        int resultlength=code.length();
//        StringBuilder head= new StringBuilder();
//        for(int i=0;i<length-resultlength;i++){
//            head.append(String.valueOf(STUFFS[0]));
//        }

      //  System.out.println("邀请码是" + head.toString()+code.toString());
        return code.toString();
    }

    public static void decode(String code) {
        char[] charts = code.toCharArray();
        int length = charts.length;
        Long count = 0L;

        for (int i = 1; i <= charts.length; i++) {
           // System.out.println(map.get(String.valueOf(charts[charts.length - i])) + "*" + (Math.pow(STUFFS.length, i - 1)));
            count += map.get(String.valueOf(charts[charts.length - i])) * (long) (Math.pow(STUFFS.length, i - 1));
        }

      //  System.out.println("用户的ID是" + count);
    }

    private static int getMaxBit(long val) {
        int length2 = length;
        int value = 1;
        for (int i = 1; i <= length2; i++) {
            if (val + 1 > Math.pow(STUFFS.length, i)) {
                value = i + 1;
            }
        }
        return value;
    }


    public static void main(String[] args) {
        encode((65535L));
        decode("Adi");
    }
}
