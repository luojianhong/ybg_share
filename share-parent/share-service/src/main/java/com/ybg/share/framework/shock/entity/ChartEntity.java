package com.ybg.share.framework.shock.entity;

/**
 * @author Hugh.HYS
 * @date 2018/11/13
 */
public interface ChartEntity {
    void clearValues();
}
