package com.ybg.share.web.test;

import com.ybg.share.core.remoteapi.TestDubboService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//@RestController
public class TestDubboController {

    @Reference(version = "1.0.0")
    TestDubboService testDubboService;

    //127.0.0.1:8090/ybg_share/testDubbo
    @GetMapping("/testDubbo")
    public void testDubbo(){
        testDubboService.test();
    }
}
