package com.ybg.share.core.pbest.service;

import com.ybg.share.core.dbapi.entity.ShareStockDayK;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class BasePbestService {
    /**
     * 获取价格列表
     *
     * @param list
     * @return
     */
    public static List<BigDecimal> getBorder(List<ShareStockDayK> list) {
        BigDecimal max = BigDecimal.ZERO;
        //设置一个不可能的值，然后可以替换成真实的值
        BigDecimal min = new BigDecimal("1000000");
        for (ShareStockDayK shareStockDayK : list) {
            if (shareStockDayK.getMinPrice().compareTo(min) < 0) {
                min = shareStockDayK.getMinPrice();
            }
            if (shareStockDayK.getMaxPrice().compareTo(max) > 0) {
                max = shareStockDayK.getMaxPrice();
            }
        }

        return getPriceList(min, max);
    }

    /**
     * 递增列表
     *
     * @param minPrice
     * @param maxPrice
     * @return
     */
    public static List<BigDecimal> getPriceList(BigDecimal minPrice, BigDecimal maxPrice) {
        List<BigDecimal> list = new ArrayList<>();

        BigDecimal IncreasingInterval = getIncreasingInterval(minPrice, maxPrice);
        BigDecimal minPrice2 = minPrice;
        do {
            list.add(minPrice2);
            minPrice2 = minPrice2.add(IncreasingInterval);

        } while (minPrice2.compareTo(maxPrice) < 0);
        return list;
    }

    /**
     * 递增区间，如果最大最小值差大于100，则变为1元为最小递增值，否则则为0.01元，解决有些股票波动较大影响计算量
     */
    private static BigDecimal getIncreasingInterval(BigDecimal minPrice, BigDecimal maxPrice) {
        if (maxPrice.subtract(minPrice).compareTo(new BigDecimal("100")) > -1) {
            return new BigDecimal("1");
        }
        return new BigDecimal("0.01");
    }

}
