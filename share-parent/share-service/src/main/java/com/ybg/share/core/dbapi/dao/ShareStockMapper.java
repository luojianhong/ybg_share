package com.ybg.share.core.dbapi.dao;


import com.ybg.share.core.dbapi.dao.sql.ShareStockSQL;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.remoteapi.dto.StockPageQueryDTO;
import com.ybg.share.core.remoteapi.vo.ShareStockVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 股票信息 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
public interface ShareStockMapper extends BaseMapper<ShareStock> {

    @Insert("insert ignore into share_stock (`name`,`code`,`market`) values(#{name},#{code},#{market})")
    boolean saveIgnore(ShareStock shareStock);

    @Insert("insert replace into share_stock (`name`,`code`,`market`) values(#{name},#{code},#{market})")
    boolean saveReplace(ShareStock shareStock);

    @Update("${sql}")
    void initSystem(@Param("sql") String sql);

    @SelectProvider(type = ShareStockSQL.class, method = "listStock")
    List<ShareStockVO> listStock(@Param("dto") StockPageQueryDTO dto);

    @SelectProvider(type = ShareStockSQL.class, method = "countStock")
    Long countStock(@Param("dto") StockPageQueryDTO dto);
}
