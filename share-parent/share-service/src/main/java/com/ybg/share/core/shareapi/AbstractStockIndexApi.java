package com.ybg.share.core.shareapi;

import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import com.ybg.framework.dto.AbstractJob;
import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.ybg.share.core.dbapi.service.ShareStockIndexService;
import com.ybg.share.utils.ThreadUtil;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 获取大盘日K交易信息
 */
public abstract class AbstractStockIndexApi {
    @Autowired
    ShareStockIndexService shareStockIndexService;

    /**
     * 执行
     *
     * @return 是否执行
     */
    public Boolean execute() {
        String url = getUrl();
        String market = getMarket();
        saveToDB(url, market);
        return Boolean.TRUE;
    }

    /**
     * 获取拉取地址
     *
     * @return 地址
     */
    public abstract String getUrl();

    /**
     * 市场简称
     *
     * @return 市场
     */
    public abstract String getMarket();

    /**
     * 保存到数据库
     *
     * @param url    地址
     * @param market 市场
     */
    private void saveToDB(String url, String market) {
        List<ShareStockIndex> list = new ArrayList<>();
        try {
            CloseableHttpClient client = null;
            CloseableHttpResponse response = null;
            try {
                HttpGet httpGet = new HttpGet(url);
                client = HttpClients.createDefault();
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                BufferedReader in = new BufferedReader(new InputStreamReader(entity.getContent(), "GBK"));
                CsvData csvData = CsvUtil.getReader().read(in);
                List<CsvRow> rows = csvData.getRows();
                if (rows.size() == 1) {
                    return;
                }
                rows.remove(0);
                for (CsvRow row : rows) {
                    ShareStockIndex shareStockIndex = new ShareStockIndex();
                    int count = 0;
                    shareStockIndex.setRefDate(row.get(count++));
                    shareStockIndex.setCode(row.get(count++).substring(1));
                    shareStockIndex.setStockName(row.get(count++));
                    shareStockIndex.setClosePrice(toNum(row.get(count++)));
                    shareStockIndex.setMaxPrice(toNum(row.get(count++)));
                    shareStockIndex.setMinPrice(toNum(row.get(count++)));
                    shareStockIndex.setOpenPrice(toNum(row.get(count++)));
                    shareStockIndex.setBeforeClose(toNum(row.get(count++)));
                    shareStockIndex.setChangeAmount(toNum(row.get(count++)));
                    shareStockIndex.setChangeRange(toNum(row.get(count++)));
                    shareStockIndex.setTradeNum(toNum(row.get(count++)).longValue());
                    shareStockIndex.setTradeMoney(toNum(row.get(count++)));
                    shareStockIndex.setMarket(market);
                    list.add(shareStockIndex);
                }

                for (ShareStockIndex shareStockIndex : list) {
//                    ThreadUtil.execute(() -> {
//                        shareStockIndexService.insertIgnore(shareStockIndex);
//
//                    });

                    ThreadUtil.execute(new AbstractJob(shareStockIndex) {
                        @Override
                        public void execute() {
                            ShareStockIndex shareStockIndex = (ShareStockIndex) getParams();
                            shareStockIndexService.insertIgnore(shareStockIndex);
                        }
                    });

                }
            } finally {
                if (response != null) {
                    response.close();
                }
                if (client != null) {
                    client.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private BigDecimal toNum(String value) {
        try {
            return new BigDecimal(value);
        } catch (Exception e) {

        }
        return BigDecimal.ZERO;

    }
}
