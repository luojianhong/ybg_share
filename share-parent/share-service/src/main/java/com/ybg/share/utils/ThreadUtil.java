package com.ybg.share.utils;


import com.ybg.framework.dto.AbstractJob;

import java.util.concurrent.*;

/**
 * 线程池定义
 */
public class ThreadUtil {
    /**
     * 线程数
     */
    public static final int THREAD_NUM = 4000;
    //acquire() //申请获取一个许可证，如果没有许可证，就阻塞直到能够获取或者被打断
    //availablePermits() // 返回当前有多少个有用的许可证数量hasQueuedThreads()//查询是否有线程正在等待获取许可证
    //drainPermits()//获得并返回所有立即可用的许可证数量
    //getQueuedThreads()//返回一个List包含当前可能正在阻塞队列里面所有线程对象
    //getQueueLength()//返回当前可能在阻塞获取许可证线程的数量
    //hasQueuedThreads()//查询是否有线程正在等待获取许可证
    //isFair()//返回是否为公平模式
    //reducePermits(int reduction)//减少指定数量的许可证
    //reducePermits(int reduction)//释放一个许可证
    //release(int permits)//释放指定数量的许可证
    //tryAcquire()//非阻塞的获取一个许可证
    public static final Semaphore THREAD_SEMAPHORE = new Semaphore(THREAD_NUM, false);
    private static ExecutorService executorDayKThreadPool = new ThreadPoolExecutor(THREAD_NUM, THREAD_NUM,
            0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(THREAD_NUM), new ThreadPoolExecutor.CallerRunsPolicy());

    private ThreadUtil() {

    }

    /**
     * 执行线程（不推荐使用，量太大会出现队列满问题）
     *
     * @param runnable
     */
    @Deprecated
    public static void execute(Runnable runnable) {
        try {
            executorDayKThreadPool.submit(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 执行线程
     *
     * @param job
     */
    public static void execute(AbstractJob job) {
        executorDayKThreadPool.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    THREAD_SEMAPHORE.acquire();
                    job.execute();
                    THREAD_SEMAPHORE.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });


    }


}
