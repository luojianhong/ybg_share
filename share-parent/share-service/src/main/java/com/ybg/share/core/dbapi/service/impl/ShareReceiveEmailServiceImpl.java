package com.ybg.share.core.dbapi.service.impl;

import com.ybg.share.core.dbapi.entity.ShareReceiveEmail;
import com.ybg.share.core.dbapi.dao.ShareReceiveEmailMapper;
import com.ybg.share.core.dbapi.service.ShareReceiveEmailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 会员接收邮箱的列表 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-12-30
 */
//@Service(version = "1.0.0", interfaceClass = ShareReceiveEmailService.class)
@Service
public class ShareReceiveEmailServiceImpl extends ServiceImpl<ShareReceiveEmailMapper, ShareReceiveEmail> implements ShareReceiveEmailService {

}
