package com.ybg.share.core.dbapi.service.impl;

import com.ybg.share.core.dbapi.entity.ShareStockEmailData;
import com.ybg.share.core.dbapi.dao.ShareStockEmailDataMapper;
import com.ybg.share.core.dbapi.service.ShareStockEmailDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2020-01-06
 */
//@Service(version = "1.0.0", interfaceClass = ShareStockEmailDataService.class)
@Service
public class ShareStockEmailDataServiceImpl extends ServiceImpl<ShareStockEmailDataMapper, ShareStockEmailData> implements ShareStockEmailDataService {

}
