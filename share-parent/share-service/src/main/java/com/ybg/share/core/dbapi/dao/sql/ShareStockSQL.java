package com.ybg.share.core.dbapi.dao.sql;


import cn.hutool.core.util.StrUtil;
import com.alibaba.druid.sql.PagerUtils;
import com.alibaba.druid.util.JdbcConstants;
import com.ybg.share.core.remoteapi.dto.StockPageQueryDTO;
import org.apache.ibatis.annotations.Param;


public class ShareStockSQL {

    public String listStock(@Param("dto") StockPageQueryDTO dto) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT id,name,code,market,status,spell FROM share_stock WHERE 1=1 ");

        if (StrUtil.isNotBlank(dto.getCode())) {
            sql.append(" AND code LIKE CONCAT (#{dto.code},'%')");
        }
        if (StrUtil.isNotBlank(dto.getName())) {
            sql.append(" AND name LIKE CONCAT (#{dto.name},'%')");
        }
        if (StrUtil.isNotBlank(dto.getMarket())) {
            sql.append(" AND market = #{dto.market}");
        }
        if (StrUtil.isNotBlank(dto.getPinyin())) {
            sql.append(" AND spell LIKE CONCAT (#{dto.pinyin},'%')");
        }
        if (dto.getState() != null) {
            sql.append(" AND status = #{dto.state}");
        }
        if (dto.getId() != null) {
            sql.append(" AND id = #{dto.id}");
        }

        return PagerUtils.limit(sql.toString(), JdbcConstants.MYSQL,dto.getOffset().intValue(),dto.getSize().intValue());
    }

    public String countStock(@Param("dto") StockPageQueryDTO dto) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT id,name,code,market,status,spell FROM share_stock  WHERE 1=1 ");

        if (StrUtil.isNotBlank(dto.getCode())) {
            sql.append(" AND code LIKE CONCAT (#{dto.code},'%')");
        }
        if (StrUtil.isNotBlank(dto.getName())) {
            sql.append(" AND name LIKE CONCAT (#{dto.name},'%')");
        }
        if (StrUtil.isNotBlank(dto.getMarket())) {
            sql.append(" AND market = #{dto.market}");
        }
        if (StrUtil.isNotBlank(dto.getPinyin())) {
            sql.append(" AND spell LIKE CONCAT (#{dto.pinyin},'%')");
        }
        if (dto.getState() != null) {
            sql.append(" AND status = #{dto.state}");
        }
        if (dto.getId() != null) {
            sql.append(" AND id = #{dto.id}");
        }
        return PagerUtils.count(sql.toString(), JdbcConstants.MYSQL);

    }
}
