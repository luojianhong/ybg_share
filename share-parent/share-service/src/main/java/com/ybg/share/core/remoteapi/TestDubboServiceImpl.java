package com.ybg.share.core.remoteapi;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
@Slf4j
//Service一定要使用apache.dubbo的注解，alibaba和sprig的都不要用
@Service(version = "${dubbo.provider.version}")
public class TestDubboServiceImpl implements TestDubboService {
    @Override
    public void test() {
        log.info("测试dubbo");
        System.out.println("测试dubbo");
    }
}
