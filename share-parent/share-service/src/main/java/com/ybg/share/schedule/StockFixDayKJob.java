package com.ybg.share.schedule;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.framework.core.zookeeper.DefaultZkLockConfig;
import com.ybg.framework.core.zookeeper.ZkDistributedLock;
import com.ybg.framework.enums.MarketEnum;
import com.ybg.framework.enums.SyncPolicyEnum;
import com.ybg.share.core.dbapi.entity.ShareFixDaykTask;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareFixDaykTaskService;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.core.shareapi.impl.ShanghaiStockDayKApi;
import com.ybg.share.core.shareapi.impl.ShanghaiStockIndexApi;
import com.ybg.share.core.shareapi.impl.ShenzhenStockDayKApi;
import com.ybg.share.core.shareapi.impl.ShenzhenStockIndexApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 定时定量修复日K数据，每天拿固定数量的个股来修复，防止真的丢失数据（拉取日K是增量的，旧的数据没有再去检测）
 */
@Component
@Slf4j
public class StockFixDayKJob {

    @Autowired
    private ShareFixDaykTaskService shareFixDaykTaskService;

    @Autowired
    private ShareStockService shareStockService;

    @Autowired
    private ShenzhenStockDayKApi shenzhenStockDayKApi;
    @Autowired
    private ShanghaiStockDayKApi shanghaiStockDayKApi;
    @Autowired
    private DefaultZkLockConfig defaultZkLockConfig;
    @Autowired
    private ShanghaiStockIndexApi shanghaiStockIndexApi;
    @Autowired
    private ShenzhenStockIndexApi shenZhenStockIndexApi;


    @Scheduled(cron = "0 30 20 * * ?")
    public void fixDayK() {
        ZkDistributedLock lock = new ZkDistributedLock(defaultZkLockConfig, "fixDayK");
        boolean trylock = lock.tryLock();
        if (!trylock) {
            return;
        }
        try {
           //忽略性插入
            shareFixDaykTaskService.initList();
            QueryWrapper<ShareFixDaykTask> wrapper = new QueryWrapper<>();
            wrapper.orderByAsc(ShareFixDaykTask.TIME);
            wrapper.last("LIMIT 20");
            List<ShareFixDaykTask> list = shareFixDaykTaskService.list(wrapper);
            for (ShareFixDaykTask shareFixDaykTask : list) {
                ShareStock shareStock = shareStockService.getById(shareFixDaykTask.getStockId());
                if (shareStock.getMarket().equals(MarketEnum.sh.getCode())) {
                    shanghaiStockDayKApi.execute(shareStock, SyncPolicyEnum.all_ignore);
                }
                if (shareStock.getMarket().equals(MarketEnum.sz.getCode())) {
                    shenzhenStockDayKApi.execute(shareStock, SyncPolicyEnum.all_ignore);
                }
                ShareFixDaykTask task = new ShareFixDaykTask();
                task.setTime(shareFixDaykTask.getTime() + 1);
                task.setId(shareFixDaykTask.getId());
                shareFixDaykTaskService.updateById(task);
            }
        } catch (Exception e) {
        log.info("error{}",e);
        }finally {
            lock.unlock();
        }


    }


    /**
     * 更新大盘
     */
    @Scheduled(cron = "0 30 3 * * ?")
    public void updateStockIndex() {

        ZkDistributedLock lock = new ZkDistributedLock(defaultZkLockConfig, "updateStockIndex");
        boolean trylock = lock.tryLock();
        if (!trylock) {
            return;
        }
        try {
            shanghaiStockIndexApi.execute();
            shenZhenStockIndexApi.execute();
        } catch (Exception e) {
            log.info("error{}", e);
        } finally {
            lock.unlock();
        }

    }

    /**
     * 拉取日K
     */
    @Scheduled(cron = "0 30 4 * * ?")
    public void updateStockDayK() {
        ZkDistributedLock lock = new ZkDistributedLock(defaultZkLockConfig, "updateStockDayK");
        boolean trylock = lock.tryLock();
        if (!trylock) {
            return;
        }
        try {

            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq(ShareStock.STATUS, 1);
            List<ShareStock> list = shareStockService.list(queryWrapper);
            for (ShareStock shareStock : list) {
                if (shareStock.getMarket().equals(MarketEnum.sh.getCode())) {
                    shanghaiStockDayKApi.execute(shareStock, SyncPolicyEnum.increment);
                }
                if (shareStock.getMarket().equals(MarketEnum.sz.getCode())) {

                    shenzhenStockDayKApi.execute(shareStock, SyncPolicyEnum.increment);
                }
            }
        } catch (Exception e) {
            log.info("error{}", e);
        } finally {
            lock.unlock();
        }
    }

}
