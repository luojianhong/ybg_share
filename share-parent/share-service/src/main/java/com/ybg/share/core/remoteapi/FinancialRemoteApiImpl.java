package com.ybg.share.core.remoteapi;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.framework.vo.R;
import com.ybg.share.core.dbapi.entity.ShareFinancialQuarter;
import com.ybg.share.core.dbapi.entity.ShareFinancialYear;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareFinancialQuarterService;
import com.ybg.share.core.dbapi.service.ShareFinancialYearService;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.core.remoteapi.datamapper.StockFinancialDataMapper;
import com.ybg.share.core.remoteapi.dto.FinancialDTO;
import com.ybg.share.core.remoteapi.vo.FinancialVO;
import com.ybg.share.core.shareapi.impl.FinancialQuarterApi;
import com.ybg.share.core.shareapi.impl.FinancialYearApi;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(version = "${dubbo.provider.version}")
public class FinancialRemoteApiImpl implements FinancialRemoteApi {
    @Autowired
    private ShareFinancialQuarterService shareFinancialQuarterService;
    @Autowired
    private ShareFinancialYearService shareFinancialYearService;
    @Autowired
    private FinancialQuarterApi financialQuarterApi;
    @Autowired
    private FinancialYearApi financialYearApi;
    @Autowired
    private ShareStockService shareStockService;

    @Override
    public FinancialVO getYearFinancialInfo(Long stockId, String year) {
        QueryWrapper<ShareFinancialYear> wrapper = new QueryWrapper<>();
        wrapper.eq(ShareFinancialYear.STOCK_ID, stockId);
        wrapper.likeLeft(ShareFinancialYear.REPORT_DATE, year);
        ShareFinancialYear bean = shareFinancialYearService.getOne(wrapper);
        return StockFinancialDataMapper.INSTANCE.toYearVO(bean);
    }

    @Override
    public List<FinancialVO> getAllMonthInfo(Long stockId, String year) {
        QueryWrapper<ShareFinancialQuarter> wrapper = new QueryWrapper<>();
        wrapper.eq(ShareFinancialQuarter.STOCK_ID, stockId);
        wrapper.likeLeft(ShareFinancialQuarter.REPORT_DATE, year);
        List<ShareFinancialQuarter> list = shareFinancialQuarterService.list(wrapper);
        return StockFinancialDataMapper.INSTANCE.toMonthVOList(list);
    }

    @Override
    public List<FinancialVO> getAllYearInfo(Long stockId) {
        QueryWrapper<ShareFinancialYear> wrapper = new QueryWrapper<>();
        wrapper.eq(ShareFinancialYear.STOCK_ID, stockId);
        List<ShareFinancialYear> list = shareFinancialYearService.list(wrapper);
        return StockFinancialDataMapper.INSTANCE.toYearVOList(list);
    }

    @Override
    public R syncMonthFinancialInfo(Long stockId) {
        ShareStock shareStock = shareStockService.getById(stockId);
        financialQuarterApi.execute(shareStock);
        return R.success();
    }

    @Override
    public R syncYearFinancialInfo(Long stockId) {
        ShareStock shareStock = shareStockService.getById(stockId);
        financialYearApi.execute(shareStock);
        return R.success();
    }

    @Override
    public R syncAllFinancial() {
        QueryWrapper<ShareStock> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(ShareStock.STATUS, 1);
        List<ShareStock> list = shareStockService.list(queryWrapper);
        for (ShareStock shareStock : list) {
            financialQuarterApi.execute(shareStock);
            financialYearApi.execute(shareStock);
        }
        return R.success();
    }

    @Override
    public Page<FinancialVO> getFinancialOfQuarterByPage(FinancialDTO dto) {
        QueryWrapper<FinancialVO> queryWrapper = new QueryWrapper<>();

        List<ShareFinancialQuarter> list = shareFinancialQuarterService.list();
        return null;
    }

    @Override
    public Page<FinancialVO> getFinancialOfYearByPage(FinancialDTO dto) {
        return null;
    }
}
