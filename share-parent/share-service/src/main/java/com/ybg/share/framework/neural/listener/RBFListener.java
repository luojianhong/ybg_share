package com.ybg.share.framework.neural.listener;

import lombok.extern.slf4j.Slf4j;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.nnet.learning.LMS;

@Slf4j
public class RBFListener implements LearningEventListener {
    @Override
    public void handleLearningEvent(LearningEvent learningEvent) {

        LMS lr = (LMS) learningEvent.getSource();
        log.info("{}", lr.getLearningRate() + "," + lr.getCurrentIteration());
        log.info(lr.getCurrentIteration() + ". iteration | Total network error: " + lr.getTotalNetworkError() + ",");
    }
}
