package com.ybg.share.core.dbapi.service.impl;

import com.ybg.share.core.dbapi.entity.ShareFixDaykTask;
import com.ybg.share.core.dbapi.dao.ShareFixDaykTaskMapper;
import com.ybg.share.core.dbapi.service.ShareFixDaykTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 日K自我修复任务 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-12-30
 */
//@Service(version = "1.0.0", interfaceClass = ShareFixDaykTaskService.class)
@Service
public class ShareFixDaykTaskServiceImpl extends ServiceImpl<ShareFixDaykTaskMapper, ShareFixDaykTask> implements ShareFixDaykTaskService {

    @Override
    public void initList() {
        baseMapper.initList(getMinTime());
    }

    @Override
    public int getMinTime() {
        return baseMapper.getMinTime();
    }
}
