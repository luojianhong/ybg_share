package com.ybg.share.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.framework.vo.R;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.core.shareapi.impl.EastMoneyStockInfoApi;
import com.ybg.share.core.shareapi.impl.FinancialQuarterApi;
import com.ybg.share.core.shareapi.impl.FinancialYearApi;
import com.ybg.share.core.shareapi.impl.ShanghaiStockDayKApi;
import com.ybg.share.core.shareapi.impl.ShanghaiStockIndexApi;
import com.ybg.share.core.shareapi.impl.ShenzhenStockDayKApi;
import com.ybg.share.core.shareapi.impl.ShenzhenStockIndexApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/*
 * 在swagger-annotations jar包中 1.5.X版本以上, 注解 io.swagger.annotations.API
 * 中的description被废弃了。新的swagger组件中使用了新的方法来对Web api 进行分组。原来使用 description ，
 * 默认一个Controller类中包含的方法构成一 个api分组。现在使用tag，可以更加方便的分组。
 * 比如把两个Controller类里的方法划分成同一个分组。tag的key用来区分不同的分组。tag的value用做分组的描述。
 * @ApiOperation 中value是api的简要说明，在界面api 链接的右侧，少于120个字符。
 * @ApiOperation 中notes是api的详细说明，需要点开api 链接才能看到。
 * @ApiOperation 中 produces 用来标记api返回值的具体类型。这里是json格式，utf8编码。
 */
@Api(tags = "loadData", description = "拉取外部数据到本地接口")
//@RestController
public class LoadDataController {


    @GetMapping("/api/test")
    public R test() {

        return new R<>(R.SUCCESS);
    }

    @Autowired
    EastMoneyStockInfoApi eastMoneyStockInfoApi;

    @ApiOperation("拉取股票列表（深圳和上海交易所）")
    @PostMapping("/api/loadStockInfoData")
    public R loadStockInfoData() {
        eastMoneyStockInfoApi.execute();
        return new R<>(R.SUCCESS);
    }

    @Autowired
    ShanghaiStockIndexApi shanghaiStockIndexApi;
    @Autowired
    ShenzhenStockIndexApi shenZhenStockIndexApi;

    @ApiOperation("大盘日K数据（全量）")
    @PostMapping("/api/loadCompositeIndexData")
    public R loadCompositeIndexData() {
        //todo 加入分布式锁 防止重复点击
        shanghaiStockIndexApi.execute();
        shenZhenStockIndexApi.execute();
        return new R();
    }

    @Autowired
    ShanghaiStockDayKApi shanghaiStockDayKApi;
    @Autowired
    ShenzhenStockDayKApi shenzhenStockDayKApi;
    @Autowired
    ShareStockService shareStockService;


    //127.0.0.1:8090/ybg_share/api/loadStockDayKData
    @ApiOperation("日K数据(增量)")
    @PostMapping("/api/loadStockDayKData")
    public R loadStockDayKData() {



        return new R();
    }


    @ApiOperation("全量拉取（强制更新，开始时间和市场开始时间一致）")
    @PostMapping("/api/rewriteAllDayK")
    public R rewriteAllDayK() {
        //todo 加入分布式锁 防止重复点击
        //todo 执行代码
        //todo 执行完后释放锁
        return new R();
    }


    @ApiOperation("指定股票拉取（全量，开始时间和市场开始时间一致）")
    @PostMapping("/api/rewriteOneStockDayK")
    public R rewriteOneStockDayK(@RequestParam() String code) {
        System.out.println(code);
        //todo 加入分布式锁 防止重复点击
        //todo 执行代码
        //todo 执行完后释放锁
        return new R();
    }

    @Autowired
    FinancialQuarterApi financialQuarterApi;
    @Autowired
    FinancialYearApi financialYearApi;

    //127.0.0.1:8090/ybg_share/api/loadFinancialQuarterData
    @ApiOperation("基本财务报表数据拉取")
    @PostMapping("/api/loadFinancialQuarterData")
    public R loadFinancialQuarterData() {
        //todo 加入分布式锁 防止重复点击
        QueryWrapper<ShareStock> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(ShareStock.STATUS, 1);
        List<ShareStock> list = shareStockService.list(queryWrapper);
        for (ShareStock shareStock : list) {
            financialQuarterApi.execute(shareStock);
            financialYearApi.execute(shareStock);
        }

        return new R();
    }


}
