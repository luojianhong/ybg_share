package com.ybg.share.core.remoteapi.datamapper;

import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.remoteapi.vo.StockDayKVO;
import org.mapstruct.factory.Mappers;

import java.util.List;

public interface StockDayKDataMapper {
    StockDayKDataMapper INSTANCE= Mappers.getMapper(StockDayKDataMapper.class);
    StockDayKVO toVO(ShareStockDayK bean);
    List<StockDayKVO> toVOList(List<ShareStockDayK> list);
}
