package com.ybg.share.core.dbapi.service.impl;

import com.ybg.share.core.dbapi.service.ShareStockCountMonthService;
import com.ybg.framework.dto.AbstractJob;
import com.ybg.share.config.SystemInit;
import com.ybg.share.core.dbapi.entity.ShareStockCountMonth;
import com.ybg.share.core.dbapi.dao.ShareStockCountMonthMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.share.utils.ThreadUtil;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 个股月度统计 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-31
 */
//@Service(version = "1.0.0", interfaceClass = ShareStockCountMonthService.class)
@Service
public class ShareStockCountMonthServiceImpl extends ServiceImpl<ShareStockCountMonthMapper, ShareStockCountMonth> implements ShareStockCountMonthService {

    @Override
    public void initTable() {
//        ThreadUtil.execute(()->{
//            for (int i = 0; i < SystemInit.SHARE_STOCK_COUNT_MONTH_TABLESHARDING; i++) {
//                baseMapper.initTable(i);
//            }
//        });

        ThreadUtil.execute(new AbstractJob(null) {
            @Override
            public void execute() {

                for (int i = 0; i < SystemInit.SHARE_STOCK_COUNT_MONTH_TABLESHARDING; i++) {
                    baseMapper.initTable(i);
                }
            }
        });
    }

    @Override
    public int saveReplace(ShareStockCountMonth entity) {
        return baseMapper.saveReplace(entity);
    }
}
