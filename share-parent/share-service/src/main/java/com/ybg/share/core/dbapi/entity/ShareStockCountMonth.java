package com.ybg.share.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 个股月度统计
 * </p>
 *
 * @author yanyu
 * @since 2019-10-31
 */
@ApiModel(value = "个股月度统计")
@TableName("share_stock_count_month")
public class ShareStockCountMonth extends Model<ShareStockCountMonth> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 股票ID
     */
    @ApiModelProperty(value = "股票ID")
    private Long stockId;
    /**
     * 日期
     */
    @ApiModelProperty(value = "日期")
    private String refDate;
    /**
     * 股票代码
     */
    @ApiModelProperty(value = "股票代码")
    private String code;
    /**
     * 市场
     */
    @ApiModelProperty(value = "市场")
    private String market;
    /**
     * 上涨次数
     */
    @ApiModelProperty(value = "上涨次数")
    private Integer upTimes;
    /**
     * 下跌次数
     */
    @ApiModelProperty(value = "下跌次数")
    private Integer downTimes;
    /**
     * 最高价
     */
    @ApiModelProperty(value = "最高价")
    private BigDecimal topPrice;
    /**
     * 最低价
     */
    @ApiModelProperty(value = "最低价")
    private BigDecimal lowPrice;
    /**
     * 最高价日期
     */
    @ApiModelProperty(value = "最高价日期")
    private String topDate;
    /**
     * 最低价日期
     */
    @ApiModelProperty(value = "最低价日期")
    private String lowDate;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取股票ID
     */
    public Long getStockId() {
        return stockId;
    }

    /**
     * 设置股票ID
     */

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    /**
     * 获取日期
     */
    public String getRefDate() {
        return refDate;
    }

    /**
     * 设置日期
     */

    public void setRefDate(String refDate) {
        this.refDate = refDate;
    }

    /**
     * 获取股票代码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置股票代码
     */

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取市场
     */
    public String getMarket() {
        return market;
    }

    /**
     * 设置市场
     */

    public void setMarket(String market) {
        this.market = market;
    }

    /**
     * 获取上涨次数
     */
    public Integer getUpTimes() {
        return upTimes;
    }

    /**
     * 设置上涨次数
     */

    public void setUpTimes(Integer upTimes) {
        this.upTimes = upTimes;
    }

    /**
     * 获取下跌次数
     */
    public Integer getDownTimes() {
        return downTimes;
    }

    /**
     * 设置下跌次数
     */

    public void setDownTimes(Integer downTimes) {
        this.downTimes = downTimes;
    }

    /**
     * 获取最高价
     */
    public BigDecimal getTopPrice() {
        return topPrice;
    }

    /**
     * 设置最高价
     */

    public void setTopPrice(BigDecimal topPrice) {
        this.topPrice = topPrice;
    }

    /**
     * 获取最低价
     */
    public BigDecimal getLowPrice() {
        return lowPrice;
    }

    /**
     * 设置最低价
     */

    public void setLowPrice(BigDecimal lowPrice) {
        this.lowPrice = lowPrice;
    }

    /**
     * 获取最高价日期
     */
    public String getTopDate() {
        return topDate;
    }

    /**
     * 设置最高价日期
     */

    public void setTopDate(String topDate) {
        this.topDate = topDate;
    }

    /**
     * 获取最低价日期
     */
    public String getLowDate() {
        return lowDate;
    }

    /**
     * 设置最低价日期
     */

    public void setLowDate(String lowDate) {
        this.lowDate = lowDate;
    }

    /**
     * 获取创建时间
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     */

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 股票ID列的数据库字段名称
     */
    public static final String STOCK_ID = "stock_id";

    /**
     * 日期列的数据库字段名称
     */
    public static final String REF_DATE = "ref_date";

    /**
     * 股票代码列的数据库字段名称
     */
    public static final String CODE = "code";

    /**
     * 市场列的数据库字段名称
     */
    public static final String MARKET = "market";

    /**
     * 上涨次数列的数据库字段名称
     */
    public static final String UP_TIMES = "up_times";

    /**
     * 下跌次数列的数据库字段名称
     */
    public static final String DOWN_TIMES = "down_times";

    /**
     * 最高价列的数据库字段名称
     */
    public static final String TOP_PRICE = "top_price";

    /**
     * 最低价列的数据库字段名称
     */
    public static final String LOW_PRICE = "low_price";

    /**
     * 最高价日期列的数据库字段名称
     */
    public static final String TOP_DATE = "top_date";

    /**
     * 最低价日期列的数据库字段名称
     */
    public static final String LOW_DATE = "low_date";

    /**
     * 创建时间列的数据库字段名称
     */
    public static final String CREATE_TIME = "create_time";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareStockCountMonth{" +
                "id=" + id +
                ", stockId=" + stockId +
                ", refDate=" + refDate +
                ", code=" + code +
                ", market=" + market +
                ", upTimes=" + upTimes +
                ", downTimes=" + downTimes +
                ", topPrice=" + topPrice +
                ", lowPrice=" + lowPrice +
                ", topDate=" + topDate +
                ", lowDate=" + lowDate +
                ", createTime=" + createTime +
                "}";
    }
}
