package com.ybg.share.framework.shock.entity;

import com.ybg.share.framework.shock.entity.base.*;

import java.util.ArrayList;
import java.util.List;

public class VolEntity implements ChartEntity {

    public List<BarEntry> bars;

    public VolEntity() {
        this.bars = new ArrayList<>();
    }

    @Override
    public void clearValues() {
        if (bars != null) {
            bars.clear();
        }
    }
}
