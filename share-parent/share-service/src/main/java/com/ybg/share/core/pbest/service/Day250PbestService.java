package com.ybg.share.core.pbest.service;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.framework.enums.PbestEnum;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.dbapi.entity.ShareStockPbest;
import com.ybg.share.core.dbapi.service.ShareStockDayKService;
import com.ybg.share.core.dbapi.service.ShareStockPbestService;
import com.ybg.share.core.pbest.dto.PbestDTO;
import com.ybg.share.core.pbest.vo.PbestVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * 250天个交易日（最近一年最优买卖点算法）
 */
@Service
public class Day250PbestService extends BasePbestService {
    /**
     * 按照年份进行计算
     *
     * @param shareStock
     * @param year 年份
     */
    @Autowired
    private ShareStockDayKService shareStockDayKService;
    @Autowired
    private ShareStockPbestService shareShockPbestService;


    public PbestVO dat250Pbest(ShareStock shareStock) {
        String endDate = DateUtil.today();
        String startDate = DateUtil.formatDate(DateUtil.offsetDay(new Date(), -365));
        QueryWrapper<ShareStockDayK> wrapper = new QueryWrapper<>();
        wrapper.eq(ShareStockDayK.STOCK_ID, shareStock.getId());
        wrapper.ge(ShareStockDayK.REF_DATE, startDate);
        wrapper.le(ShareStockDayK.REF_DATE, endDate);
        wrapper.orderByAsc(ShareStockDayK.REF_DATE);
        BigDecimal maxProfit = BigDecimal.ZERO;
        PbestDTO maxDTO = null;
        List<ShareStockDayK> list = shareStockDayKService.list(wrapper);
        List<BigDecimal> buylist = getBorder(list);
        List<BigDecimal> selllist = getBorder(list);
        for (BigDecimal buy : buylist) {
            for (BigDecimal sell : selllist) {
                if (buy.compareTo(sell) == -1) {
                    PbestDTO dto = new PbestDTO(buy, sell, list);
                    BigDecimal profit = dto.getPbestOutPut();
                    if (profit.compareTo(maxProfit) > 0) {
                        maxProfit = profit;
                        maxDTO = dto;
                    }
                }
            }
        }
        if (maxDTO == null) {
            return null;
        }
        PbestVO vo = new PbestVO();
        vo.setStockId(shareStock.getId());
        vo.setRefYear(DateUtil.thisYear());
        vo.setBuyPrice(maxDTO.getBuyPrice());
        vo.setSellPrice(maxDTO.getSellPrice());
        vo.setPbestName(PbestEnum.DAY250.name());
        vo.setUpdateTime(LocalDateTime.now());
        vo.setCreateTime(LocalDateTime.now());
        vo.setTimes(maxProfit.divide(maxDTO.getSellPrice().subtract(maxDTO.getBuyPrice())).intValue());
        vo.setProfit(maxProfit);
        vo.setProfitRate(maxProfit.divide(maxDTO.getBuyPrice(), 2, RoundingMode.DOWN));
        vo.setStockCode(shareStock.getCode());
        vo.setStockName(shareStock.getName());
        vo.setMarket(shareStock.getMarket());
        saveToDB(vo);
        return vo;
    }

    public void saveToDB(PbestVO vo) {
        ShareStockPbest entity = new ShareStockPbest();
        BeanUtils.copyProperties(vo, entity);
        shareShockPbestService.saveReplace(entity);
    }


}
