package com.ybg.share.core.dbapi.dao.sql;

import com.ybg.share.core.dbapi.dao.ShareStockCountMonthMapper;
import com.ybg.share.core.dbapi.entity.ShareStockCountMonth;
import org.apache.ibatis.annotations.Param;

/**
 * {@link ShareStockCountMonthMapper}
 */
public class ShareStockCountMonthSQL {
    public String saveReplace(@Param("e") ShareStockCountMonth entity) {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO share_stock_count_month(stock_id,ref_date,code,market,up_times,down_times,top_price,low_price,top_date,low_date,create_time)");
        sql.append(" VALUES(#{e.stockId},#{e.refDate},#{e.code},#{e.market},#{e.upTimes},#{e.downTimes},#{e.topPrice},#{e.lowPrice},#{e.topDate},#{e.lowDate},#{e.createTime})");
        sql.append(" ON DUPLICATE KEY UPDATE ref_date=VALUES( ref_date) , code=VALUES( code),market=VALUES( market),up_times=VALUES( up_times),down_times=VALUES( down_times),top_price=VALUES( top_price),low_price=VALUES( low_price),top_date=VALUES( top_date),low_date=VALUES( low_date),create_time=VALUES( create_time)");

        return sql.toString();
    }


    public String initTable(@Param("i") int i) {
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLE IF NOT EXISTS `share_stock_count_month" + i + "` ( " +
                "  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键', " +
                "  `stock_id` bigint(20) NOT NULL COMMENT '股票ID', " +
                "  `ref_date` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '日期', " +
                "  `code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票代码', " +
                "  `market` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '市场', " +
                "  `up_times` int(3) DEFAULT NULL COMMENT '上涨次数', " +
                "  `down_times` int(3) DEFAULT NULL COMMENT '下跌次数', " +
                "  `top_price` decimal(10,2) DEFAULT NULL COMMENT '最高价', " +
                "  `low_price` decimal(10,2) DEFAULT NULL COMMENT '最低价', " +
                "  `top_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最高价日期', " +
                "  `low_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最低价日期', " +
                "  `create_time` datetime DEFAULT NULL COMMENT '创建时间', " +
                "  PRIMARY KEY (`id`), " +
                "  UNIQUE KEY `uni_index` (`stock_id`,`ref_date`) USING BTREE " +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='个股月度统计'; ");
        return sql.toString();


    }

}
