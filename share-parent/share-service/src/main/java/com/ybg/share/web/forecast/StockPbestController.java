package com.ybg.share.web.forecast;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpResponse;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.framework.vo.R;
import com.ybg.share.core.dbapi.entity.ShareReceiveEmail;
import com.ybg.share.core.dbapi.entity.ShareStockEmailData;
import com.ybg.share.core.dbapi.service.ShareReceiveEmailService;
import com.ybg.share.core.dbapi.service.ShareStockEmailDataService;
import com.ybg.share.core.pbest.vo.PbestEmailVO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/download/")
public class StockPbestController {

    @Autowired
    ShareStockEmailDataService shareStockEmailDataService;
    @Autowired
    ShareReceiveEmailService receiveEmailService;

    ///api/download/download
    @RequestMapping("download")
    public void download(HttpServletResponse response,String email) throws  Exception{
        if(StrUtil.isBlank(email)){
            return;
        }
        QueryWrapper<ShareReceiveEmail> wrapper= new QueryWrapper<>();
        wrapper.eq(ShareReceiveEmail.EMAIL,email);

        ShareReceiveEmail shareReceiveEmail = receiveEmailService.getOne(wrapper);
        if(shareReceiveEmail==null){
            return ;
        }
        if(shareReceiveEmail.getEndTime().isBefore(LocalDateTime.now())){
            return ;
        }

        List<ShareStockEmailData> list = shareStockEmailDataService.list();
        List<PbestEmailVO> exportData= new ArrayList<>();
        for (ShareStockEmailData shareStockEmailData : list) {
            PbestEmailVO bean = new PbestEmailVO();
            BeanUtils.copyProperties(shareStockEmailData,bean);
            exportData.add(bean);
        }
        if (!CollectionUtils.isEmpty(exportData)) {
            Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("Ybg_share 数据列表", "Sheet1"), PbestEmailVO.class, exportData);
            try {
                response.setCharacterEncoding("UTF-8");
                response.setHeader("content-Type", "application/vnd.ms-excel");
                response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("Ybg_share 数据列表.xls", "UTF-8"));
                workbook.write(response.getOutputStream());
                workbook.close();
            } catch (IOException e) {
                response.getWriter().println("发生错误");
                return;
            }
        } else {
            response.getWriter().println("无数据");
        }


    }
}
