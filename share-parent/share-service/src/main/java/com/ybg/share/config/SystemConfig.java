package com.ybg.share.config;

import com.ybg.framework.core.NetasetSendEmail;
import com.ybg.framework.core.zookeeper.DefaultZkLockConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Slf4j
@Configuration
public class SystemConfig {
//    email.netaset.account

    /**
     * 网易邮箱配置
     *
     * @param account
     * @param password
     * @return
     */
    @Bean
    public NetasetSendEmail netasetSendEmail(
            @Value("${email.netaset.account}") String account,
            @Value("${email.netaset.password}") String password) {
        log.info("系统邮箱："+account);
        return new NetasetSendEmail(account, password);
    }

    /**
     * zk分布式锁
     * @param zkAddr
     * @param basePath
     * @return
     */
    @Bean
    public DefaultZkLockConfig zkLockConfig(@Value("${dubbo.registry.address}") String zkAddr
            , @Value("${spring.application.name}") String basePath) {
        DefaultZkLockConfig config = new DefaultZkLockConfig();
        config.setZkServers(zkAddr.replace("zookeeper://",""));
        config.setBasePath("/"+basePath);
        log.info("zk锁："+zkAddr+ ",basePath="+basePath);
        //    ZkDistributedLock lock = new ZkDistributedLock(config,"lockKey");
//        lock.lock();
//        try{
//            //这里放业务代码;
//        }catch(Exception ex){
//            //异常处理
//        }finally{
//            lock.unlock();
//        }
        return config;
    }




}
