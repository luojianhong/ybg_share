package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareFixDaykTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 日K自我修复任务 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-12-30
 */
public interface ShareFixDaykTaskMapper extends BaseMapper<ShareFixDaykTask> {

    @Insert("insert ignore into share_fix_dayk_task(stock_id,time,update_time)   select id,${minTime},now() from share_stock where status=1")
    void initList(@Param("minTime") int minTime);

    @Select("SELECT ifnull(MIN(time),0) FROM share_fix_dayk_task")
    int getMinTime();
}
