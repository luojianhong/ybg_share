CREATE TABLE IF NOT EXISTS `share_stock_index`
(
    `id`            bigint(20) NOT NULL AUTO_INCREMENT COMMENT ' 主键 ',
    `ref_date`      varchar(20)    DEFAULT NULL COMMENT ' 日期，2019-10-16 ',
    `code`          varchar(20)    DEFAULT NULL COMMENT ' 股票代码 ',
    `market`        varchar(20)    DEFAULT NULL COMMENT ' 所属市场 ',
    `stock_name`    varchar(60)    DEFAULT NULL COMMENT ' 名称 ',
    `close_price`   decimal(10, 4) DEFAULT NULL COMMENT ' 收盘价 ',
    `max_price`     decimal(10, 4) DEFAULT NULL COMMENT ' 最高价 ',
    `min_price`     decimal(10, 4) DEFAULT NULL COMMENT ' 最低价 ',
    `open_price`    decimal(10, 4) DEFAULT NULL COMMENT ' 开盘价 ',
    `before_close`  decimal(10, 4) DEFAULT NULL COMMENT ' 前收盘 ',
    `change_amount` decimal(10, 4) DEFAULT NULL COMMENT ' 涨跌额 ',
    `change_range`  decimal(10, 4) DEFAULT NULL COMMENT ' 涨跌幅 ',
    `trade_num`     bigint(20)     DEFAULT NULL COMMENT ' 成交量 ',
    `trade_money`   decimal(20, 4) DEFAULT NULL COMMENT ' 成交金额 ',
    PRIMARY KEY (`id`),
    UNIQUE `only` USING BTREE (`ref_date`, `code`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 0
  CHARSET = utf8mb4 COMMENT '上证指数和深证指数';
CREATE TABLE IF NOT EXISTS `share_stock`
(
    `id`     bigint(20)  NOT NULL AUTO_INCREMENT COMMENT ' 主键 ',
    `name`   varchar(60) NOT NULL COMMENT ' 股票名称 ',
    `code`   varchar(10) NOT NULL COMMENT ' 1 可用，0禁用 ',
    `market` varchar(10) NOT NULL COMMENT ' 所属市场 ',
    `status` tinyint(1)                    DEFAULT ' 1 ' COMMENT ' 1 可用 0不可用 ',
    `spell`  varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT ' 股票拼音 ',
    PRIMARY KEY (`id`),
    UNIQUE `code` (`code`),
    KEY `name` (`name`),
    KEY `spell` (`spell`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 0
  CHARSET = utf8mb4 COMMENT '股票信息';
CREATE TABLE IF NOT EXISTS `share_financial_quarter`
(
    `id`                     bigint(20)                     NOT NULL AUTO_INCREMENT COMMENT ' 主键 ',
    `report_date`            varchar(30) CHARACTER SET utf8 NOT NULL COMMENT ' 报告月份 格式yyyy-MM-dd ',
    `stock_id`               bigint(20)                     NOT NULL COMMENT ' 个股ID ',
    `code`                   varchar(10) CHARACTER SET utf8 NOT NULL COMMENT ' 股票代码 ',
    `basic_eps`              decimal(20, 3)                DEFAULT NULL COMMENT ' 基本每股收益(元)',
    `eps`                    decimal(20, 3)                DEFAULT NULL COMMENT ' 每股净资产(元)',
    `cash_flow`              varchar(20)                   DEFAULT NULL COMMENT ' 每股经营活动产生的现金流量净额(元)',
    `revenue`                decimal(20, 3)                DEFAULT NULL COMMENT ' 主营业务收入(万元)',
    `profitability`          decimal(20, 3)                DEFAULT NULL COMMENT ' 主营业务利润(万元)',
    `operating_profit`       decimal(20, 3)                DEFAULT NULL COMMENT ' 营业利润(万元)',
    `investment_return`      decimal(20, 3)                DEFAULT NULL COMMENT ' 投资收益 ',
    `non_operating_income`   decimal(20, 3)                DEFAULT NULL COMMENT ' 营业外收支净额(万元)',
    `total_profit`           decimal(20, 3)                DEFAULT NULL COMMENT ' 利润总额(万元)',
    `net_profit`             decimal(20, 3)                DEFAULT NULL COMMENT ' 净利润(万元)',
    `deduct_profit`          decimal(20, 3)                DEFAULT NULL COMMENT ' 净利润(扣除非经常性损益后)(万元)',
    `net_cash_flow`          decimal(20, 3)                DEFAULT NULL COMMENT ' 经营活动产生的现金流量净额(万元)',
    `net_increase_cash`      decimal(20, 3)                DEFAULT NULL COMMENT ' 现金及现金等价物净增加额(万元)',
    `total_assets`           decimal(20, 3)                DEFAULT NULL COMMENT ' 总资产(万元)',
    `current_assets`         decimal(20, 3)                DEFAULT NULL COMMENT ' 流动资产(万元)',
    `total_liabilities`      decimal(20, 3)                DEFAULT NULL COMMENT ' 总负债（万元）',
    `current_liabilities`    decimal(20, 3)                DEFAULT NULL COMMENT ' 流动负债(万元)',
    `stockholder_equity`     decimal(20, 3)                DEFAULT NULL COMMENT ' 股东权益不含少数股东权益(万元)',
    `weighted_return_equity` decimal(20, 3)                DEFAULT NULL COMMENT ' 净资产收益率加权(%)',
    `market`                 varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT ' 交易市场 ',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 0
  CHARSET = utf8mb4 COMMENT '财务报表-季度';
CREATE TABLE IF NOT EXISTS `share_financial_year`
(
    `id`                     bigint(20)                     NOT NULL AUTO_INCREMENT COMMENT ' 主键 ',
    `report_date`            varchar(30) CHARACTER SET utf8 NOT NULL COMMENT ' 报告年分 格式yyyy-MM-dd ',
    `stock_id`               bigint(20)                     NOT NULL COMMENT ' 个股ID ',
    `code`                   varchar(10) CHARACTER SET utf8 NOT NULL COMMENT ' 股票代码 ',
    `basic_eps`              decimal(20, 3)                DEFAULT NULL COMMENT ' 基本每股收益(元)',
    `eps`                    decimal(20, 3)                DEFAULT NULL COMMENT ' 每股净资产(元)',
    `cash_flow`              varchar(20)                   DEFAULT NULL COMMENT ' 每股经营活动产生的现金流量净额(元)',
    `revenue`                decimal(20, 3)                DEFAULT NULL COMMENT ' 主营业务收入(万元)',
    `profitability`          decimal(20, 3)                DEFAULT NULL COMMENT ' 主营业务利润(万元)',
    `operating_profit`       decimal(20, 3)                DEFAULT NULL COMMENT ' 营业利润(万元)',
    `investment_return`      decimal(20, 3)                DEFAULT NULL COMMENT ' 投资收益 ',
    `non_operating_income`   decimal(20, 3)                DEFAULT NULL COMMENT ' 营业外收支净额(万元)',
    `total_profit`           decimal(20, 3)                DEFAULT NULL COMMENT ' 利润总额(万元)',
    `net_profit`             decimal(20, 3)                DEFAULT NULL COMMENT ' 净利润(万元)',
    `deduct_profit`          decimal(20, 3)                DEFAULT NULL COMMENT ' 净利润(扣除非经常性损益后)(万元)',
    `net_cash_flow`          decimal(20, 3)                DEFAULT NULL COMMENT ' 经营活动产生的现金流量净额(万元)',
    `net_increase_cash`      decimal(20, 3)                DEFAULT NULL COMMENT ' 现金及现金等价物净增加额(万元)',
    `total_assets`           decimal(20, 3)                DEFAULT NULL COMMENT ' 总资产(万元)',
    `current_assets`         decimal(20, 3)                DEFAULT NULL COMMENT ' 流动资产(万元)',
    `total_liabilities`      decimal(20, 3)                DEFAULT NULL COMMENT ' 总负债（万元）',
    `current_liabilities`    decimal(20, 3)                DEFAULT NULL COMMENT ' 流动负债(万元)',
    `stockholder_equity`     decimal(20, 3)                DEFAULT NULL COMMENT ' 股东权益不含少数股东权益(万元)',
    `weighted_return_equity` decimal(20, 3)                DEFAULT NULL COMMENT ' 净资产收益率加权(%)',
    `market`                 varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT ' 交易市场 ',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 0
  CHARSET = utf8mb4 COMMENT '财务报表 年度';

CREATE TABLE IF NOT EXISTS `share_stock_pbest`
(
    `id`          bigint(20)                     NOT NULL AUTO_INCREMENT COMMENT ' 主键 ',
    `shock_id`    bigint(20)                     NOT NULL COMMENT ' 股票ID ',
    `ref_year`    int(5)                         NOT NULL COMMENT ' 年份 ',
    `buy_price`   decimal(10, 2)                 NOT NULL COMMENT ' 买入价格 ',
    `sell_price`  decimal(10, 2)                 NOT NULL COMMENT ' 卖出价格 ',
    `pbest_name`  varchar(20) CHARACTER SET utf8 NOT NULL COMMENT ' 算法名称 ',
    `update_time` datetime                       DEFAULT NULL COMMENT ' 更新时间 ',
    `create_time` datetime                       DEFAULT NULL COMMENT ' 创建时间 ',
    `times`       int(11)                        DEFAULT ' 0 ' COMMENT ' 最大次数 ',
    `profit`      decimal(10, 2)                 DEFAULT NULL COMMENT ' 盈利 ',
    `profit_rate` decimal(10, 4)                 DEFAULT NULL COMMENT ' 利润率 ',
    `shock_code`  varchar(8) CHARACTER SET utf8  DEFAULT NULL COMMENT ' 股票代码 ',
    `market`      varchar(8) CHARACTER SET utf8  DEFAULT NULL COMMENT ' 市场 ',
    `shock_name`  varchar(60) CHARACTER SET utf8 DEFAULT NULL COMMENT ' 股票名称 ',
    PRIMARY KEY (`id`),
    UNIQUE `shock_id` USING BTREE (`shock_id`, `ref_year`, `pbest_name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 0
  CHARSET = utf8mb4 COMMENT '最佳买卖算法记录表';

CREATE TABLE IF NOT EXISTS `share_stock_pbest_history`
(
    `id`          bigint(20)                     NOT NULL AUTO_INCREMENT COMMENT ' 主键 ',
    `shock_id`    bigint(20)                     NOT NULL COMMENT ' 股票ID ',
    `ref_year`    int(5)                         NOT NULL COMMENT ' 年份 ',
    `buy_price`   decimal(10, 2)                 NOT NULL COMMENT ' 买入价格 ',
    `sell_price`  decimal(10, 2)                 NOT NULL COMMENT ' 卖出价格 ',
    `pbest_name`  varchar(20) CHARACTER SET utf8 NOT NULL COMMENT ' 算法名称 ',
    `update_time` datetime                       DEFAULT NULL COMMENT ' 更新时间 ',
    `create_time` datetime                       DEFAULT NULL COMMENT ' 创建时间 ',
    `times`       int(11)                        DEFAULT ' 0 ' COMMENT ' 最大次数 ',
    `profit`      decimal(10, 2)                 DEFAULT NULL COMMENT ' 盈利 ',
    `profit_rate` decimal(10, 4)                 DEFAULT NULL COMMENT ' 利润率 ',
    `shock_code`  varchar(8) CHARACTER SET utf8  DEFAULT NULL COMMENT ' 股票代码 ',
    `market`      varchar(8) CHARACTER SET utf8  DEFAULT NULL COMMENT ' 市场 ',
    `shock_name`  varchar(60) CHARACTER SET utf8 DEFAULT NULL COMMENT ' 股票名称 ',
    PRIMARY KEY (`id`),
    UNIQUE `shock_id` USING BTREE (`shock_id`, `ref_year`, `pbest_name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 0
  CHARSET = utf8mb4 COMMENT '最佳买卖算法记录表(历史 非当前年份)';

CREATE TABLE IF NOT EXISTS `share_stock_count_year` (
                                          `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                          `stock_id` bigint(20) NOT NULL COMMENT '股票ID',
                                          `ref_date` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '日期',
                                          `code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票代码',
                                          `market` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '市场',
                                          `up_times` int(3) DEFAULT NULL COMMENT '上涨次数',
                                          `down_times` int(3) DEFAULT NULL COMMENT '下跌次数',
                                          `top_price` decimal(10,2) DEFAULT NULL COMMENT '最高价',
                                          `low_price` decimal(10,2) DEFAULT NULL COMMENT '最低价',
                                          `top_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最高价日期',
                                          `low_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最低价日期',
                                          `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                          PRIMARY KEY (`id`),
                                          UNIQUE KEY `uni_index` (`stock_id`,`ref_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='个股年度统计';