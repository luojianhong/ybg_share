/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.ybg.framework.constant;

/**
 * @author lengleng
 * @date 2017/10/29
 */
public interface CommonConstant {

	/**
	 * 菜单
	 */
	String MENU = "0";

	/**
	 * token请求头名称
	 */
	String REQ_HEADER = "Authorization";

	/**
	 * 路由信息Redis保存的key
	 */
	String ROUTE_KEY = "_ROUTE_KEY";


	/**
	 * 删除
	 */
	String STATUS_DEL = "1";



	/**
	 * 正常
	 */
	String STATUS_NORMAL = "0";

	/**
	 * token分割符
	 */
	String TOKEN_SPLIT = "Bearer ";

	/**
	 * 编码
	 */
	String UTF8 = "UTF-8";
}
