package com.ybg.framework.enums;

/**
 * 同步数据策略枚举
 */
public enum SyncPolicyEnum {
    /**
     * 全量，冲突覆盖
     */
    all_rewrite(1),
    /**
     * 增量
     */
    increment(2),
    /**
     * 全量，冲突忽略
     */
    all_ignore(3);

    int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    SyncPolicyEnum(int value) {
        this.value = value;
    }
}
