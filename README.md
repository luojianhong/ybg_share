# ybg_share

#### 介绍
Ybg股票分析， java api 仅供学习参考，采用srpingboot2  构建
使用了springboot2 +mybatis plus3 +mysql+shardingjdbc +redis+zookeeper + dubbo 技术栈
方便程序员自己选股，只要设置好算法偏差值 第二天早上就能收到选股列表邮件，懒人必备。



### 前端
https://gitee.com/SYDeament/ybg_share_webUI   (未开发完)

## 使用的网易财经的api以及爬虫技术获取数据

# 运行步骤
- 推荐使用idea开发工具，eclipse 可能很多东西需要设置
- 启动zookeeper 端口 2181 
- 修改share-service 的application.properties文件的内容 把pro改成dev。并且配置email.netaset.account 和email.netaset.password两个属性 你自己的163的系统邮箱地址，其中 email.netaset.password 可以是授权码
- 修改数据库和redis配置。
- 导入数据库文件，脚本地址 https://gitee.com/SYDeament/ybg_share/wikis/%E6%9C%AC%E6%95%B0%E6%8D%AE%E5%BA%93%E8%84%9A%E6%9C%AC?sort_id=1839176
- share_receive_email数据库表 添加接收推荐股票列表的邮箱，其中的start_time 和end_time 必须要合理。并且接收邮箱必须设置163的系统邮箱地址到白名单中，否则其他企业可能会自行拦截收不到邮箱。
- 运行ShareApplication 的main方法。等待拉取数据完毕（定时任务跑的，如果想立即执行，可以用单元测试调起来）推荐使用centerOS7 跑。

# 运行结果
![输入图片说明](https://images.gitee.com/uploads/images/2020/0101/213802_c8bce75b_880593.png "屏幕截图.png")


## 修改版本历史
- 2019-10-13 创建项目
- 2019-11-01 完成拉个股取日K，财务 ，基本功能
- 2019-11-01 完成最佳买入算法（单调） 基本功能
- 2019-11-02 扩展框架，加入授权服务，添加dubbo依赖
- 2020-01-01 发布1.0.0 版 可以收到推荐列表邮件
- 2020-01-08 完成千股千评的数据拉取


## 模块依赖
```
├── block-parent 区块链模块（可以无视，暂未开发）
├── share-parent 股票模块
   ├── share-admin              #超管的微服务 dubbo消费者 （可以无视，暂未开发）
   ├── share-api                #dubbo 接口声明
   └── share-service            #股票dubbo 服务提供者
├── ybg-common                  #公告的pojo，工具类，常量 模块（不能引入框架的东西），apiDubbo服务依赖此maven
├── ybg-core                    #核心框架公共包，写一些公共框架的东西，依赖了ybg-common  ,是Dubbo提供者依赖此类或微服务依赖此类
└── ybg-auth                    #授权中心，依赖了ybg-common（可以无视，暂未开发）

```

# 系统开发进度


## 基础数据
- [x] 分析股票列表，有个禁用分析的字段，非启用的不允许拉取日K数据 
- [x] 拉取深沪个股票日K数据，
- [x] 大盘数据拉取 拉取上证指数，深证指数数据,

## 基础数据 定时任务
- [x] 分析股票列表，有个禁用分析的字段，非启用的不允许拉取日K数据 
- [x] 拉取深沪个股票日K数据，
- [x] 大盘数据拉取 拉取上证指数，深证指数数据,

## 其他数据
- [x] 拉取财务报表数据，
- [x] 股票所属板块关联，板块模块  
- [x] 千股千评数据拉取
- [x] 个股公告信息 
- [ ] 财务盈利能力信息拉取 ![](http://quotes.money.163.com/old#query=YLNL&DataType=yynl&sort=MGSY_T&order=desc&count=25&page=0  "")
- [ ] 偿债能力信息拉取 ![](http://quotes.money.163.com/old#query=YLNL&DataType=yynl&sort=MGSY_T&order=desc&count=25&page=0  "")
- [ ] 成长能力信息拉取![](http://quotes.money.163.com/old#query=YLNL&DataType=yynl&sort=MGSY_T&order=desc&count=25&page=0  "")
- [ ] 运营能力信息拉取![](http://quotes.money.163.com/old#query=YLNL&DataType=yynl&sort=MGSY_T&order=desc&count=25&page=0  "")
- [ ] 基金持股信息拉取 ![](http://quotes.money.163.com/old#query=JJCGPH&DataType=jjcgph&sort=SHULIANG&order=desc&count=25&page=0"")

## 报告模块
- [x] 数据分析
- [x] 分析报告




#### 题外话
-  想要推荐股票列表的可以下方留言。名额有限，免费名额最多100位。
-  为什么使用dubbo而不是spring cloud?
   因为成本太大。本地的环境内存都要炸（16G）。并不是没有能力开发()
-  收费吗？二次开发收费吗？
   原则上是不收费。但是请不要申请软件著作权。本人已经申请了。 商用也就睁一只眼闭一只眼。
-  为什么要开发此系统？
   只是太闲了玩玩。。
-  作者联系方式？
   若是有心相信您总能找到
-  作者接外包吗？
   不接私活，不做外包。
-  除了股票的信息，你可以在这里学会的东西
   - 第三方http请求。
   - 爬虫框架
   - 并发拉取数据
   - 数据分表
   - springboot和dubbo的框架
   - 项目结构
   - 发送邮件
   - redisTemplate 数据操作
   - zookper 锁的操作
   
   
   
       